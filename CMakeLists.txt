cmake_minimum_required(VERSION 2.8.12)
project(AccerionSensorAPI)

SET(DEBUGMODE OFF)
SET(_WIN32 	OFF)

if(DEBUGMODE STREQUAL "ON")
message(STATUS "DEBUG flag active")
add_definitions(-DDEBUG)
endif()

if(TARGET AccerionSensorAPI)
    return()
endif(TARGET AccerionSensorAPI)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DLINUX=1 -D_UNIX -D_CONSOLE -g")
set(LD "$(CXX)")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

set(include_dest "include/AccerionSensorAPI")
set(main_lib_dest "lib/AccerionSensorAPI")

if(DEFINED ACCERION_PIC_COMPILE)
    message(STATUS "COMPILING WITH PIC FLAG")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
endif()

set(header_path "${AccerionSensorAPI_SOURCE_DIR}/include/")
file(GLOB_RECURSE headers "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
file(GLOB_RECURSE sources "${CMAKE_CURRENT_SOURCE_DIR}/source/*.cpp")

configure_file("source/config.hpp.in" "${CMAKE_CURRENT_BINARY_DIR}/config_impl.hpp")

if(DEFINED ACCERION_API_BUILD_DYNAMIC)
    add_library( AccerionSensorAPI SHARED ${headers} ${sources} )
else()
    add_library( AccerionSensorAPI ${headers} ${sources} )
endif()

target_include_directories(AccerionSensorAPI PUBLIC
        $<BUILD_INTERFACE:${AccerionSensorAPI_SOURCE_DIR}/include> # for headers when building
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}> # for config_impl.hpp when building
        $<INSTALL_INTERFACE:${include_dest}> # for client in install mode
        $<INSTALL_INTERFACE:${lib_dest}> # for config_impl.hpp in install mode)
        )

target_link_libraries( AccerionSensorAPI -pthread)
set_target_properties( AccerionSensorAPI PROPERTIES  COMPILE_FLAGS "${EXTRA_CXX_FLAGS}")

install(TARGETS AccerionSensorAPI DESTINATION "${main_lib_dest}")
install(FILES ${headers} DESTINATION "${include_dest}")
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/config_impl.hpp DESTINATION "${include_dest}")

set(lib_dest "${main_lib_dest}/")
install(TARGETS AccerionSensorAPI EXPORT AccerionSensorAPI DESTINATION "${lib_dest}")
install(FILES accerionsensorapi-config.cmake DESTINATION ${main_lib_dest})
install(EXPORT AccerionSensorAPI DESTINATION "${lib_dest}" FILE accerionsensorapi-config.cmake)