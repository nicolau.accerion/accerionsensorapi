/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef ACCERION_SENSOR_H
#define ACCERION_SENSOR_H
#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <cmath>
#include <ctime>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include "TypeDef.h"
#include "commands.h"
#include "structs.h"
#include "callbacks.h"
#include "TCPClient.h"
#include "UDPReceiver.h"
#include "UDPTransmitter.h"
#include "TCPClient.h"
#include "CRC8.h"
#include "ProfileTimer.h"
#include "Serialization.h"

#ifdef __linux__ 
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/// AccerionSensor provides an object-oriented "interface". Methods invoked on this object are relayed to the sensor over the network and received messages are forwarded to the registered callbacks
class AccerionSensor
{
    public:
        /**
         * \brief Constructor for AccerionSensor object, can be invoked by you, but recommended use is through AccerionSensorManager
         * \param ip address of the sensor that is to be connected to
         * \param serial of the sensor that is to be connected to
         * \param localIP is the ip address of the local machine in case unicasting is used
         * \param connType to provide the preferred method of connection
         **/
        AccerionSensor(Address ip, const std::string& serial, Address localIP, ConnectionType connType);
        /// AccerionSensor Destructor
        ~AccerionSensor();

        // STREAMING MSGS
        /**
         * \brief Method to register a callback to the HeartBeat message
         * \param hbCallback callback method which has to be invoked on an incoming HeartBeat message
         **/
        void subscribeToHeartBeat(_heartBeatCallBack hbCallback);

        /**
         * \brief Method to register a callback to the Corrected Pose message
         * \param cpCallback callback method which has to be invoked on an incoming Corrected Pose message
         **/
        void subscribeToCorrectedPose(_correctedPoseCallBack cpCallback);

        /**
         * \brief Method to register a callback to the Uncorrected Pose message
         * \param upCallback callback method which has to be invoked on an incoming Uncorrected Pose message
         **/
        void subscribeToUncorrectedPose(_uncorrectedPoseCallBack upCallback);

        /**
         * \brief Method to register a callback to the Diagnostics message
         * \param diagCallback callback method which has to be invoked on an incoming Diagnostics message
         **/
        void subscribeToDiagnostics(_diagnosticsCallBack diagCallback);

        /**
         * \brief Method to register a callback to the Drift Correction message
         * \param dcCallback callback method which has to be invoked on an incoming Drift Correction message
         **/
        void subscribeToDriftCorrections(_driftCorrectionCallBack dcCallback);

        /**
         * \brief Method to register a callback to the Quality Estimate message
         * \param qeCallback callback method which has to be invoked on an incoming Quality Estimate message
         **/
        void subscribeToQualityEstimates(_qualityEstimateCallBack qeCallback);

        /**
         * \brief Method to register a callback to the Line Follower message
         * \param lfCallback callback method which has to be invoked on an incoming Line Follower message
         **/
        void subscribeToLineFollowerData(_lineFollowerCallBack lfCallback);

        /**
         * \brief Method to register a callback to the Marker Position Packet message
         * \param mppCallback callback method which has to be invoked on an incoming Marker Position Packet message
         **/
        void subscribeToMarkerPosPacket(_markerPosPacketCallBack mppCallback);

        /**
         * \brief Method to register a callback to the Console Output message
         * \param consoleCallback callback method which has to be invoked on an incoming Console Output message
         **/
        void subscribeToConsoleOutputInfo(_consoleOutputCallback consoleCallback);

        // CMD - ACK MSGS
        /**
         * \brief Method to toggle Accerion QR Mapping
         * \param on bool holding true/false whether to turn this on/off
         * \param accQRMappingCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleAccQRMapping(bool on, _acknowledgementCallBack accQRMappingCallBack);
        /**
         * \brief Method to toggle Accerion QR Mapping
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleAccQRMappingBlocking(bool on);

        /**
         * \brief Method to toggle Absolute Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param amCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleAbsoluteMode(bool on, _acknowledgementCallBack amCallBack);
        /**
         * \brief Method to toggle Absolute Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleAbsoluteModeBlocking(bool on);

        /**
         * \brief Method to toggle Recording Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param recordingModeCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleRecordingMode(bool on, _acknowledgementCallBack recordingModeCallBack);
        /**
         * \brief Method to toggle Recording Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleRecordingModeBlocking(bool on);

        /**
         * \brief Method to toggle Idle Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param idleCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleIdleMode(bool on, _acknowledgementCallBack idleCallBack);
        /**
         * \brief Method to toggle Idle Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleIdleModeBlocking(bool on);

        /**
         * \brief Method to toggle Reboot Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param rebootCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleRebootMode(bool on, _acknowledgementCallBack rebootCallBack);
        /**
         * \brief Method to toggle Reboot Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleRebootModeBlocking(bool on);

        /**
         * \brief Method to toggle Calibration Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param calibCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleCalibrationMode(bool on, _acknowledgementCallBack calibCallBack);
        /**
         * \brief Method to toggle Calibration Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleCalibrationModeBlocking(bool on);

        /**
         * \brief Method to request the Marker Map
         * \param mppCallback callback method which has to be invoked when the sensor sends a Marker Pos Packet
         **/
        void requestMarkerMap(_markerPosPacketCallBack mppCallback);

        /**
         * \brief Method to get the IP Address
         * \param ipCallback callback method which has to be invoked when the sensor responds
         **/
        void getIPAddress(_ipAddressCallBack ipCallback);
        /**
         * \brief Method to get the IP Address
         * \return IPAddressExtended holding all the IP Address values, 0 if no acknowledgement has been received/a timeout has occurred.
         **/
        IPAddressExtended getIPAddressBlocking();

        /**
         * \brief Method to get the Sample Rate
         * \param srCallback callback method which has to be invoked when the sensor responds
         **/
        void getSampleRate(_sampleRateCallBack srCallback);
        /**
         * \brief Method to get the Sample Rate
         * \return SampleRate holding the Sample Rate value, 0 if no acknowledgement has been received/a timeout has occurred.
         **/
        SampleRate getSampleRateBlocking();

        /**
         * \brief Method to clear the QR Library
         * \param clearQRLibCallBack callback method which has to be invoked when the sensor responds
         **/
        void clearQRLibrary(_acknowledgementCallBack clearQRLibCallBack);
        /**
         * \brief Method to clear the QR Library
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int  clearQRLibraryBlocking();

        /**
         * \brief Method to get the Serial Number
         * \param snCallback callback method which has to be invoked when the sensor responds
         **/
        void getSerialNumber(_serialNumberCallBack snCallback);
        /**
         * \brief Method to get the Serial Number
         * \return SerialNumber holding the Serial Number value, 0 if no acknowledgement has been received/a timeout has occurred.
         **/
        SerialNumber getSerialNumberBlocking();

        /**
         * \brief Method to clear the Cluster Library
         * \param clearClusterLibCallBack callback method which has to be invoked when the sensor responds
         **/
        void clearClusterLibrary(_acknowledgementCallBack clearClusterLibCallBack);
        /**
         * \brief Method to clear the Cluster Library
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int  clearClusterLibraryBlocking();

        /**
         * \brief Method to toggle Aruco Marker Detection Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param tammCallBack callback method which has to be invoked when the sensor responds
         **/
        void toggleArucoMarkerDetectionMode(bool on, _acknowledgementCallBack tammCallBack);
        /**
         * \brief Method to toggle Aruco Marker Detection Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int  toggleArucoMarkerDetectionModeBlocking(bool on);
        
        /**
        * \brief Method to trigger the sensor to send all kinds of acknowledgements
        * YOU NEED TO BE SUBSCRIBED/HAVE A CALLBACK REGISTERED TO THE MESSAGES FOR THIS TO WORK
        */
        void getAllAcknowledgements();

        /**
         * \brief Method to get the Software Version
         * \param svCallback callback method which has to be invoked when the sensor responds
         **/
        void getSoftwareVersion(_softwareVersionCallBack svCallback);
        /**
         * \brief Method to get the Software Version
         * \return SoftwareVersion holding the Software Version values, 0's if no acknowledgement has been received/a timeout has occurred.
         **/
        SoftwareVersion getSoftwareVersionBlocking();

        /**
         * \brief Method to get the TCP/IP Information
         * \param tcpIPCallback callback method which has to be invoked when the sensor responds
         **/
        void getTCPIPInformation(_tcpIPInformationCallBack tcpIPCallback);
        /**
         * \brief Method to get the TCP/IP Information
         * \return TCPIPInformation holding the TCP/IP Information, 0's if no acknowledgement has been received/a timeout has occurred.
         **/
        TCPIPInformation getTCPIPInformationBlocking();

        /**
         * \brief Method to get the Mount Pose of the Sensor
         * \param mpCallback callback method which has to be invoked when the sensor responds
         **/
        void getSensorMountPose(_poseCallBack mpCallback);
        /**
         * \brief Method to get the Mount Pose of the Sensor
         * \return Pose holding the Mount Position, 0's if no acknowledgement has been received/a timeout has occurred.
         **/
        Pose getSensorMountPoseBlocking();

        /**
         * \brief Method to toggle Expert Mode
         * \param on bool holding true/false whether to turn this on/off
         * \param emCallback callback method which has to be invoked when the sensor responds
         **/
        void toggleExpertMode(bool on, _acknowledgementCallBack emCallback);
        /**
         * \brief Method to toggle Expert Mode
         * \param on bool holding true/false whether to turn this on/off
         * \return int which holds -1 if no acknowledgement has been received, 0 if it is off, 1 if it is on. Note that -1 does not mean the value has not been changed.
         **/
        int toggleExpertModeBlocking(bool on);

        /**
         * \brief Method to set the Sample Rate
         * \param rate SampleRate struct holding the sample rate value that is to be set
         * \param srCallback callback method which has to be invoked when the sensor responds
         **/
        void setSampleRate(SampleRate rate,  _sampleRateCallBack srCallback);
        /**
         * \brief Method to set the Sample Rate
         * \param rate SampleRate struct holding the sample rate value that is to be set
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setSampleRateBlocking(SampleRate rate);

        /**
         * \brief Method to remove a QR from the Library
         * \param qrID uint16_t ID of the QR that has to be removed
         * \param rqrCallback callback method which has to be invoked when the sensor responds
         **/
        void removeQRFromLibrary(uint16_t qrID,  _removeQRCallBack rqrCallback);
        /**
         * \brief Method to remove a QR from the Library
         * \param qrID uint16_t ID of the QR that has to be removed
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int removeQRFromLibraryBlocking(uint16_t qrID);

        /**
         * \brief Method to set the Recovery Mode
         * \param on bool value to toggle it either on or off
         * \param radius search radius in meters
         * \param rmCallback callback method to be invoked upon acknowledgement
         **/
        void toggleRecoveryMode(bool on, uint8_t radius, _acknowledgementCallBack rmCallback);
        /**
         * \brief method to set the Recovery Mode
         * \param on bool value to toggle it either on or off
         * \param radius search radius in meters
         **/
        int toggleRecoveryModeBlocking(bool on, uint8_t radius);

        /**
         * \brief Method to remove a Cluster from the Library
         * \param clusterID uint16_t ID of the Cluster that has to be removed
         * \param rcCallback callback method which has to be invoked when the sensor responds
         **/
        void removeClusterFromLibrary(uint16_t clusterID,  _removeClusterCallBack rcCallback);
        /**
         * \brief Method to remove a Cluster from the Library
         * \param clusterID uint16_t ID of the Cluster that has to be removed
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int removeClusterFromLibraryBlocking(uint16_t clusterID);

        /**
         * \brief Method to request Secondary Line Follower output
         * \param clusterID uint16_t ID of the Cluster that you want the closest point of
         * \param slfCallback callback method which has to be invoked when the sensor responds
         **/
        void getSecondaryLineFollowerOutput(uint16_t clusterID, _secondaryLineFollowerCallBack slfCallback);
        /**
         * \brief Method to request Secondary Line Follower output
         * \param clusterID uint16_t ID of the Cluster that you want the closest point of
         * \return LineFollowerData holding the data belonging to the closest point of the requested cluster, 0's if no acknowledgement has been received/a timeout has occurred.
         **/
        LineFollowerData getSecondaryLineFollowerOutputBlocking(uint16_t clusterID);

        /**
         * \brief Method to set the IP Address
         * \param ip IPAddress struct containing the IP address that is to be set
         * \param ipCallback callback method which has to be invoked when the sensor responds
         **/
        void setIPAddress(IPAddress ip, _ipAddressCallBack ipCallback);
        /**
         * \brief Method to set the IP Address
         * \param ip IPAddress struct containing the IP address that is to be set
         * \return IPAddressExtended containing the IP address that is set, or the previous one if it failed. Can contain 0's in case of communication failures too.
         **/
        IPAddressExtended setIPAddressBlocking(IPAddress ip);

        /**
         * \brief Method to add a QR to the Library
         * \param qrID uint16_t ID of the QR that is to be added
         * \param qrPose Pose of where the QR is to be added
         * \param qrCallBack callback method which has to be invoked when the sensor responds
         **/
        void addQRToLibrary(uint16_t qrID, Pose qrPose, _addQRCallBack qrCallBack);
        /**
         * \brief Method to add a QR to the Library
         * \param qrID uint16_t ID of the QR that is to be added
         * \param qrPose Pose of where the QR is to be added
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int addQRToLibraryBlocking(uint16_t qrID, Pose qrPose);

        /**
         * \brief Method to set the Date/Time
         * \param dt DateTime struct containing the Date and Time to be set
         * \param dtCallBack callback method which has to be invoked when the sensor responds
         **/
        void setDateTime(DateTime dt, _dateTimeCallBack dtCallBack);
        /**
         * \brief Method to set the Date/Time
         * \param dt DateTime struct containing the Date and Time to be set
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setDateTimeBlocking(DateTime dt);

        /**
         * \brief Method to toggle the Mapping Mode
         * \param on bool value to toggle it either on or off
         * \param clusterID uint16_t ID it should receive
         * \param mappingCallback callback method to be invoked upon acknowledgement
         **/
        void toggleMapping(bool on, uint16_t clusterID, _acknowledgementCallBack mappingCallback);
        /**
         * \brief Method to toggle the Mapping Mode
         * \param on bool value to toggle it either on or off
         * \param clusterID uint16_t ID it should receive
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int toggleMappingBlocking(bool on, uint16_t clusterID);

        /**
         * \brief Method to set the Sensor Pose
         * \param poseStruct Pose struct that holds the position to be set
         * \param mpCallback callback method to be invoked upon acknowledgement
         **/
        void setSensorPose(Pose poseStruct,  _poseCallBack mpCallback);
        /**
         * \brief Method to set the Sensor Pose
         * \param poseStruct Pose struct that holds the position to be set
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setSensorPoseBlocking(Pose poseStruct);

        /**
         * \brief Method to set the Sensor Mount Pose
         * \param mountPoseStruct Pose struct that holds the mount position to be set
         * \param mpCallback callback method to be invoked upon acknowledgement
         **/
        void setSensorMountPose(Pose mountPoseStruct,  _poseCallBack mpCallback);
        /**
         * \brief Method to set the Sensor Mount Pose
         * \param mountPoseStruct Pose struct that holds the mount position to be set
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setSensorMountPoseBlocking(Pose mountPoseStruct);

        /**
         * \brief Method to provide external position input to the sensor
         * \param pose InputPose struct that holds things like position and covariance that are to be set
         **/
        void setPoseAndCovariance(InputPose pose);

        /**
         * \brief Method to set the TCP/IP Receiver
         * \param ipAddr Address struct containing the receiver(sensor is sender)'s ip address
         * \param messageType uint8_t type of messages to be sent by TCP/IP. See MessageTypes
         * \param tcpipCallback callback method to be invoked upon acknowledgement
         **/
        void setTCPIPReceiver(Address ipAddr, uint8_t messageType, _tcpIPInformationCallBack tcpipCallback);
        /**
         * \brief Method to set the TCP/IP Receiver
         * \param ipAddr Address struct containing the receiver(sensor is sender)'s ip address
         * \param messageType uint8_t type of messages to be sent by TCP/IP. See MessageTypes
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setTCPIPReceiverBlocking(Address ipAddr, uint8_t messageType);

        /**
         * \brief Method to toggle the Line Following Mode
         * \param on bool indicating whether to turn the mode on/off
         * \param clusterID uint16_t ID of the cluster to follow
         * \param lfCallback callback method to be invoked upon acknowledgement
         **/
        void toggleLineFollowing(bool on, uint16_t clusterID, _acknowledgementCallBack lfCallback);
        /**
         * \brief Method to toggle the Line Following Mode
         * \param on bool indicating whether to turn the mode on/off
         * \param clusterID uint16_t ID of the cluster to follow
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int toggleLineFollowingBlocking(bool on, uint16_t clusterID);

        /**
         * \brief Method to set the UDP Settings
         * \param udpInfo UDPInfo struct containing the UDP settings to be set
         * \param udpCallback callback method to be invoked upon acknowledgement
         **/
        void setUDPSettings(UDPInfo udpInfo, _setUDPSettingsCallBack udpCallback);
        /**
         * \brief Method to set the UDP Settings
         * \param udpInfo UDPInfo struct containing the UDP settings to be set
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int setUDPSettingsBlocking(UDPInfo udpInfo);

        /**
         * \brief Method to get a Cluster in G2O format
         * \param clusterID uint16_t ID of the cluster to get
         * \param g2oCallBack callback method to be invoked upon acknowledgement
         **/
        void getClusterInG2OFormat(uint16_t clusterID, _clusterInG2OFormatCallBack g2oCallBack);
        /**
         * \brief Method to get a Cluster in G2O format
         * \param clusterID uint16_t ID of the cluster to get
         * \param path string that contains the path where it should be stored
         **/
        int getClusterInG2OFormat(uint16_t clusterID, std::string path);

        /**
         * \brief Method to replace/modify a cluster by G2O format
         * \param clusterID uint16_t ID of the cluster to get
         * \param path string path to the file containing a cluster in G2O format
         * \param g2oCallBack callback method to be invoked upon acknowledgement
         **/
        void replaceClusterWithG2OFormat(uint16_t clusterID, std::string path, _acknowledgementCallBack g2oCallBack);
        /**
         * \brief Method to replace/modify a cluster by G2O format
         * \param clusterID uint16_t ID of the cluster to get
         * \param path string path to the file containing a cluster in G2O format
         * \return int which holds -1 if no acknowledgement has been received, 0 if it failed, 1 if it succeeded. Note that -1 does not mean the value has not been changed.
         **/
        int replaceClusterWithG2OFormatBlocking(uint16_t clusterID, std::string path);

        /**
         * \brief Method to export/download a map
         * \param destinationPath string full path to where the file should be stored including name(ending on .amf)
         * \param progressCB callback method to be invoked to inform about progress
         * \param doneCB callback method to be invoked after a success or failure
         * \param statusCB callback method to be invoked to inform about status, see FileSenderStatus
         **/
        bool getMap(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB);
        /**
         * \brief Method to import/upload a map
         * \param sourcePath string full path to where the file that is to be uploaded is located
         * \param progressCB callback method to be invoked to inform about progress
         * \param doneCB callback method to be invoked after a success or failure
         * \param statusCB callback method to be invoked to inform about status, see FileSenderStatus
         * \param strategy int value 0 = replace 1 = merge
         **/
        bool sendMap(std::string sourcePath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB, int strategy);

        /**
         * \brief Method to get the Software Details
         * \param sdCallback callback method to be invoked upon acknowledgement
         **/
        void getSoftwareDetails(_softwareDetailsCallBack sdCallback);
        /**
         * \brief Method to get the Software Details
         * \return SoftwareDetails containing the Software Details or 0's in case of failure
         **/
        SoftwareDetails getSoftwareDetailsBlocking();

        /**
         * \brief Method to register a callback to the Marker Pos Packet Start/Stop Messages
         * \param mpCallBack callback method which has to be invoked on an incoming Marker Pos Packet Start/Stop Message
         **/
        void subscribeToMarkerPosPacketStartStop(_acknowledgementCallBack mpCallBack);

        /**
         * \brief Method to register a callback to the Drift Corrections Missed message
         * \param dcmCallback callback method which has to be invoked on an incoming Drift Corrections Missed message
         **/
        void subscribeToDriftCorrectionsMissed(_driftCorrectionsMissedCallBack dcmCallback);

        /**
         * \brief Method to register a callback to the Aruco Marker Detection message
         * \param amCallback callback method which has to be invoked on an incoming Aruco Marker Detected message
         **/
        void subscribeToArucoMarkers(_arucoMarkerCallBack amCallback);

        /**
         * \brief Method to register a callback to the Map Loaded message NOTE: it is experimental and not available in every software version. 
         * It is an early alpha version and will only output a success message with 100 percentage or a fail and 0 percent if no DB present.
         * \param amCallback callback method which has to be invoked on an incoming Map Loaded message
         **/
        void subscribeToMapLoaded(_mapLoadedCallBack mlCallback);

        /**
         * \brief Method to set the Buffer Length of the Buffered Recovery Mode
         * \param ubufferLength uint32_t length in meters
         * \param blCallBack callback method to be invoked upon acknowledgement
         **/
        void setBufferLength(uint32_t bufferLength, _bufferLengthCallBack blCallBack);
        /**
         * \brief Method to set the Buffer Length of the Buffered Recovery Mode
         * \param ubufferLength uint32_t length in meters
         * \return int containing the buffer length(if set successfully)
         **/
        int setBufferLengthBlocking(uint32_t bufferLength);

        /**
         * \brief Method to get the Buffer Length of the Buffered Recovery Mode
         * \param blCallBack callback method to be invoked upon acknowledgement
         **/
        void getBufferLength(_bufferLengthCallBack blCallBack);
        /**
         * \brief Method to get the Buffer Length of the Buffered Recovery Mode
         * \return int containing the buffer length(if set successfully)
         **/
        int getBufferLengthBlocking();

        /**
         * \brief Method to start the Buffered Recovery Mode
         * \param xPos int32_t x position in meters
         * \param yPos int32_t y position in meters
         * \param radius uint8_t radius in meters
         * \param bpCallBack callback method to be invoked upon acknowledgement
         **/
        void startBufferedRecovery(int32_t xPos, int32_t yPos, uint8_t radius, _bufferProgressCallBack bpCallBack);
        /**
         * \brief Method to stop the Buffered Recovery Mode
         * \param bpCallBack callback method to be invoked upon acknowledgement
         **/
        void cancelBufferedRecovery(_bufferProgressCallBack bpCallBack);

        /**
         * \brief Method to get a list of all the recordings
         * \param recCallBack callback method to be invoked upon acknowledgement
         **/
        void getRecordingsList(_recordingListCallBack recCallBack);
        /**
         * \brief Method to get a list of all the recordings
         * \param vector vector to be filled with the recording names
         * \return bool whether the request was successful or not.
         **/
        bool getRecordingsListBlocking(std::vector<std::string> &vector);

        /**
         * \brief Method to download recordings
         * \param indexes vector containing the indexes of the desired recordings
         * \param destinationPath string full path to where the file should be stored including name(ending on .arf)
         * \param progressCB callback method to be invoked to inform about progress
         * \param doneCB callback method to be invoked after a success or failure
         * \param statusCB callback method to be invoked to inform about status, see FileSenderStatus
         **/
        bool getRecordings(std::vector<uint8_t> indexes, std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB);

        /**
         * \brief Method to delete recordings, note that the indexes(starting from 0) are linked to the alphabetical order on the recordings on the sensor. 
         * If your list of recordings is outdated, you might be deleting a different recording than you wish.
         * \param indexes vector of indexes to be removed
         * \param recCallBack callback method to be invoked upon acknowledgement
         **/
        void deleteRecordings(std::vector<uint8_t> indexes, _deleteRecordingsCallBack cb);
        /**
         * \brief Method to delete recordings, note that the indexes(starting from 0) are linked to the alphabetical order on the recordings on the sensor. 
         * If your list of recordings is outdated, you might be deleting a different recording than you wish.
         * \param indexes vector of indexes to be deleted
         * \return DeleteRecordingsResult containing whether the request was successful or not and which indexes failed. If this is empty, it could be a failed request.
         **/
        DeleteRecordingsResult deleteRecordingsBlocking(std::vector<uint8_t> indexes);

        /**
         * \brief Method to retrieve a frame
         * \param camIdx the index of the camera
         * \param key that will grant you access to a frame
         * Used for Accerion Testing Purposes..
         **/
        std::vector<uint8_t> captureFrame(uint8_t camIdx, std::string key);//void captureFrame(uint8_t mode, uint16_t floorID, uint8_t frame);

    private:
        bool debugMode_ = false; //!< Flag used for debugging

        UDPTransmitter *    udpTransmitter;  //!< Class used to transmit messages from API to sensor
        UDPReceiver    *    udpReceiver;  //!< Class used to receive messages coming from a sensor
        TCPClient      *    tcpClient;  //!< TCP Communication class for sending and receiving messages
        ConnectionType      connectionType;  //!< Holds the preferred connection type to the sensor
        Address             localIP_;  //!< Holds the local IP address, this is sent to sensor in case of unicasting

        bool runUDP = true;  //!< Value that is checked by UDP thread to see if it should run
        bool runTCP = true;  //!< Value that is checked by TCP thread to see if it should run

        CRC8                    crc8_;  //!< used for verification purposes
        std::vector<uint8_t>    receivedCommand_; //!< Holds incoming UDP command (size depends on UDP command ID)
        uint8_t                 receivedCommandID_; //!< Holds the ID of the received command, see commands.h
        bool                    lastMessageWasBroken_; //!< Keeps track whether the last message was split up/broken
        uint32_t                sensorSerialNumber_ = DEFAULT_SERIAL_NUMBER; //!< Holds the unique serial number of the Jupiter unit
        uint32_t                receivedSerialNumber_; //!< Holds received serial number from incoming UDP message, used to determine if message is intended for this unit
        uint8_t                 receivedCRC8_; //!< Holds incoming CRC8 code (in 0xD8) , compared with CRC code computed at Jupiter side for error-checking
        bool                    messageReady_; //!< True if an incoming UDP message is ready, False otherwise.
        std::vector<Command>    outgoingCommands; //!< Vector of outgoing commands
        std::mutex              outgoingCommandsMutex; //!< mutex that guards the vector of outgoing commands

        void runUDPCommunication();  //!< Method that is ran on a separate thread that checks for incoming messages and sends out messages
        void runTCPCommunication();  //!< Method that is ran on a separate thread that checks for incoming messages and sends out messages

        /**
         * @brief      Checks the received UDP message, sets the necessary values
         *             and finally sends an acknowledgement message
         */
        void readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands);

        /**
         * @brief      parseMessages reads incoming network packages and dissects them. If a command is completely received it is put in the commands vector supplied in the param
         *
         * @param[in]  commands    vector of commands
         * @param[in]  receivedMessage_    vector of data
         */
        void parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_);

        /**
         * @brief      clearOutgoingCommands clears the internal vector of outgoing commands
         */
        void clearOutgoingCommands(){ outgoingCommands.clear();};

        // The Messages below are invoked after initial parsing. They all get a vector of data that is deserialized differently depending on the message
        // STREAMING MSGS
        void outputHeartBeat(std::vector<uint8_t> data);         //!< message that is invoked upon incoming heartbeat messages
        void outputCorrectedPose(std::vector<uint8_t> data);     //!< message that is invoked upon incoming corrected pose messages
        void outputUncorrectedPose(std::vector<uint8_t> data);   //!< message that is invoked upon incoming uncorrected pose messages
        void outputDiagnostics(std::vector<uint8_t> data);       //!< message that is invoked upon incoming diagnostic messages
        void outputDriftCorrection(std::vector<uint8_t> data);   //!< message that is invoked upon incoming drift correction messages
        void outputQualityEstimate(std::vector<uint8_t> data);   //!< message that is invoked upon incoming quality estimate messages
        void outputLineFollowerData(std::vector<uint8_t> data);  //!< message that is invoked upon incoming line follower messages
        void outputMarkerPosPacket(std::vector<uint8_t> data);   //!< message that is invoked upon incoming marker messages
        void outputConsoleOutputInfo(std::vector<uint8_t> data); //!< message that is invoked upon incoming console output messages

        // CMD - ACK MSGS
        void acknowledgeAccQRMapping(std::vector<uint8_t> data); //!< message that is invoked upon incoming ACC QR acknowledgement messages
        void acknowledgeAbsoluteMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming absolute mode acknowledgement messages
        void acknowledgeRecordingMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming recording mode acknowledgement messages
        void acknowledgeIdleMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming idle mode acknowledgement messages
        void acknowledgeRebootMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming reboot mode acknowledgement messages
        void acknowledgeCalibrationMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming calibration mode acknowledgement messages
        void acknowledgeIPAddress(std::vector<uint8_t> data); //!< message that is invoked upon incoming ip address acknowledgement messages
        void acknowledgeSampleRate(std::vector<uint8_t> data); //!< message that is invoked upon incoming sample rate acknowledgement messages
        void acknowledgeClearQRLibrary(std::vector<uint8_t> data); //!< message that is invoked upon incoming clear QR lib acknowledgement messages
        void acknowledgeSerialNumber(std::vector<uint8_t> data); //!< message that is invoked upon incoming serial number acknowledgement messages
        void acknowledgeClearClusterLibrary(std::vector<uint8_t> data); //!< message that is invoked upon incoming clear cluster lib acknowledgement messages
        void acknowledgeSoftwareVersion(std::vector<uint8_t> data); //!< message that is invoked upon incoming software version acknowledgement messages
        void acknowledgeTCPIPInformation(std::vector<uint8_t> data); //!< message that is invoked upon incoming TCP/IP information acknowledgement messages
        void acknowledgeExpertMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming expert mode acknowledgement messages
        void acknowledgeRemoveQR(std::vector<uint8_t> data); //!< message that is invoked upon incoming remove QR acknowledgement messages
        void acknowledgeRecoveryMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming recovery mode acknowledgement messages
        void acknowledgeRemoveCluster(std::vector<uint8_t> data); //!< message that is invoked upon incoming remove cluster acknowledgement messages
        void acknowledgeSecondaryLineFollowerOutput(std::vector<uint8_t> data); //!< message that is invoked upon incoming secondary line follower acknowledgement messages
        void acknowledgeAddQR(std::vector<uint8_t> data); //!< message that is invoked upon incoming add QR acknowledgement messages
        void acknowledgeDateTime(std::vector<uint8_t> data); //!< message that is invoked upon incoming date/time acknowledgement messages
        void acknowledgeSensorPose(std::vector<uint8_t> data); //!< message that is invoked upon incoming sensor pose acknowledgement messages
        void acknowledgeMountPose(std::vector<uint8_t> data); //!< message that is invoked upon incoming mount pose acknowledgement messages
        void acknowledgeMappingToggle(std::vector<uint8_t> data); //!< message that is invoked upon incoming toggle mapping acknowledgement messages
        void acknowledgeLineFollowingToggle(std::vector<uint8_t> data); //!< message that is invoked upon incoming toggle line following acknowledgement messages
        void acknowledgeUDPSettings(std::vector<uint8_t> data); //!< message that is invoked upon incoming UDP settings acknowledgement messages
        void acknowledgeClusterInG2OFormat(std::vector<uint8_t> data); //!< message that is invoked upon incoming clusterInG2OFormat acknowledgement messages
        void acknowledgeReplaceClusterWithG2OFormat(std::vector<uint8_t> data); //!< message that is invoked upon incoming replaceClusterWithG2OFormat acknowledgement messages
        //void incomingMapMessage(std::vector<uint8_t> data); //!< message that is invoked upon incoming map acknowledgement messages
        void acknowledgeSoftwareDetails(std::vector<uint8_t> data); //!< message that is invoked upon incoming softwaredetails acknowledgement messages
        void acknowledgeMarkerPosPacketStartStop(std::vector<uint8_t> data); //!< message that is invoked upon incoming markerpos start/stop acknowledgement messages
        void outputDriftCorrectionsMissed(std::vector<uint8_t> data); //!< message that is invoked upon incoming drift corrections missed acknowledgement messages
        void acknowledgeToggleArucoMarkerMode(std::vector<uint8_t> data); //!< message that is invoked upon incoming toggle aruco marker mode acknowledgement messages
        void outputArucoMarker(std::vector<uint8_t> data); //!< message that is invoked upon incoming aruco marker acknowledgement messages
        void outputMapLoaded(std::vector<uint8_t> data); //!< message that is invoked upon incoming map loaded messages
        void acknowledgeBufferLength(std::vector<uint8_t> data); //!< message that is invoked upon incoming buffer length acknowledgement messages
        void acknowledgeBufferProgress(std::vector<uint8_t> data); //!< message that is invoked upon incoming buffer progress acknowledgement messages
        void acknowledgeRecordingMsg(std::vector<uint8_t> data); //!< message that is invoked upon incoming recording acknowledgement messages
        void acknowledgeFrameCaptureMsg(std::vector<uint8_t> data); //!< message that is invoked upon incoming recording acknowledgement messages

        //get/send map
        bool filesSuccessfullyTransferred = true;  //!< Boolean that holds whether the file(s) have been successfully transferred
        FILE *mapSharingFile;  //!< Contains pointer to a file for either loading or saving
        std::string mapSharingPath_;  //!< Contains the path where the file is or should be located
        bool isInProgress = false;  //!< Boolean that holds whether any filesharing is currently in progress.
        uint32_t totalMessagesToBeTransferred_ = 0;   //!<  Holds the total amount of messages that are to be transferred
        uint32_t msgcounter = 0;   //!< Holds how many messages have been transferred thus far
        int totalFileSize_ = 0;   //!< Holds the total file size in bytes
        int totalsent = 0;  //!< Holds the total file size in bytes that has been sent
        int mapStrategy = 0;  //!< Holds the strategy of the map importing, 0 = replace, 1 = merge

        // get recordings
        bool recordingsSuccessfullyTransferred = true;  //!< Boolean that holds whether the file(s) have been successfully transferred
        FILE *recordingsFile;  //!< Contains pointer to a file for either loading or saving
        std::string recordingsPath_;  //!< Contains the path where the file is or should be located
        bool recordingsIsInProgress = false;  //!< Boolean that holds whether any filesharing is currently in progress.
        uint32_t totalRecordingsMessagesToBeTransferred_ = 0;   //!<  Holds the total amount of messages that are to be transferred
        uint32_t recordingsMsgcounter = 0;   //!< Holds how many messages have been transferred thus far
        int totalRecordingsFileSize_ = 0;   //!< Holds the total file size in bytes
        int totalRecordingsBytesSent = 0;  //!< Holds the total file size in bytes that has been sent
        std::vector<uint8_t> recordingIndexes_;

        static const int bufferSize = 1000000; //!< Holds the size of the buffer

        bool retrieveFirstMapPiece();  //!< Method to start the retrieval of the map
        void retrievedMapPiece(std::vector<uint8_t> receivedCommand_);  //!< Method that is invoked upon receiving a piece of the map
        void retrieveNextMapPiece();   //!< Method to start the retrieval of the next piece

        bool retrieveFirstRecordingsPiece();  //!< Method to start the retrieval of the recording
        //void retrievedRecordingsPiece(std::vector<uint8_t> receivedCommand_);  //!< Method that is invoked upon receiving a piece of the recording
        void retrieveNextRecordingsPiece();   //!< Method to start the retrieval of the next piece

        /**
         * @brief      Method to check whether a string ends with a certain substring
         * @param fullString the complete string that is to be checked
         * @param endingPart the potential end of the string
         * @return bool true if the string ends with the endingPart
         */
        static bool doesStringEndWith(const std::string& fullString, const std::string& endingPart)
        {
            return fullString.size() >= endingPart.size() && 0 == fullString.compare(fullString.size()-endingPart.size(), endingPart.size(), endingPart);
        }
        
        bool sendFirstMapPiece();  //!< Method to start the sending of the map
        void retrievedMapAck(std::vector<uint8_t> receivedCommand_);   //!< Method that is invoked when the sensor received a piece
        //void sendNextMapPiece();  //!< Method to start the sending of the next piece

        /**
         * @brief      Method to check whether a file exists
         * @param Filename the full path to a file that has to be checked
         * @return bool true if the file exists
         */
        static bool FileExists( const std::string &Filename ){return access( Filename.c_str(), 0 ) == 0;};

        _progressCallBack               progressCallBack                = nullptr;  //!< Variable that holds the callback that is to be invoked for progress messages
        _doneCallBack                   doneCallBack                    = nullptr;  //!< Variable that holds the callback that is to be invoked for done messages
        _statusCallBack                 statusCallBack                  = nullptr;  //!< Variable that holds the callback that is to be invoked for status messages
        _progressCallBack               recordingsProgressCallBack      = nullptr;  //!< Variable that holds the callback that is to be invoked for progress messages
        _doneCallBack                   recordingsDoneCallBack          = nullptr;  //!< Variable that holds the callback that is to be invoked for done messages
        _statusCallBack                 recordingsStatusCallBack        = nullptr;  //!< Variable that holds the callback that is to be invoked for status messages

        // STREAMING MSGS
        _heartBeatCallBack              heartBeatCallBack               = nullptr;  //!< Variable that holds the callback that is to be invoked for heartbeat messages
        _correctedPoseCallBack          correctedPoseCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for corrected pose messages
        _uncorrectedPoseCallBack        uncorrectedPoseCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for uncorrected pose messages
        _diagnosticsCallBack            diagnosticsCallBack             = nullptr;  //!< Variable that holds the callback that is to be invoked for diagnostics messages
        _driftCorrectionCallBack        driftCorrectionCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for drift correction messages
        _qualityEstimateCallBack        qualityEstimateCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for quality estimate messages
        _lineFollowerCallBack           lineFollowerCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for line follower messages
        _markerPosPacketCallBack        markerPosPacketCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for marker pos messages
        _consoleOutputCallback          consoleOutputCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for console output messages
                
        // CMD - ACK MSGS
        _acknowledgementCallBack        accQRMappingCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for acc QR mapping messages
        _acknowledgementCallBack        absoluteModeCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for absolute mode messages
        _acknowledgementCallBack        recordingModeCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for recording mode messages
        _acknowledgementCallBack        idleModeCallBack                = nullptr;  //!< Variable that holds the callback that is to be invoked for idle mode messages
        _acknowledgementCallBack        rebootModeCallBack              = nullptr;  //!< Variable that holds the callback that is to be invoked for reboot mode messages
        _acknowledgementCallBack        calibrationModeCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for calibration mode messages
        _ipAddressCallBack              ipAddressCallBack               = nullptr;  //!< Variable that holds the callback that is to be invoked for ip address messages
        _sampleRateCallBack             sampleRateCallBack              = nullptr;  //!< Variable that holds the callback that is to be invoked for sample rate messages
        _acknowledgementCallBack        clearQRLibraryCallBack          = nullptr;  //!< Variable that holds the callback that is to be invoked for clear QR lib messages
        _serialNumberCallBack           serialNumberCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for serial number messages
        _acknowledgementCallBack        clearClusterLibraryCallBack     = nullptr;  //!< Variable that holds the callback that is to be invoked for clear cluster lib messages
        _softwareVersionCallBack        softwareVersionCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for software version messages
        _tcpIPInformationCallBack       tcpIPInformationCallBack        = nullptr;  //!< Variable that holds the callback that is to be invoked for TCP/IP info messages
        _acknowledgementCallBack        expertModeCallBack              = nullptr;  //!< Variable that holds the callback that is to be invoked for expert mode messages
        _removeQRCallBack               removeQRCallBack                = nullptr;  //!< Variable that holds the callback that is to be invoked for remove QR messages
        _acknowledgementCallBack        recoveryModeCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for recovery mode messages
        _removeClusterCallBack          removeClusterCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for remove cluster messages
        _secondaryLineFollowerCallBack  secondaryLineFollowerCallBack   = nullptr;  //!< Variable that holds the callback that is to be invoked for secondary line follower messages
        _addQRCallBack                  addQRCallBack                   = nullptr;  //!< Variable that holds the callback that is to be invoked for add QR messages
        _dateTimeCallBack               dateTimeCallBack                = nullptr;  //!< Variable that holds the callback that is to be invoked for date/time messages
        _poseCallBack                   sensorPoseCallBack              = nullptr;  //!< Variable that holds the callback that is to be invoked for sensor pose messages
        _poseCallBack                   sensorMountPoseCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for mount pose messages
        _acknowledgementCallBack        toggleMappingCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for toggle mapping messages
        _acknowledgementCallBack        toggleLineFollowingCallBack     = nullptr;  //!< Variable that holds the callback that is to be invoked for toggle line following messages
        _setUDPSettingsCallBack         setUDPSettingsCallBack          = nullptr;  //!< Variable that holds the callback that is to be invoked for UDP settings messages
        _clusterInG2OFormatCallBack     clusterInG2OFormatCallBack      = nullptr;  //!< Variable that holds the callback that is to be invoked for clusterInG2OFormat messages
        _acknowledgementCallBack        replaceClusterG2OCallBack       = nullptr;  //!< Variable that holds the callback that is to be invoked for replaceClusterWithG2OFormat messages
        _softwareDetailsCallBack        softwareDetailsCallBack         = nullptr;  //!< Variable that holds the callback that is to be invoked for software details messages
        _acknowledgementCallBack        markerPosStartStopCallBack      = nullptr;  //!< Variable that holds the callback that is to be invoked for marker pos start/stop messages
        _driftCorrectionsMissedCallBack driftCorrectionsMissedCallBack  = nullptr;  //!< Variable that holds the callback that is to be invoked for drift corrections missed messages
        _acknowledgementCallBack        toggleArucoMarkerModeCallBack   = nullptr;  //!< Variable that holds the callback that is to be invoked for toggle aruco marker mode messages
        _arucoMarkerCallBack            arucoMarkerCallBack             = nullptr;  //!< Variable that holds the callback that is to be invoked for aruco marker messages
        _mapLoadedCallBack              mapLoadedCallBack               = nullptr;  //!< Variable that holds the callback that is to be invoked for map loaded messages
        _bufferLengthCallBack           bufferLengthCallBack            = nullptr;  //!< Variable that holds the callback that is to be invoked for buffer length messages
        _bufferProgressCallBack         bufferProgressCallBack          = nullptr;  //!< Variable that holds the callback that is to be invoked for buffer progress messages
        _recordingListCallBack          recordingListCallBack           = nullptr;  //!< Variable that holds the callback that is to be invoked for recording list messages
        _deleteRecordingsCallBack       deleteRecordingsCallBack        = nullptr;  //!< Variable that holds the callback that is to be invoked for delete recordings acknowledgement messages

        // ACC QR MAPPING ACK //
        std::mutex accQRMappingAckMutex;  //!< Mutex that guards the variables related to ACC QR Mapping
        std::condition_variable accQRMappingAckCV;  //!< Condition variable primitive for ACC QR Mapping
        Acknowledgement receivedAccQRMappingAck;  //!< Variable that holds the latest received acknowledgement for ACC QR Mapping
        // END OF ACC QR MAPPING ACK //

        // ABS MODE ACK //
        std::mutex absoluteModeAckMutex;  //!< Mutex that guards the variables related to absolute mode
        std::condition_variable absoluteModeAckCV;  //!< Condition variable primitive for absolute mode
        Acknowledgement receivedAbsoluteModeAck;  //!< Variable that holds the latest received acknowledgement for absolute mode
        // END OF ABS MODE ACK //

        // RECORDING MODE ACK //
        std::mutex recordingModeAckMutex;  //!< Mutex that guards the variables related to recording mode
        std::condition_variable recordingModeAckCV;  //!< Condition variable primitive for recording mode
        Acknowledgement receivedRecordingModeAck;  //!< Variable that holds the latest received acknowledgement for recording mode
        // END OF RECORDING MODE ACK //

        // IDLE MODE ACK //
        std::mutex idleModeAckMutex;  //!< Mutex that guards the variables related to idle mode
        std::condition_variable idleModeAckCV;  //!< Condition variable primitive for idle mode
        Acknowledgement receivedIdleModeAck;  //!< Variable that holds the latest received acknowledgement for idle mode
        // END OF IDLE MODE ACK //

        // REBOOT MODE ACK //
        std::mutex rebootModeAckMutex;  //!< Mutex that guards the variables related to reboot mode
        std::condition_variable rebootModeAckCV;  //!< Condition variable primitive for reboot mode
        Acknowledgement receivedRebootModeAck;  //!< Variable that holds the latest received acknowledgement for reboot mode
        // END OF REBOOT MODE ACK //

        // CALIBRATION MODE ACK //
        std::mutex calibrationModeAckMutex;  //!< Mutex that guards the variables related to calibration mode
        std::condition_variable calibrationModeAckCV;  //!<  Condition variable primitive for calibration mode
        Acknowledgement receivedCalibrationModeAck;  //!< Variable that holds the latest received acknowledgement for calibration mode
        // END OF CALIBRATION MODE ACK //

        // IP ADDR ACK //
        std::mutex ipAddressAckMutex;  //!< Mutex that guards the variables related to ip address messages
        std::condition_variable ipAddressAckCV;  //!< Condition variable primitive for ip address messages
        IPAddressExtended receivedIPAddress;  //!< Variable that holds the latest received IPAddressExtended
        // END OF IP ADDR ACK //

        // SAMPLE RATE ACK //
        std::mutex sampleRateAckMutex;  //!< Mutex that guards the variables related to sample rate messages
        std::condition_variable sampleRateAckCV;  //!< Condition variable primitive for sample rate messages
        SampleRate receivedSampleRate;  //!< Variable that holds the latest received SampleRate
        // END OF SAMPLE RATE ACK //

        // CLEAR QR LIB ACK //
        std::mutex clearQRLibraryAckMutex;  //!< Mutex that guards the variables related to clear QR Lib messages
        std::condition_variable clearQRLibraryAckCV;  //!< Condition variable primitive for clear QR Lib messages
        Acknowledgement receivedClearQRLibraryAck;  //!< Variable that holds the latest received acknowledgement for clear QR Lib
        // END OF CLEAR QR LIB ACK //

        // SERIALNUMBER ACK //
        std::mutex serialNumberAckMutex;  //!< Mutex that guards the variables related to serial number messages
        std::condition_variable serialNumberAckCV;  //!<  Condition variable primitive for serial number messages
        SerialNumber receivedSerialNumber;  //!< Variable that holds the latest received SerialNumber
        // END OF SERIALNUMBER ACK //

        // CLEAR CLUSTER LIB ACK //
        std::mutex clearClusterLibraryAckMutex;  //!< Mutex that guards the variables related to clear cluster lib messages
        std::condition_variable clearClusterLibraryAckCV;  //!< Condition variable primitive for clear cluster lib messages
        Acknowledgement receivedClearClusterLibraryAck;  //!< Variable that holds the latest received acknowledgement for clear cluster lib
        // END OF CLEAR CLUSTER LIB ACK //

        // SOFTWARE VERSION ACK //
        std::mutex softwareVersionAckMutex;  //!< Mutex that guards the variables related to software version messages
        std::condition_variable softwareVersionAckCV;  //!< Condition variable primitive for software version messages
        SoftwareVersion receivedSoftwareVersion;  //!< Variable that holds the latest received SoftwareVersion
        // END OF SOFTWARE VERSION ACK //

        // TCP/IP Information ACK //
        std::mutex tcpIPInformationAckMutex;  //!< Mutex that guards the variables related to TCP/IP information messages
        std::condition_variable tcpIPInformationAckCV;  //!< Condition variable primitive for TCP/IP information messages
        TCPIPInformation receivedTCPIPInformation;  //!< Variable that holds the latest received TCPIPInformation
        // END OF TCP/IP Information ACK //

        // MOUNT POSE //
        std::mutex mountPoseMutex;  //!< Mutex that guards the variables related to mount pose messages
        std::condition_variable mountPoseCV;  //!< Condition variable primitive for mount pose messages
        Pose receivedMountPose;  //!< Variable that holds the latest received mount Pose
        // END OF MOUNT POSE //

        // EXPERT MODE ACK //
        std::mutex expertModeAckMutex;  //!< Mutex that guards the variables related to expert mode
        std::condition_variable expertModeAckCV;  //!< Condition variable primitive for expert mode
        Acknowledgement receivedExpertModeAck;  //!< Variable that holds the latest received acknowledgement for expert mode
        // END OF EXPERT MODE ACK //

        // REMOVE QR ACK //
        std::mutex removeQRAckMutex;  //!< Mutex that guards the variables related to remove QR messages
        std::condition_variable removeQRAckCV;  //!< Condition variable primitive for remove QR messages
        uint16_t receivedRemoveQRAck;  //!< Variable that holds the latest received ID or errorcode for remove QR(max uint16_t indicates an error)
        // END OF REMOVE QR ACK //

        // RECOVERY MODE ACK //
        std::mutex recoveryModeAckMutex;  //!< Mutex that guards the variables related to recovery mode
        std::condition_variable recoveryModeAckCV;  //!< Condition variable primitive for recovery mode
        Acknowledgement receivedRecoveryModeAck;  //!< Variable that holds the latest received acknowledgement for recovery mode
        // END OF RECOVERY MODE ACK //

        // REMOVE CLUSTER ACK //
        std::mutex removeClusterAckMutex;  //!< Mutex that guards the variables related to remove cluster messages
        std::condition_variable removeClusterAckCV;  //!< Condition variable primitive for remove cluster messages
        uint16_t receivedRemoveClusterAck;  //!< Variable that holds the latest received ID or errorcode for remove cluster(max uint16_t indicates an error)
        // END OF REMOVE CLUSTER ACK //

        // SECONDARY LINE FOLLOWER OUTPUT ACK //
        std::mutex secondaryLineFollowerOutputMutex;  //!< Mutex that guards the variables related to secondary line follower output messages
        std::condition_variable secondaryLineFollowerOutputCV;  //!< Condition variable primitive for secondary line follower output messages
        LineFollowerData receivedSecondaryLineFollowerOutput;  //!< Variable that holds the latest received LineFollowerData for secondary line following
        // END OF SECONDARY LINE FOLLOWER OUTPUT ACK //

        // ADD QR ACK //
        std::mutex addQRMutex;  //!< Mutex that guards the variables related to add QR messages
        std::condition_variable addQRCV;  //!< Condition variable primitive for add QR messages
        AddQRResult receivedAddQRAck;  //!< Variable that holds the latest received AddQRResult
        // END OF ADD QR ACK //

        // DATETIME ACK //
        std::mutex dateTimeMutex;  //!< Mutex that guards the variables related to date/time messages
        std::condition_variable dateTimeCV;  //!< Condition variable primitive for date/time messages
        DateTime receivedDateTimeAck;  //!< Variable that holds the latest received DateTime
        // END OF DATETIME ACK //

        // SENSOR POSE //
        std::mutex sensorPoseMutex;  //!< Mutex that guards the variables related to sensor pose messages
        std::condition_variable sensorPoseCV;  //!< Condition variable primitive for sensor pose messages
        Pose receivedSensorPose;  //!< Variable that holds the latest received Pose
        // END OF SENSOR POSE //

        // MAPPING ACK //
        std::mutex mappingAckMutex;  //!< Mutex that guards the variables related to mapping messages
        std::condition_variable mappingAckCV;  //!< Condition variable primitive for mapping messages
        Acknowledgement receivedMappingAck;  //!< Variable that holds the latest received acknowledgement for mapping
        // END OF MAPPING ACK //

        // LINEFOLLOWING ACK //
        std::mutex lineFollowingAckMutex;  //!< Mutex that guards the variables related to line follower toggling
        std::condition_variable lineFollowingAckCV;  //!< Condition variable primitive for line follower toggling
        Acknowledgement receivedLineFollowingAck;  //!< Variable that holds the latest received acknowledgement for line follower toggling
        // END OF LINEFOLLOWING ACK //

        // SETUDPSETTINGS ACK //
        std::mutex setUDPSettingsAckMutex;  //!< Mutex that guards the variables related to set UDP settings messages
        std::condition_variable setUDPSettingsAckCV;  //!< Condition variable primitive for set UDP settings messages
        UDPInfo receivedSetUDPSettingsAck;  //!< Variable that holds the latest received UDPInfo
        // END OF SETUDPSETTINGS ACK //

        // CLUSTERING2O ACK //
        std::mutex clusterInG2OAckMutex;  //!< Mutex that guards the variables related to clusterInG2OFormat messages
        std::condition_variable clusterInG2OAckCV;  //!< Condition variable primitive for clusterInG2OFormat messages
        std::vector<uint8_t> receivedClusterInG2OAck;  //!< Variable that holds the latest received clusterInG2OFormat
        // END OF CLUSTERING2O ACK //

        // REPLACECLUSTERG2O ACK //
        std::mutex replaceClusterWithG2OAckMutex;  //!< Mutex that guards the variables related to replaceClusterWithG2O messages
        std::condition_variable replaceClusterWithG2OAckCV;  //!< Condition variable primitive for replaceClusterWithG2O messages
        Acknowledgement receivedReplaceClusterWithG2OAck;  //!< Variable that holds the latest received acknowledgement for replaceClusterWithG2O messages
        // END OF REPLACECLUSTERG2O ACK //

        // SOFTWARE DETAILS ACK //
        std::mutex softwareDetailsAckMutex;  //!< Mutex that guards the variables related to software details messages
        std::condition_variable softwareDetailsAckCV;  //!< Condition variable primitive for software details messages
        SoftwareDetails receivedSoftwareDetails;  //!< Variable that holds the latest received SoftwareDetails
        // END OF SOFTWARE DETAILS ACK //

        // ARUCO MARKER MODE ACK //
        std::mutex arucoMarkerModeAckMutex;  //!< Mutex that guards the variables related to aruco marker mode toggling
        std::condition_variable arucoMarkerModeAckCV;  //!< Condition variable primitive for aruco marker mode toggling
        Acknowledgement receivedArucoMarkerModeAck;  //!< Variable that holds the latest received acknowledgement for toggling aruco marker mode
        // END OF ARUCO MARKER MODE ACK //

        // BUFFER LENGTH ACK //
        std::mutex bufferLengthAckMutex;  //!< Mutex that guards the variables related to buffer length messages
        std::condition_variable bufferLengthAckCV;  //!< Condition variable primitive for buffer length messages
        int receivedBufferLength;  //!< Variable that holds the latest received bufferlength
        // END OF BUFFER LENGTH ACK //

        // RECORDING LIST ACK //
        std::mutex recordingListAckMutex;  //!< Mutex that guards the variables related to buffer length messages
        std::condition_variable recordingListAckCV;  //!< Condition variable primitive for buffer length messages
        std::vector<std::string> receivedRecordingList;  //!< Variable that holds the latest received bufferlength
        // END OF RECORDING LIST ACK //

        // DELETE RECORDINGS ACK //
        std::mutex deleteRecordingsAckMutex;  //!< Mutex that guards the variables related to buffer length messages
        std::condition_variable deleteRecordingsAckCV;  //!< Condition variable primitive for buffer length messages
        DeleteRecordingsResult deleteRecordingsResult;  //!< Variable that holds the latest received bufferlength
        // END OF DELETE RECORDINGS  ACK //

        // CAPTURE FRAME ACK //
        std::mutex captureFrameAckMutex;  //!< Mutex that guards the variables related to buffer length messages
        std::condition_variable captureFrameAckCV;  //!< Condition variable primitive for buffer length messages
        std::vector<uint8_t> captureFrameResult;  //!< Variable that holds the latest received bufferlength
        // END OF CAPTURE FRAME ACK //

        int timeOutInSecs = 3;
};

#endif