/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef ACCERION_UPDATESERVICE_H
#define ACCERION_UPDATESERVICE_H
#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <cmath>
#include <ctime>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include "TypeDef.h"
#include "commands.h"
#include "structs.h"
#include "callbacks.h"
#include "TCPClient.h"
#include "UDPReceiver.h"
#include "TCPClient.h"
#include "CRC8.h"

#ifdef __linux__ 
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/// AccerionUpdateService provides an object-oriented "interface". Methods invoked on this object are relayed to the sensor over the network and received messages are forwarded to the registered callbacks
class AccerionUpdateService
{
    public:
        /**
         * \brief Constructor for AccerionUpdateService object, can be invoked by you, but recommended use is through AccerionSensorManager
         * \param ip address of the sensor that is to be connected to
         * \param serial of the sensor that is to be connected to
         * \param localIP is the ip address of the local machine in case unicasting is used
         **/
        AccerionUpdateService(Address ip, std::string serial, Address localIP);
        /// AccerionSensor Destructor
        ~AccerionUpdateService();

        /**
         * \brief Method to register a callback to the Heartbeat message
         * \param hbCallback callback method which has to be invoked on an incoming Heartbeat message
         **/
        void subscribeToHeartBeat(_heartBeatCallBack hbCallback);

        /**
         * \brief Method to register a callback to the Diagnostics message
         * \param diagCallback callback method which has to be invoked on an incoming Diagnostics message
         **/
        void subscribeToDiagnostics(_diagnosticsCallBack diagCallback);

        /**
         * \brief Method to retrieve the logs from the sensor
         * \param destinationPath string full path to where the file should be stored including name
         * \param progressCB callback method to be invoked to inform about progress
         * \param doneCB callback method to be invoked after a success or failure
         * \param statusCB callback method to be invoked to inform about status, see FileSenderStatus
         **/
        bool getLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB);

        /**
         * \brief Method to retrieve the backup logs from the sensor
         * \param destinationPath string full path to where the file should be stored including name
         * \param progressCB callback method to be invoked to inform about progress
         * \param doneCB callback method to be invoked after a success or failure
         * \param statusCB callback method to be invoked to inform about status, see FileSenderStatus
         **/
        bool getBackupLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB);

    private:
        bool debugMode_ = false; //!< Flag used for debugging

        UDPReceiver    *    udpReceiver; //!< lass used to receive messages coming from a sensor 
        TCPClient      *    tcpClient; //!< Communication class for sending and receiving messages 
        ConnectionType      connectionType; //!< Holds the preferred connection type to the sensor
        Address             localIP_; //!< Holds the local IP address, this is sent to sensor in case of unicasting

        bool runTCP = true; //!< Value that is checked by TCP thread to see if it should run 

        CRC8                    crc8_;  //!< used for verification purposes
        std::vector<uint8_t>    receivedCommand_;  //!<  Holds incoming UDP command (size depends on UDP command ID)
        uint8_t                 receivedCommandID_;   //!< Holds the ID of the received command, see commands.h
        bool                    lastMessageWasBroken_; //!<  Keeps track whether the last message was split up/broken
        uint32_t                sensorSerialNumber_ = DEFAULT_SERIAL_NUMBER; //!< Holds the unique serial number of the Jupiter unit
        uint32_t                receivedSerialNumber_; //!<  Holds received serial number from incoming UDP message, used to determine if message is intended for this unit
        uint8_t                 receivedCRC8_; //!<  Holds incoming CRC8 code (in 0xD8) , compared with CRC code computed at Jupiter side for error-checking
        bool                    messageReady_; //!<  True if an incoming UDP message is ready, False otherwise.
        std::vector<Command>    outgoingCommands; //!< Vector of outgoing commands
        std::mutex              outgoingCommandsMutex;  //!< Mutex guarding the vector of outgoing commands
        uint8_t                 commandIDToBeSent_;

        void runTCPCommunication();  //!< Method that is ran on a seperate thread that checks for incoming messages and sends out messages */

        /**
         * @brief      Checks the received UDP message, sets the necessary values
         *             and finally sends an acknowledgement message
         * @param incomingCommands : vector of commands that have been parsed by parseMessages(std::vector<Command>, std::vector<uint8_t>)
         * @param outgoingCommands : vector of commands that are to be send.
         */
        void readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands);

        /**
         * @brief      parseMessages reads incomming network packages and dissects them. If a command is competely received it is put in the commands vector supplied in the param
         *
         * @param[in]  commands         vector of commands
         * @param[in]  receivedMessage_ vector of data
         */
        void parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_);

        /**
         * @brief      clearOutgoingCommands clears the internal vector of outoing commands
         */
        void clearOutgoingCommands(){ outgoingCommands.clear();};

        // STREAMING MSGSsendSerialNumberAcknowledgeMessage
        void outputHeartBeat(std::vector<uint8_t> data); //!< Method invoked when a heartbeat is identified in readMessages
        void outputDiagnostics(std::vector<uint8_t> data);  //!< Method invoked when a diagnostics message is identified in readMessages

        // CMD - ACK MSGS
        //void incomingLogsMessage(std::vector<uint8_t> data); //!< Method invoked when a logs message is identified in readMessages

        //get logs
        bool filesSuccesfullyTransferred = true; //!< Holds whether the file transfer is/was succesfully
        FILE *logsFile;  //!< Pointer to fileobject where logs are written to
        std::string logsPath_;  //!< Path to where the logs should be stored
        bool isInProgress = false;  //!< Holds whether there is a filetransfer in progress
        uint32_t totalMessagesToBeTransfered_ = 0;  //!< The total amount of messages it takes to send/retrieve the file
        uint32_t msgcounter = 0;  //!< The current message that is being transferred
        int totalFileSize_ = 0;  //!< The total size of the file in bytes
        int totalsent = 0;  //!< Total amount of bytes that have been sent
        static const int bufferSize = 1000000; //!< The buffersize used in transferring files

        bool retrieveFirstLogPiece(); //!< Method to retrieve the first log piece of the sensor
        void retrievedLogPiece(std::vector<uint8_t> receivedCommand_);  //!< Method that is invoked when a log piece has been sent by the sensor
        void retrieveNextLogPiece();  //!< Method to request the next log piece of the sensor

        /**
         * \brief Method to check whether a string ends with another string
         * 
         * \param fullString the full string
         * \param endingPart the part the string should end with
         * \return bool returns true when the full string ends with the part string
         */
        static bool doesStringEndWith(const std::string& fullString, const std::string& endingPart)
        {
            return fullString.size() >= endingPart.size() && 0 == fullString.compare(fullString.size()-endingPart.size(), endingPart.size(), endingPart);
        }

        /**
         * \brief Method to check whether a file exists
         * 
         * \param Filename : the full path to the file
         * \return bool returns true when the file exists, false otherwise
         */
        static bool FileExists( const std::string &Filename ){return access( Filename.c_str(), 0 ) == 0;};

        _progressCallBack               progressCallBack                = nullptr; //!< callback that has to be invoked to notify of progress (0-100)
        _doneCallBack                   doneCallBack                    = nullptr; //!< callback that has to be invoked to notify when done ( either true(success) or false(failure))
        _statusCallBack                 statusCallBack                  = nullptr; //!< callback that has to be invoked to notify of the latest status
        //end of get/send map

        // STREAMING MSGS
        _heartBeatCallBack              heartBeatCallBack               = nullptr; //!< callback that has to be invoked upon incoming heartbeat messages
        _diagnosticsCallBack            diagnosticsCallBack             = nullptr; //!< callback that has to be invoked upon incoming diagnostics messages

};

#endif