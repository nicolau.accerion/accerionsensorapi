/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <list>
#include <string>
#include <iostream>
#include <vector>
#include <thread>
#include <sstream>
#include "TypeDef.h"
#include "commands.h"
#include "CRC8.h"
#include "AccerionUpdateService.h"
#include "UDPReceiver.h"
#include "UDPTransmitter.h"


#ifdef __linux__ 
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/// AccerionUpdateServiceManager class is responsible for detecting update services of sensors in the local subnet and providing access to them
class AccerionUpdateServiceManager
{

    public:
        /* Static access method. */
        static AccerionUpdateServiceManager* getInstance();

        /**
         * \brief method to list all UpdateServices
         * \return a list that contains pairs of IP addresses and serial numbers
         * 
         * This class listens to broadcast messages on a certain port. It tries to identify
         * different UpdateServices and adds them to a list. This method returns that list and gives 
         * you access to the IP addresses and serial numbers of all the found UpdateServices.
         **/
        std::list<std::pair<Address, std::string>> getAllUpdateServices();

        /**
         * \brief method to get UpdateService object based on its IP address
         * \param sensorIP struct that contains the IP address of the UpdateServices you'd like to conect to
         * \param localIP struct containing the local IP address, use an address the UpdateServices can access
         * \return UpdateService object which allows you to interact with that UpdateService.
         * 
         **/
        AccerionUpdateService* getAccerionUpdateServiceByIP(Address sensorIP, Address localIP);

        /**
         * \brief method to get UpdateService object based on its serial number
         * \param serial contains the serial of the UpdateService you'd like to conect to
         * \param localIP struct containing the local IP address, use an address the UpdateService can access
         * \return UpdateService object which allows you to interact with that UpdateService.
         * 
         **/
        AccerionUpdateService* getAccerionUpdateServiceBySerial(std::string serial, Address localIP);

    private:
        /**
         * \brief  Private constructor to prevent instancing. 
         **/
        AccerionUpdateServiceManager();
        /** 
         * \brief Instantation prevention 
         */ 
        AccerionUpdateServiceManager(const AccerionUpdateServiceManager&) = delete;
        /** 
         * \brief Instantation prevention 
         */ 
        AccerionUpdateServiceManager& operator=(const AccerionUpdateServiceManager&) = delete;

        CRC8 crc8_; //!< Used for message verification
        bool debugMode_ = false; //!< Flag used for debugging purposes
 
        std::list<std::pair<Address, std::string>> updateServices; //!< list of pairs that contain the ip address and serial number for each detected sensor
        std::vector<uint8_t>    receivedCommand_; //!< Holds incoming UDP command (size depends on UDP command ID)
        uint8_t                 receivedCommandID_;  //!< Holds the commandID of the received command
        bool                    lastMessageWasBroken_; //!< Keeps track whether the last message was split up/broken
        uint32_t                sensorSerialNumber_ = DEFAULT_SERIAL_NUMBER; //!< Holds the unique serial number of the Jupiter uni
        uint32_t                receivedSerialNumber_; //!<Holds received serial number from incoming UDP message, used to determine if message is intended for this unit
        uint8_t                 receivedCRC8_; //!< Holds incoming CRC8 code (in 0xD8) , compared with CRC code computed at Jupiter side for error-checking
        bool                    messageReady_; //!< True if an incoming UDP message is ready, False otherwise.

        /// Method that runs on separate thread and receives messages
        void runUDPCommunication();

        /**
         * @brief      parseMessages reads incomming network packages and dissects them. If a command is competely received it is put in the commands vector supplied in the param
         *
         * @param[in]  commands         vector of commands
         * @param[in]  receivedMessage_ vector of data
         */
        void parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_);

};


