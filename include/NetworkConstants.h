/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef NETWORKCONSTANTS_H_
#define NETWORKCONSTANTS_H_

#include <string>

namespace NetworkConstants
{                                                              
    static constexpr unsigned int   maximumNumOfBytesInUDPMessage   = 32000; //!< Maximum amount of bytes in a UDP message
    static constexpr unsigned int   TCPBufferSize                   = 1000000; //!< Buffer size for TCP specific. A too smal buffersize might result in issues when transferring files.                                                                  
    static int                      APIThroughput                   = 300; //!< The throughput the API has to adhere to. Note this is an upper limit. It will slow the communicaiton threads down.
}

#endif
