/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#ifndef PROFILETIMER_H_
#define PROFILETIMER_H_

#include <vector>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <chrono>
#include <utility>

#define MAX_N_SECTIONS 20  //!< Maximum number of sections to be profiled

/** 
 * @brief   Class for usage of timers for profiling purposes; it is assumed that at every loop, same number of parts will be profiled with the same order
 */
class ProfileTimer
{
public:
    ProfileTimer(const std::string& mainName, bool keepHistogram = false);

    ~ProfileTimer();

    void startLoopTime();
    void endLoopTime();
    void startAbsTime();
    void endAbsTime();
    void storeRelTime(const std::string& sectionName);
    // void storeAbsTime(const std::string& sectionName);

    void outputProfileDetails();
    void outputThroughputDetails();

    float computeCurrentThroughput();
    float computeAverageThroughput();

    float getCurrentThroughput(){return currThroughput_;};
    float getAverageThroughput(){return avgThroughput_;};

    long getTotalLoopTime(){return totalLoopTime_;};
private:
    unsigned int idxCurrPart_     = 0;
    unsigned int nPartsToProfile_ = 0;

    std::chrono::high_resolution_clock::time_point currTime_, prevTime_, loopStartTime_, loopEndTime_, absStartTime_, absEndTime_;

    long        totalAbsTime_;
    long        totalLoopTime_;

    long        currentDurations_[MAX_N_SECTIONS];
    double      meanDurations_[MAX_N_SECTIONS];
    std::string sectionNames_[MAX_N_SECTIONS];
    std::string mainName_;
    long        loopCount_;
    float       avgThroughput_, currThroughput_, avgLoopThroughput_;


    int             histGridSz_;
    unsigned int    histSize_;
    bool            keepHistogram_;
    std::vector<unsigned int> histogramStepTimes_;
};


#endif
