/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef TCPRECEIVER_H_
#define TCPRECEIVER_H_

#include <iostream>
#include <cstring>
#include <vector>
#include <array>
#include <thread>
#include <chrono>
#include <sys/types.h>

#include "TypeDef.h"
#include "NetworkConstants.h"
#include "commands.h"
#include "CRC8.h"

#ifdef __linux__ 
    #include <unistd.h>
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>
    #include <fcntl.h>
    #pragma comment(lib, "Ws2_32.lib")
#endif

/**
 * @brief      
 */
class TCPClient
{
public:

    /**
     * @brief      Constructs a TCPCLient object
     * @param[in]  address the address to connect to
     * @param[in]  receivePort  The receive port
     */
    TCPClient(struct in_addr address, unsigned int receivePort);
    ~TCPClient(); //!< Destructor for TCPClient

    /**
     * \brief Internal Method used for connecting to the sensor
     * 
     */
    void connectToServer();

    /**
     * \brief Internal Method used for reading messages.
     * 
     * \return bool when message has been received
     */
    bool receiveMessage();

    /**
     * @brief      Method to transmit a vector of commands.
     *
     * @param      commands     A vector of Command objects
     */
    void sendMessages(std::vector<Command> &commands);

    /**
     * @brief      Gets a uint8_t pointer received message.
     *
     * @return     Pointer to the received message.
     */
#ifdef __linux__ 
    inline uint8_t* getReceivedMessage()
    {
        return &receivedMessage_[0];
    }
#elif _WIN32
    inline char* getReceivedMessage()
    {
        return &receivedMessage_[0];
    }
#endif

    /**
     * @brief      Gets the received number of bytes.
     *
     * @return     The received number of bytes.
     */
    inline int getReceivedNumOfBytes()
    {
      return receivedNumOfBytes_;
    }

    /**
     * @brief      get the value of the connected variable
     *
     * @return connected_ : boolean value
     */
    inline bool getConnected() { return connected_; };
    
    uint32_t sensorSerialNumber_; /**< Holds the unique serial number of the Jupiter unit*/
    
  private:
    /**
     * \brief Internal Method used for opening the socket.
     * 
     * \return bool for success/failure
     */
    bool openSocket();

    /**
     * \brief Internal Method used for closing the socket.
     */
    void closeSocket();

    /**
     * @brief      Transmits a TCP Message, pointed by transmittedMessage and
     *             has byte size transmittedNumOfBytes
     *
     * @param      transmittedMessage     The transmitted message
     * @param[in]  transmittedNumOfBytes  The transmitted number of bytes
     *
     * @return     Errno set by Linux system calls, 0 if no error
     */
    int transmitMessage(uint8_t *transmittedMessage, unsigned int transmittedNumOfBytes);

    /**
     * \brief Internal Method used for transmitting messages.
     * 
     * \return Errno set by transmitMessage(uint8_t, unsigned int) or 0
     */
    int transmitMessage();

    uint32_t formMessage(); //!< Method that wraps the message with all the required data, e.g. serial number, CRC etc.

    bool sendMessage();//!< Method to send a single message that is "loaded" by sendMessages(std::vector<Command> &commands);

    /**
     * @brief      sets the value of the connected variable
     *
     * @param[in] connected : boolean value to set the connected_ boolean variable
     */
    inline void setConnected(bool connected) { connected_ = connected;};

    unsigned int remoteReceivePort_;    //!< Holds the remote receive port to which the outgoing TCP messages are sent
#ifdef __linux__
    int socketEndpoint_;                //!< Holds the file descriptor for TCP socket
#elif _WIN32
    SOCKET socketEndpoint_;             //!< Holds the file descriptor for TCP socket
#endif

    struct sockaddr_in remoteAddress_;  //!< Holds the remote address to which outgoing TCP messages are sent

    bool open_; //!< Holds whether the socket is open or not
    bool connected_; //!< Holds whether there is a connection or not

    static constexpr unsigned int bufferSize_ = NetworkConstants::TCPBufferSize; //!< holds the buffer size which indicates the maximumnuber of bytes in a message

#ifdef __linux__ 
    uint8_t receivedMessage_[bufferSize_];          //!< Holds received message as a byte array
#elif _WIN32
    char receivedMessage_[bufferSize_];          //!< Holds received message as a byte array
#endif
    
    int receivedNumOfBytes_;                        //!< Holds number of bytes in received TCP message

    MessageTypes  transmittedMessageType_;      //!< Holds the type of the message that is to be transmitted
    std::vector<uint8_t> transmittedMessage_;   //!< Holds the entire outgoing TCP message
    uint32_t transmittedNumOfBytes_;            //!< Holds number of bytes in outgoing TCP message
    int tcpSettings_;                           //!< Holds track of tcp settings, e.g. intermittent, streaming

    uint32_t transmittedSerialNumber_;        //!< Holds the serial number as a single uint32_t
    uint8_t transmittedSerialNumberData_[4];  //!< Holds the byte array for outgoing serial number
    
    uint8_t transmittedCommandID_;            //!< Holds outgoing TCP command ID
    uint8_t transmittedCRC8_;                 //!< Holds CRC8 code (in 0xD8) of outgoing TCP message
    std::vector<uint8_t> transmittedData_;    //!< Holds outgoing UDP data (i.e. part of the message excluding serial number, command ID and CRC code)
    CRC8 crc8_; //!< Object of CRC8 class, used to determine CRC code (in 0xD8) 

    bool debugMode_ = false; //!< Turns on/off debug console messages 
};
#endif