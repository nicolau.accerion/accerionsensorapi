/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef UDPRECEIVER_H_
#define UDPRECEIVER_H_

#include <cstring>
#include <iostream>
#include <sys/types.h>
#include <errno.h>
#include "TypeDef.h"
#include "NetworkConstants.h"
#include "commands.h"

#ifdef __linux__ 
    #include <unistd.h>
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/**
 * @brief      Wrapper class that holds a UDP socket for receiving messages.
 */
class UDPReceiver
{
public:

    /**
     * @brief      Constructs a UDPReceiver object
     *
     * @param[in]  receivePort  The receive port
     */
    UDPReceiver(unsigned int receivePort);
    ~UDPReceiver(); //!< Destructor of UDPReceiver
    
    /**
     * @brief      Receives a single UDP message
     *
     * @return     True if UDP message is received, false otherwise
     */
    bool ReceiveMessage();

    /**
     * @brief      Method to set MulticastIPAddress
     *
     * @param      multicastAddress that is to be set
     * @return     True if set succesfully false otherwise
     */
    // bool setMulticastIPAddress(struct in_addr multicastAddress);

    /**
     * @brief      Gets a uint8_t pointer received message.
     *
     * @return     Pointer to the received message.
     */
#ifdef __linux__ 
    inline uint8_t* getReceivedMessage()
    {
        return &receivedMessage_[0];
    }
#elif _WIN32
    inline char* getReceivedMessage()
    {
        return &receivedMessage_[0];
    }
#endif

    /**
     * @brief      Gets the received number of bytes.
     *
     * @return     The received number of bytes.
     */
    inline int getReceivedNumOfBytes()
    {
        return receivedNumOfBytes_;
    }

    struct sockaddr_in thisAddress_;//!< Holds address of the Jupiter sensor
    struct sockaddr_in remoteAddress_;//!<  Holds address of machine from which UDP messages are coming, set to empty

private:
    static constexpr unsigned int bufferSize_ = NetworkConstants::maximumNumOfBytesInUDPMessage; //!< Maximum buffer size for UDP messages 
    unsigned int receivePort_; //!< Holds the receiving UDP socket
    int socketEndpoint_; //!< Holds the file descriptor of receiving UDP socket
    
    int receivedNumOfBytes_; //!< Holds number of bytes in received UDP message
    bool debugMode_ = false; //!< Turns on/off debug console messages

    #ifdef __linux__ 
        socklen_t socketLength_; //!< Holds size of UDP socket
        uint8_t receivedMessage_[bufferSize_]; //!< Holds received message as a byte array
    #elif _WIN32
        int socketLength_; //!< Holds size of UDP socket
        char receivedMessage_[bufferSize_]; //!< Holds received message as a byte array
    #endif

};
#endif