/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef UDPTRANSMITTER_H_
#define UDPTRANSMITTER_H_

#include <iostream>
#include <cstring>
#include <vector>
#include <array>
#include <thread>
#include <sys/types.h>

#include "TypeDef.h"
#include "Serialization.h"
#include "commands.h"
#include "CRC8.h"
#include "NetworkConstants.h"

#ifdef __linux__ 
    #include <unistd.h>
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/**
 * @brief      Wrapper class that holds a UDP socket for transmitting messages.
 */
class UDPTransmitter
{
public:

    /**
     * @brief      Constructs a UDPTransmitter object
     *
     * @param[in]  remoteReceivePort  The remote receive port to which the outgoing UDP messages are sent
     */
    UDPTransmitter(unsigned int remoteReceivePort);
    ~UDPTransmitter(); //!< Destructor of the UDP Transmitter

    /**
     * @brief      Transmits a UDP Message, pointed by transmittedMessage and
     *             has byte size transmittedNumOfBytes
     *
     * @param      transmittedMessage     The transmitted message
     * @param[in]  transmittedNumOfBytes  The transmitted number of bytes
     *
     * @return     True if UDP message is sent successfully, false otherwise
     */
    bool transmitMessage(uint8_t* transmittedMessage, unsigned int transmittedNumOfBytes);

    /**
     * @brief      Method to set the IP address that is to be transmitted to
     *
     * @param      ipAddress     The address that is to be transmitted to
     */
    void setIPAddress(struct in_addr ipAddress);

    /**
     * @brief      Method to get the IP address that is to be transmitted to
     *
     * @return      ipAddress     The address that is to be transmitted to
     */
    inline sockaddr_in getRemoteIPAddress(){return remoteAddress_;};

    /**
     * @brief      Method to transmit a vector of commands.
     *
     * @param      commands     A vector of Command objects
     */
    void sendMessages(std::vector<Command> &commands);

    uint32_t sensorSerialNumber_; //!< Serial number of the sensor that is to be transmitted to. A Wrong value will have the sensor ignore the message.

  private:
    unsigned int remoteReceivePort_; //!< Holds the remote receive port to which the outgoing UDP messages are sent.
    int socketEndpoint_; //!< Holds the file descriptor for UDP socket
    struct sockaddr_in remoteAddress_; //!< Holds the remote address to which outgoing UDP messages are sent (now set to Broadcast)

    static constexpr unsigned int bufferSize_ = NetworkConstants::maximumNumOfBytesInUDPMessage; //!< Maximum Buffer Size of UDP Messages
    bool debugMode_ = false; //!<  Turns on/off debug console messages

    uint32_t formMessage(); //!< Method that wraps the message with all the required data, e.g. serial number, CRC etc.

    bool sendMessage(); //!< Method to send a single message that is "loaded" by sendMessages(std::vector<Command> &commands);

    //int transmitMessage(); //!< Method to transmit a message over UDP.

    CRC8 crc8_;//!< CRC8 for message verification

    /*TRANSMITTED MESSAGE*/
    uint8_t   transmittedSerialNumberData_[4];  //!< Holds the byte array for outgoing serial number 
    uint8_t   transmittedTimeStamp_[8];         //!< Holds outgoing timestamp value as a byte array 
    uint32_t  transmittedSerialNumber_;         //!< Holds the serial number as a single uint32_t 
    uint8_t   transmittedCommandID_;            //!< Holds outgoing UDP command ID 
    uint32_t  transmittedNumOfBytes_;           //!< Holds number of bytes in outgoing UDP message 
    uint8_t   transmittedNumOfBytesData_[4];    //!< Holds number of bytes in outgoing UDP message in byte array
    uint8_t   transmittedCRC8_;                 //!< Holds CRC8 code (in 0xD8) of outgoing UDP message
    std::vector<uint8_t> transmittedData_;      //!< Holds outgoing UDP data (i.e. part of the message excluding serial number, command ID and CRC code)
    std::vector<uint8_t> transmittedMessage_;   //!< Holds the entire outgoing UDP message

    bool debugModeStreaming_ = false; //!< Flag used for debugging purposes


};
#endif
