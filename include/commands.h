/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef SENSOR_COMMANDS_H
#define SENSOR_COMMANDS_H
#include <iostream>
#include <map>
#include <vector>
#include <cmath>
#include "TypeDef.h"
#include "Serialization.h"

#define DEFAULT_SERIAL_NUMBER 0xFFFFFFFF //!< The default value of the Serial Number, not using the real serial number will result in messages being ignored

#define API_RECEIVE_PORT_UDP        13359 //!< The UDP port the API has to LISTEN to, e.g. the Jupiter talks over this port
#define JUPITER_RECEIVE_PORT_UDP    13360 //!< The UDP port the API has to SEND over, e.g. the Jupiter listens to this port
#define TCP_PORT                    13361  //!< The port the API communicates over with the sensor when using TCP

#define UPDATE_SERVICE_TRANSMIT_PORT_UDP 13362 //!< The UDP port the API has to LISTEN to, e.g. the UpdateService talks over this port
#define UPDATE_SERVICE_PORT_TCP 1989 //!< The port the API communicates over with the UpdateService when using TCP

/// Enum for sending/receiving boolean values over the network
enum CommandFlags
{
    cmdTrue              = 0x01,
    cmdFalse             = 0x02
};

/// Enum containing the position or size values of all the fields in a command
enum CommandFields
{
    SERIAL_NUMBER_BEGIN_BYTE    = 0, //!< Byte the serial number begins at in the message
    SENSOR_SERIAL_NUMBER_LENGTH = 4, //!< Length of the serial number in bytes
    COMMAND_ID_BEGIN_BYTE       = 4, //!< Byte the command id begins at in the message
    COMMAND_ID_LENGTH           = 1, //!< Length of the command id in bytes
    CRC_LENGTH                  = 1, //!< Length of the CRC in bytes
    MESSAGE_SIZE_LENGTH         = 4, //!< Length of the message size field
    PACKAGE_NO_LENGTH           = 4, //!< Length of the package number field
    PACKAGE_RESULT_LENGTH       = 1  //!< Length of the package result field
};

/// Enum containing all the commands that go to and from a sensor with their respective hex values
enum CommandIDs
{
    PRD_HEARTBEAT_INFO                      = 0x01,
    STR_CORRECTED_POSE_DATA                 = 0x10,
    STR_CORRECTED_POSE_DATA_LIGHT           = 0x11,
    STR_UNCORRECTED_POSE_DATA               = 0x12,
    STR_DIAGNOSTICS                         = 0x13,
    INT_DRIFT_CORRECTION_DONE               = 0x14,
    STR_QUALITY_ESTIMATE                    = 0x15,
    STR_LINE_FOLLOWER                       = 0x16,
    STR_SIGNATURE_MARKER                    = 0x17,
    INT_ARUCO_MARKER                        = 0x18,
    ACK_LEARNING_MODE                       = 0x20,
    ACK_QR_DETECTION_MODE                   = 0x21,
    ACK_DRIFT_CORRECTION_MODE               = 0x22,
    ACK_RECORDING_MODE                      = 0x23,
    ACK_IDLE_MODE                           = 0x24,
    ACK_REBOOT_MODE                         = 0x25,
    ACK_CALIBRATION_MODE                    = 0x27,
    ACK_COMPLETE_QR_LIBRARY_REMOVED         = 0x28,
    ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED    = 0x29,
    ACK_RECOVERY_MODE                       = 0x2A,
    ACK_ARUCO_MARKER_DETECTION_MODE         = 0x2B,
    ACK_ACCERION_ONLY_2                     = 0x2C,
    ACK_LINE_FOLLOWER_MODE                  = 0x2D,
    ACK_SIGNATURE_MARKER_MAP_START_STOP     = 0x2E,
    ACK_REPLACE_CLUSTER_G2O                 = 0x2F,
    ACK_QR_ADDED_TO_LIBRARY                 = 0x30,
    ACK_QR_REMOVED_FROM_LIBRARY             = 0x31,
    ACK_SAMPLE_RATE                         = 0x32,
    ACK_DRIFT_CORRECTION_MISSED             = 0x33,
    ACK_CLUSTER_REMOVED                     = 0x34,
    ACK_CLUSTER_MAP                         = 0x35,
    ACK_LOGS                                = 0x36,
    ACK_UPDATE                              = 0x37,
    ACK_PLACE_MAP                           = 0x38,
    ACK_EXPERTMODE                          = 0x39,
    ACK_SOFTWAREHASH                        = 0x3A,
    ACK_MAP_LOADED                          = 0x3B,
    ACK_SET_BUFFER_LENGTH                   = 0x3C,
    ACK_BUFFER_PROGRESS_INDICATOR           = 0x3D,
    ACK_RECORD_RECOVERY_BUFFER              = 0x3E,
    ACK_RECORDINGS                          = 0x3F,
    ACK_IP_ADDRESS                          = 0x40,
    ACK_TCPIP_INFO                          = 0x41,
    ACK_NEW_POSITION_IS_SET                 = 0x42,
    ACK_CALIBRATION_INFO                    = 0x43,
    ACK_SERIAL_NUMBER                       = 0x44,
    ACK_CONSOLE_OUTPUT_INFO                 = 0x45,
    ACK_JUPITER_STARTED                     = 0x46,
    ACK_JUPITER_STOPPED                     = 0x47,
    ACK_TIME_AND_DATE                       = 0x48,
    ACK_SENSOR_MOUNT_POSE                   = 0x49,
    ACK_SECOND_LINE                         = 0x4B,
    ACK_FRAME                               = 0x4C,
    ACK_SOFTWARE_VERSION                    = 0x4D,
    ACK_UDP_INFO                            = 0x4E,
    ACK_CLUSTER_G2O                         = 0x4F,
    CMD_SET_LEARNING_MODE                   = 0x50,
    CMD_SET_QR_DETECTION                    = 0x51,
    CMD_SET_DRIFT_CORRECTION_MODE           = 0x52,
    CMD_SET_RECORDING_MODE                  = 0x53,
    CMD_SET_IDLE_MODE                       = 0x54,
    CMD_SET_REBOOT_MODE                     = 0x55,
    CMD_SET_CALIBRATION_MODE                = 0x56,
    CMD_ACCERION_ONLY_1                     = 0x57,
    CMD_ACCERION_ONLY_2                     = 0x58,
    CMD_ACCERION_ONLY_3                     = 0x59,
    CMD_GET_SIGNATURE_MARKER_MAP            = 0x5A,
    CMD_SET_ARUCO_MARKER_MODE               = 0x5B,
    CMD_SET_RECORD_RECOVERY_BUFFER          = 0x5C,
    CMD_RECORDINGS                          = 0x5D,
    CMD_GET_IP_ADDRESS                      = 0x60,
    CMD_GET_ALL_IP_ADDRESSES                = 0x61,
    CMD_GET_SAMPLE_RATE                     = 0x62,
    CMD_REMOVE_COMPLETE_QR_LIBRARY          = 0x63,
    CMD_DOWNLOAD_QR_LIBRARY                 = 0x64,
    CMD_GET_ALL_SERIAL_NUMBERS              = 0x65,
    CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY     = 0x66,
    CMD_GET_ALL_ACKNOWLEDGEMENTS            = 0x67,
    CMD_GET_SOFTWARE_VERSION                = 0x68,
    CMD_GET_TCPIP_RECEIVER                  = 0x69,
    CMD_GET_SENSOR_MOUNT_POSE               = 0x6A,
    CMD_SET_EXPERTMODE                      = 0x6B,
    CMD_GET_BUFFER_LENGTH                   = 0x6C,
    CMD_STOP_PROCESSING_BUFFER              = 0x6D,
    CMD_SET_SAMPLE_RATE                     = 0x70,
    CMD_REMOVE_QR_FROM_LIBRARY              = 0x71,
    CMD_SET_RECOVERY_MODE                   = 0x72,
    CMD_ACCERION_ONLY_4                     = 0x73,
    CMD_DELETE_CLUSTER                      = 0x74,
    CMD_SECOND_LINE                         = 0x75,
    CMD_CAPTURE_FRAME                       = 0x76,
    CMD_SET_IP_ADDRESS                      = 0x80,
    CMD_SET_NEW_POSE                        = 0x81,
    CMD_ADD_QR_TO_LIBRARY                   = 0x82,
    CMD_SET_TIME_DATE                       = 0x83,
    CMD_SET_SENSOR_MOUNT_POSE               = 0x84,
    CMD_SET_POSE_AND_COVARIANCE             = 0x85,
    CMD_START_MARKERLESS_LEARNING           = 0x86,
    CMD_SET_TCPIP_RECEIVER                  = 0x87,
    CMD_START_LINE_FOLLOWER                 = 0x88,
    CMD_SET_UDP_SETTINGS                    = 0x89,
    CMD_SET_BUFFER_LENGTH                   = 0x8A,
    CMD_START_PROCESSING_BUFFER             = 0x8B,
    CMD_GET_CLUSTER_G2O                     = 0x90,
    CMD_REPLACE_CLUSTER_G2O                 = 0x91,
    CMD_GET_MAP                             = 0x92,
    CMD_GET_LOGS                            = 0x93,
    CMD_UPDATE                              = 0x94,
    CMD_GET_BACKUP_LOGS                     = 0x95,
    CMD_PLACE_MAP                           = 0x96,
    CMD_GET_SOFTWAREHASH                    = 0x97
};

/// Map containing the possible options for log retrieval to get the int value by the string
const std::map<std::string, int> logOptionsStrInt {
    {"Logs Only" ,0},
    {"Logs and Dev Logs" ,1},
    {"Logs and Stats" ,2},
    {"Complete Bundle" ,3},
    {"Dev Logs" ,4},
    {"Dev Logs and Stats" ,5},
    {"Stats" ,6}
};

/// Map containing the possible options for log retrieval to get the string value by the int
const std::map<int, std::string> logOptionsIntStr {
    {0, "Logs Only"},
    {1, "Logs and Dev Logs"},
    {2, "Logs and Stats"},
    {3, "Complete Bundle"},
    {4, "Dev Logs"},
    {5, "Dev Logs and Stats"},
    {6, "Stats"}
};

/// Map containing all the warnings that the diagnostics might output
const std::map<int, std::string> warningMap{
    {0, "Too Fast For Mapping"},
    {1, "Too Fast For Matching"},
    {2, "Low Surface Quality"},
    {3, "Low Throughput Read"},
    {4, "Low Throughput Track"},
    {5, "Low Throughput Main"},
    {6, "Low Memory"},
    {7, "Low Brightness Cam 1"}, // 40
    {8, "Low Brightness Cam 2"}, // 40
    {9, "High Brightness Cam 1"}, // 170
    {10, "High Brightness Cam 2"} // 170
};

/// Map containing all the errors that the diagnostics might output
const std::map<int, std::string> errorMap{
    {0, "Error in Tracking"},
    {1, "Lost Position"},
    {2, "Connection Error"},
    {3, "License Error"},
    {4, "Security Error"},
    {5, "Connection Init Error"},
    {6, "Camera Timeout"},
    {7, "Camera Regrab"},
    {8, "Camera Consec"},
    {9, "Camera Other"},
    {10, "Camera Init"},
    {11, "Camera Count"},
    {12, "Camera Err Other"},
    {13, "Skipped Frame"},
    {14, "Map Error 1"},
    {15, "Map Error 2"},
    {16, "Map Error 3"},
    {17, "Map Error 4"},
    {18, "Map Error 5"},
    {19, "Map Error 6"},
};

/// Map containing all the status that the diagnostics might output
const std::map<int, std::string> statusMap{
    {0, "Above velocity threshold; ignoring commands"},
    {1, "Test Status 2"},
};

/// Enum containing the various stages and feedback of file transmissions
enum FileSenderStatus
{
    PACKING_MAP                 = 0,
    PACKING_LOGS                = 1,
    SENDING_MAP                 = 2,
    SENDING_UPDATE              = 3,
    UNPACKING_AND_REPLACING_MAP = 4,
    UNPACKING_AND_UPDATING      = 5,
    FAILED_TO_STOP              = 6,
    FAILED_TO_START             = 7,
    FAILED_TO_BACKUP            = 8,
    FAILED_TO_UNZIP_UPDATE      = 9,
    FAILED_TO_RESTORE_BACKUP    = 10,
    ERROR_CODE_18               = 11,
    ERROR_CODE_19               = 12,
    ERROR_CODE_20               = 13,
    RETRIEVING_LOGS             = 14,
    RETRIEVING_MAP              = 15,
    CONNECTION_FAILED           = 16,
    FAILED_TO_REMOVE_EXISTING   = 17,
    FAILED_TO_OPEN_FILE         = 18,
    ALREADY_IN_PROGRESS         = 19,
    PACKING_RECORDINGS          = 20,
    RETRIEVING_RECORDINGS       = 21
};

/// Enum containing the various types of message types, each command belongs to a certain type and might have an influence on the way they are sent/received
enum class MessageTypes : uint8_t
{
    PERIODIC        = 0X01,
    STREAMING       = 0x02,
    INTERMITTENT    = 0x03,
    ACKNOWLEDGEMENT = 0x04,
    COMMAND         = 0x05
};

/// Enum containing the various types of TCP message type settings
enum class TCPMessageTypeSettings
{
    INACTIVE        = 0x01,
    STREAMING       = 0x02,
    INTERMITTENT    = 0x03,
    BOTH            = 0x04
};

/// Enum containing socket errors
enum SocketErrors
{
    BROKEN_PIPE = 32
};

/// Map containing all the in and outgoing messages with their length and messagetype
const std::map<uint8_t, std::tuple<std::string, unsigned int, MessageTypes>> commandValues
{
    {PRD_HEARTBEAT_INFO                     , std::tuple<std::string, unsigned int, MessageTypes> {"PRD_HEARTBEAT_INFO"                     , 14,   MessageTypes::PERIODIC      }},
    {STR_CORRECTED_POSE_DATA                , std::tuple<std::string, unsigned int, MessageTypes> {"STR_CORRECTED_POSE_DATA"                , 81,   MessageTypes::STREAMING     }},
    {STR_CORRECTED_POSE_DATA_LIGHT          , std::tuple<std::string, unsigned int, MessageTypes> {"STR_CORRECTED_POSE_DATA_LIGHT"          , 61,   MessageTypes::STREAMING     }},
    {STR_UNCORRECTED_POSE_DATA              , std::tuple<std::string, unsigned int, MessageTypes> {"STR_UNCORRECTED_POSE_DATA"              , 49,   MessageTypes::STREAMING     }},
    {STR_DIAGNOSTICS                        , std::tuple<std::string, unsigned int, MessageTypes> {"STR_DIAGNOSTICS"                        , 23,   MessageTypes::STREAMING     }},
    {INT_DRIFT_CORRECTION_DONE              , std::tuple<std::string, unsigned int, MessageTypes> {"INT_DRIFT_CORRECTION_DONE"              , 54,   MessageTypes::INTERMITTENT  }},
    {STR_QUALITY_ESTIMATE                   , std::tuple<std::string, unsigned int, MessageTypes> {"STR_QUALITY_ESTIMATE"                   , 16,   MessageTypes::STREAMING     }},
    {STR_LINE_FOLLOWER                      , std::tuple<std::string, unsigned int, MessageTypes> {"STR_LINE_FOLLOWER"                      , 40,   MessageTypes::STREAMING     }},
    {STR_SIGNATURE_MARKER                   , std::tuple<std::string, unsigned int, MessageTypes> {"STR_SIGNATURE_MARKER"                   , 29,   MessageTypes::STREAMING     }},
    {INT_ARUCO_MARKER                       , std::tuple<std::string, unsigned int, MessageTypes> {"INT_ARUCO_MARKER"                       , 28,   MessageTypes::INTERMITTENT  }},
    {ACK_LEARNING_MODE                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_LEARNING_MODE"                      , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_QR_DETECTION_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_DETECTION_MODE"                  , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_DRIFT_CORRECTION_MODE              , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_DRIFT_CORRECTION_MODE"              , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_RECORDING_MODE                     , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_RECORDING_MODE"                     , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_IDLE_MODE                          , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_IDLE_MODE"                          , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_REBOOT_MODE                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_REBOOT_MODE"                        , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_CALIBRATION_MODE                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CALIBRATION_MODE"                   , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_COMPLETE_QR_LIBRARY_REMOVED        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_COMPLETE_QR_LIBRARY_REMOVED"        , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED"   , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_RECOVERY_MODE                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_RECOVERY_MODE"                      , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_ARUCO_MARKER_DETECTION_MODE        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_ARUCO_MARKER_DETECTION_MODE"        , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_ACCERION_ONLY_2                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_ACCERION_ONLY_2"                    , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_LINE_FOLLOWER_MODE                 , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_LINE_FOLLOWER_MODE"                 , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SIGNATURE_MARKER_MAP_START_STOP    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SIGNATURE_MARKER_MAP_START_STOP"    , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_QR_ADDED_TO_LIBRARY                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_ADDED_TO_LIBRARY"                , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_QR_REMOVED_FROM_LIBRARY            , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_QR_REMOVED_FROM_LIBRARY"            , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SAMPLE_RATE                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SAMPLE_RATE"                        , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_DRIFT_CORRECTION_MISSED            , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_DRIFT_CORRECTION_MISSED"            , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_CLUSTER_REMOVED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CLUSTER_REMOVED"                    , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_CLUSTER_MAP                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CLUSTER_MAP"                        , 0,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_LOGS                               , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_LOGS"                               , 0,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_MAP_LOADED                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_MAP_LOADED"                         , 8,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_IP_ADDRESS                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_IP_ADDRESS"                         , 26,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_TCPIP_INFO                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_TCPIP_INFO"                         , 15,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_NEW_POSITION_IS_SET                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_NEW_POSITION_IS_SET"                , 18,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_CALIBRATION_INFO                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CALIBRATION_INFO"                   , 0,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SERIAL_NUMBER                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SERIAL_NUMBER"                      , 10,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_CONSOLE_OUTPUT_INFO                , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_CONSOLE_OUTPUT_INFO"                , 0,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_JUPITER_STARTED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_JUPITER_STARTED"                    , 6,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_JUPITER_STOPPED                    , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_JUPITER_STOPPED"                    , 6,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_TIME_AND_DATE                      , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_TIME_AND_DATE"                      , 13,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SENSOR_MOUNT_POSE                  , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SENSOR_MOUNT_POSE"                  , 18,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SECOND_LINE                        , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SECOND_LINE"                        , 40,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_FRAME                              , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_FRAME"                              , 0,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SOFTWARE_VERSION                   , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SOFTWARE_VERSION"                   , 9,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_UDP_INFO                           , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_UDP_MULTICAST"                      , 12,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_UPDATE                             , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_UPDATE"                             , 11,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_PLACE_MAP                          , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_PLACE_MAP"                          , 11,   MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_EXPERTMODE                         , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_EXPERTMODE"                         , 7,    MessageTypes::ACKNOWLEDGEMENT}},
    {ACK_SOFTWAREHASH                       , std::tuple<std::string, unsigned int, MessageTypes> {"ACK_SOFTWAREHASH"                       , 57,   MessageTypes::ACKNOWLEDGEMENT}},
    {CMD_SET_LEARNING_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_LEARNING_MODE"                  , 7,    MessageTypes::COMMAND}},
    {CMD_SET_QR_DETECTION                   , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_QR_DETECTION"                   , 7,    MessageTypes::COMMAND}},
    {CMD_SET_DRIFT_CORRECTION_MODE          , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_DRIFT_CORRECTION_MODE"          , 7,    MessageTypes::COMMAND}},
    {CMD_SET_RECORDING_MODE                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_RECORDING_MODE"                 , 7,    MessageTypes::COMMAND}},
    {CMD_SET_IDLE_MODE                      , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_IDLE_MODE"                      , 7,    MessageTypes::COMMAND}},
    {CMD_SET_REBOOT_MODE                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_REBOOT_MODE"                    , 7,    MessageTypes::COMMAND}},
    {CMD_SET_CALIBRATION_MODE               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_CALIBRATION_MODE"               , 7,    MessageTypes::COMMAND}},
    {CMD_ACCERION_ONLY_1                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ACCERION_ONLY_1"                    , 7,    MessageTypes::COMMAND}},
    {CMD_ACCERION_ONLY_2                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ACCERION_ONLY_2"                    , 7,    MessageTypes::COMMAND}},
    {CMD_ACCERION_ONLY_3                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ACCERION_ONLY_3"                    , 7,    MessageTypes::COMMAND}},
    {CMD_GET_SIGNATURE_MARKER_MAP           , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SIGNATURE_MARKER_MAP"           , 6,    MessageTypes::COMMAND}},
    {CMD_SET_ARUCO_MARKER_MODE              , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_ARUCO_MARKER_MODE"              , 7,    MessageTypes::COMMAND}},
    {CMD_GET_IP_ADDRESS                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_IP_ADDRESS"                     , 6,    MessageTypes::COMMAND}},
    {CMD_GET_ALL_IP_ADDRESSES               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_IP_ADDRESSES"               , 6,    MessageTypes::COMMAND}},
    {CMD_GET_SAMPLE_RATE                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SAMPLE_RATE"                    , 6,    MessageTypes::COMMAND}},
    {CMD_REMOVE_COMPLETE_QR_LIBRARY         , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_COMPLETE_QR_LIBRARY"         , 6,    MessageTypes::COMMAND}},
    {CMD_DOWNLOAD_QR_LIBRARY                , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_DOWNLOAD_QR_LIBRARY"                , 6,    MessageTypes::COMMAND}},
    {CMD_GET_ALL_SERIAL_NUMBERS             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_SERIAL_NUMBERS"             , 6,    MessageTypes::COMMAND}},
    {CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY"    , 6,    MessageTypes::COMMAND}},
    {CMD_GET_ALL_ACKNOWLEDGEMENTS           , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_ALL_ACKNOWLEDGEMENTS"           , 6,    MessageTypes::COMMAND}},
    {CMD_GET_SOFTWARE_VERSION               , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SOFTWARE_VERSION"               , 6,    MessageTypes::COMMAND}},
    {CMD_GET_TCPIP_RECEIVER                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_TCPIP_RECEIVER"                 , 6,    MessageTypes::COMMAND}},
    {CMD_SET_EXPERTMODE                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_EXPERTMODE"                     , 7,    MessageTypes::COMMAND}},
    {CMD_GET_BUFFER_LENGTH                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_BUFFER_LENGTH"                  , 6,    MessageTypes::COMMAND}},
    {CMD_STOP_PROCESSING_BUFFER             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_STOP_PROCESSING_BUFFER"             , 6,    MessageTypes::COMMAND}},
    {CMD_SET_SAMPLE_RATE                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_SAMPLE_RATE"                    , 8,    MessageTypes::COMMAND}},
    {CMD_REMOVE_QR_FROM_LIBRARY             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_REMOVE_QR_FROM_LIBRARY"             , 8,    MessageTypes::COMMAND}},
    {CMD_SET_RECOVERY_MODE                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_RECOVERY_MODE"                  , 8,    MessageTypes::COMMAND}},
    {CMD_ACCERION_ONLY_4                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ACCERION_ONLY_4"                    , 8,    MessageTypes::COMMAND}},
    {CMD_DELETE_CLUSTER                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_DELETE_CLUSTER"                     , 8,    MessageTypes::COMMAND}},
    {CMD_SECOND_LINE                        , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SECOND_LINE"                        , 8,    MessageTypes::COMMAND}},
    {CMD_CAPTURE_FRAME                      , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_CAPTURE_FRAME"                      , 23,   MessageTypes::COMMAND}},
    {CMD_SET_IP_ADDRESS                     , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_IP_ADDRESS"                     , 18,   MessageTypes::COMMAND}},
    {CMD_SET_NEW_POSE                       , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_NEW_POSE"                       , 18,   MessageTypes::COMMAND}},
    {CMD_ADD_QR_TO_LIBRARY                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_ADD_QR_TO_LIBRARY"                  , 20,   MessageTypes::COMMAND}},
    {CMD_SET_TIME_DATE                      , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_TIME_DATE"                      , 13,   MessageTypes::COMMAND}},
    {CMD_SET_SENSOR_MOUNT_POSE              , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_SENSOR_MOUNT_POSE"              , 18,   MessageTypes::COMMAND}},
    {CMD_SET_POSE_AND_COVARIANCE            , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_POSE_AND_COVARIANCE"            , 38,   MessageTypes::COMMAND}},
    {CMD_START_MARKERLESS_LEARNING          , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_START_MARKERLESS_LEARNING"          , 9,    MessageTypes::COMMAND}},
    {CMD_SET_TCPIP_RECEIVER                 , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_TCPIP_RECEIVER"                 , 11,   MessageTypes::COMMAND}},
    {CMD_START_LINE_FOLLOWER                , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_START_LINE_FOLLOWER"                , 9,    MessageTypes::COMMAND}},
    {CMD_SET_UDP_SETTINGS                   , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_UDP_SETTINGS"                   , 12,   MessageTypes::COMMAND}},
    {CMD_SET_BUFFER_LENGTH                  , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_SET_BUFFER_LENGTH"                  , 10,   MessageTypes::COMMAND}},
    {CMD_START_PROCESSING_BUFFER            , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_START_PROCESSING_BUFFER"            , 15,   MessageTypes::COMMAND}},
    {CMD_GET_MAP                            , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_MAP"                            , 6,    MessageTypes::COMMAND}},
    {CMD_GET_LOGS                           , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_LOGS"                           , 10,   MessageTypes::COMMAND}},
    {CMD_UPDATE                             , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_UPDATE"                             , 0,    MessageTypes::COMMAND}},
    {CMD_GET_BACKUP_LOGS                    , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_BACKUP_LOGS"                    , 10,   MessageTypes::COMMAND}},
    {CMD_PLACE_MAP                          , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_PLACE_MAP"                          , 0,    MessageTypes::COMMAND}},
    {CMD_GET_SOFTWAREHASH                   , std::tuple<std::string, unsigned int, MessageTypes> {"CMD_GET_SOFTWAREHASH"                   , 6,    MessageTypes::COMMAND}}
};

/** 
 * Default Command structure
 * This is the base command which all other commands inherit. It basically consists of an ID and the data.
 */
struct Command
{
    Command(uint8_t commandID, std::vector<uint8_t> commandData);
    uint8_t commandID_;
    std::vector<uint8_t> command_;
};

/** 
 * Empty Command
 * This is a command that is used when it has no fields
 */
struct EmptyCommand : Command
{
    EmptyCommand(uint8_t commandID);
    ~EmptyCommand()= default;;
    std::vector<uint8_t> serialize();
};

/** 
 * Boolean Command
 * This is a command that is used when a boolean has to be sent
 */
struct BooleanCommand : Command
{
    BooleanCommand(uint8_t commandID, bool boolValue);
    ~BooleanCommand() = default;;
    std::vector<uint8_t> serialize();
    uint8_t ackValue_;
};

/** 
 * UINT16 Command
 * This is a command that is used when a single 16 bit value has to be sent
 */
struct UINT16Command : Command 
{
    UINT16Command(uint8_t commandID, uint16_t value);
    ~UINT16Command() = default;;
    std::vector<uint8_t> serialize();
    uint16_t value_;
};

/** 
 * UINT32 Command
 * This is a command that is used when a single 32 bit value has to be sent
 */
struct UINT32Command : Command 
{
    UINT32Command(uint8_t commandID, uint32_t value);
    ~UINT32Command() = default;;
    std::vector<uint8_t> serialize();
    uint32_t value_;
};

/** 
 * AddQR Command
 * This is a command that is used when a QR has to be added
 * Additionally to the parent struct input it takes a qrID, x position, y position and theta.
 */
struct AddQRCommand : Command
{
    AddQRCommand(uint8_t commandID, uint16_t qrID, int32_t xPos, int32_t yPos, int32_t theta);
    std::vector<uint8_t> serialize();
    uint16_t qrID_;
    int32_t xPos_, yPos_, theta_;
};

/** 
 * Recovery Command
 * This is a command that is used when Recovery Mode has to be toggled
 * Additionally to the parent struct input it takes a bool and a radius
 */
struct RecoveryCommand : Command
{
    RecoveryCommand(uint8_t commandID, bool onOff, uint8_t radius);
    ~RecoveryCommand()= default;;
    std::vector<uint8_t> serialize();
    uint8_t  onOff_;
    uint8_t radius_;
};

/** 
 * Pose Command
 * This is a command that is used when a pose has to be sent
 * Additionally to the parent struct input it takes a x position, y position and theta
 */
struct PoseCommand : Command
{
    PoseCommand(uint8_t commandID, double xPos, double yPos, double theta);
    std::vector<uint8_t> serialize();
    int32_t xPos_, yPos_, theta_;
};

/** 
 * Set IP Command
 * This is a command that is used when the IP has to be set
 * Additionally to the parent struct input it takes an IP address, netmask and gateway.
 */
struct SetIPCommand : Command
{
    SetIPCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t netmaskFirst, uint8_t netmaskSecond, uint8_t netmaskThird, uint8_t netmaskFourth, uint8_t gatewayFirst, uint8_t gatewaySecond, uint8_t gatewayThird, uint8_t gatewayFourth);
    std::vector<uint8_t> serialize();
    uint8_t ipAddrFirst_;
    uint8_t ipAddrSecond_;
    uint8_t ipAddrThird_;
    uint8_t ipAddrFourth_;
    uint8_t netmaskFirst_;
    uint8_t netmaskSecond_;
    uint8_t netmaskThird_;
    uint8_t netmaskFourth_;
    uint8_t gatewayFirst_;
    uint8_t gatewaySecond_;
    uint8_t gatewayThird_;
    uint8_t gatewayFourth_;
};

/** 
 * DateTime Command
 * This is a command that is used when the Date/Time has to be set
 * Additionally to the parent struct input it takes a date and time
 */
struct DateTimeCommand : Command
{
    DateTimeCommand(uint8_t commandID, uint8_t day, uint8_t month, uint16_t year, uint8_t hours, uint8_t minutes, uint8_t seconds);
    std::vector<uint8_t> serialize();
    uint8_t     day_;
    uint8_t     month_;
    uint16_t    year_;
    uint8_t     hours_;
    uint8_t     minutes_;
    uint8_t     seconds_;
};

/** 
 * ToggleMapping Command
 * This is a command that is used when Mapping has to be toggled
 * Additionally to the parent struct input it takes a bool and a clusterID
 */
struct ToggleMappingCommand : Command
{
    ToggleMappingCommand(uint8_t commandID, bool value, uint16_t clusterID);
    std::vector<uint8_t> serialize();
    uint16_t clusterID_;
    bool value_;
};

/** 
 * PoseAndCovariance Command
 * This is a command that is used to sent external reference input to the sensor
 * Additionally to the parent struct input it takes a timestamp, x position, y position, heading and standard deviation in x y and theta
 */
struct PoseAndCovarianceCommand : Command
{
    PoseAndCovarianceCommand(uint8_t commandID, uint64_t timeStamp, double xPos, double yPos, double heading, double stdDevX, double stdDevY, double stdDevTheta);
    std::vector<uint8_t> serialize();
    uint64_t    timeStamp_;
    int32_t     xPos_;
    int32_t     yPos_;
    int32_t     heading_;
    uint32_t    stdDevX_;
    uint32_t    stdDevY_;
    uint32_t    stdDevTheta_;
};

/** 
 * TCPIPReceiver Command
 * This is a command that is used when the TCP/IP Receiver has to be set
 * Additionally to the parent struct input it takes an IP address and the messageType
 */
struct TCPIPReceiverCommand : Command
{
    TCPIPReceiverCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t messageType);
    std::vector<uint8_t> serialize();
    uint8_t ipAddrFirst_;
    uint8_t ipAddrSecond_;
    uint8_t ipAddrThird_;
    uint8_t ipAddrFourth_;
    uint8_t messageType_;
};

/** 
 * UDPSettings Command
 * This is a command that is used when  the UDP Settings have to be set
 * Additionally to the parent struct input it takes an ip address, the messageType and a value indicating broad or unicasting
 */
struct UDPSettingsCommand : Command
{
    UDPSettingsCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t messageType, uint8_t broadOrUniCast);
    std::vector<uint8_t> serialize();
    uint8_t ipAddrFirst_;
    uint8_t ipAddrSecond_;
    uint8_t ipAddrThird_;
    uint8_t ipAddrFourth_;
    uint8_t messageType_;
    uint8_t broadOrUniCast_;
};

/** 
 * G2O Command
 * This is a command that is used when a G2O file has to be sent to the sensor
 * Additionally to the parent struct input it takes a messageLength, clusterID and a vector of data containing the g2o file contents.
 */
struct G2OCommand : Command
{
    G2OCommand(uint8_t commandID, uint32_t messageLength, uint16_t clusterID, std::vector<uint8_t> data);
    std::vector<uint8_t> serialize();
    uint32_t messageLength_;
    uint16_t clusterID_;
    std::vector<uint8_t> data_;
};

/** 
 * PlaceMap Command
 * This is a command that is used when a map has to be sent to the sensor
 * Additionally to the parent struct input it takes the length of the message, the type of this message, the packetNumber and a vector of data
 */
struct PlaceMapCommand : Command
{
    PlaceMapCommand(uint8_t commandID, uint32_t messageLength, uint8_t packetType, uint32_t packetNumber, std::vector<uint8_t> data);
    std::vector<uint8_t> serialize();
    uint32_t messageLength_;
    uint8_t packetType_;
    uint32_t packetNumber_;
    std::vector<uint8_t> data_;
};

/** 
 * BufferedRecovery Command
 * This is a command that is used when Buffered Recovery Mode has to be toggled
 * Additionally to the parent struct input it takes the x position, y position and radius in which it should search
 */
struct BufferedRecoveryCommand : Command
{
    BufferedRecoveryCommand(uint8_t commandID, uint32_t xPos, uint32_t yPos, uint8_t radius);
    std::vector<uint8_t> serialize();
    uint32_t xPos_;
    uint32_t yPos_;
    uint8_t radius_;
};

/** 
 * Recordings Command
 * This is a command that is used for recordings management
 * Additionally to the parent struct input it takes the length of the message, the type of this message, and various other content
 */
struct RecordingsCommand : Command
{
    RecordingsCommand(uint8_t commandID, uint8_t packetType, std::vector<uint8_t> data);
    std::vector<uint8_t> serialize();
    uint32_t messageLength_;
    uint8_t packetType_;
    std::vector<uint8_t> data_;
};

/** 
 * CaptureFrame Command
 * This is a command that is used for testing purposes..
 */
struct CaptureFrameCommand: Command
{
    CaptureFrameCommand(uint8_t commandID, uint8_t camIdx, std::string key);
    std::vector<uint8_t> serialize();
    uint8_t camIdx_;
    std::string key_;
    std::vector<uint8_t> data_;
};

#endif // COMMANDS_H
