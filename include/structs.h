/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef STRUCTS_H
#define STRUCTS_H

#include <cstdint>
#include <iostream>
#include <vector>
#include "TypeDef.h"

/**
 * \brief ConnectionType enum indicating which way the connection to the sensor should be made
 *  One can either connect over TCP or UDP. For UDP the distinction is made between unicasting and broadcasting.
 *  Unicasting can reduce the network load. 
 */
enum class ConnectionType
{
  CONNECTION_TCP = 0,
  CONNECTION_UDP_BROADCAST = 1,
  CONNECTION_UDP_UNICAST = 2
};

struct FloorCheckerEntry
{
    double  velocity;
    bool    driftCorrectionHit; // true is hit, false is a missed one
};

struct LogRetrieval{
    std::string ipAddress;
    uint16_t    options;
};

struct FileEntry{
    uint32_t    serial;
    std::string ipAddress;
    std::string file;
};

/**
 * \brief HeartBeat struct as received through the HeartBeat message.
 *  The HeartBeat is sent by the sensor or updateservice to indicate it is live.
 *  It contains the IP and unicast address 
 */
struct HeartBeat
{
    uint8_t ipAddrFirst;
    uint8_t ipAddrSecond;
    uint8_t ipAddrThird;
    uint8_t ipAddrFourth;
    uint8_t uniFirst;
    uint8_t uniSecond;
    uint8_t uniThird;
    uint8_t uniFourth;
};

struct Pose
{
    double x; // < x position in meter */
    double y; // < y position in meter */
    double heading; // < heading in degree */
};

struct StandardDeviation
{
    double    x;// < in meter */
    double    y;// < in meter */
    double    theta;// < in degree */
};

struct CorrectedPose
{
    uint64_t            timeStamp;
    Pose                pose;
    double             xVel;// < in meter per second */
    double             yVel;// < in meter per second*/
    double             thVel;// < in degree per second */
    StandardDeviation   standardDeviation;
    StandardDeviation   standardDeviationVelocity;
    uint8_t             qualityEstimate;
};

struct UncorrectedPose
{
    uint64_t            timeStamp;
    Pose                pose;
    double             xVel;// < in meter per second */
    double             yVel;// < in meter per second */
    double             thVel;// < in degree per second */
    StandardDeviation   standardDeviationVelocity;
    uint8_t             qualityEstimate;
};

struct Diagnostics
{
    uint64_t    timeStamp;
    uint16_t    modes;
    uint16_t    warningCodes;
    uint32_t    errorCodes;
    uint8_t     statusCodes;
};

struct DriftCorrection
{
    uint64_t    timeStamp;
    Pose        pose;
    double     xDelta;// < in meter */
    double     yDelta;// < in meter */
    double     thDelta;// < in degree */
    double    cumulativeTravelledDistance;// < in meter */
    double    cumulativeTravelledHeading;// < in degree */
    uint32_t    errorPercentage;
    uint16_t    QRID;
    uint8_t     typeOfCorrection;//1 = infraless, 2 = qr, 3 = manual
    uint8_t     qualityEstimate;
};

struct QualityEstimate
{
    uint8_t     qualityEstimator1;
    uint8_t     qualityEstimator2;
    uint8_t     qualityEstimator3;
    uint8_t     qualityEstimator4;
    uint8_t     qualityEstimator5;
    uint8_t     qualityEstimator6;
    uint8_t     qualityEstimator7;
    uint8_t     qualityEstimator8;
    uint8_t     qualityEstimator9;
    uint8_t     qualityEstimator10;
};

struct LineFollowerData
{
    uint64_t    timeStamp;
    Pose        pose;
    double     closestPointX;// < in meter */
    double     closestPointY;// < in meter */
    int32_t     reserved;
    uint16_t    clusterID;
};

struct Marker
{
    double     xPos;// < in meter */
    double     yPos;// < in meter */
    uint16_t    clusterID;
    uint32_t    signatureID;
    uint32_t    sigStatus;//0x01 Matched, 0x02 Loaded, 0x03 Not loaded
    double    stdDevX;// < in meter */
    double    stdDevY;// < in meter */
};

struct MarkerPosPacket
{
    uint16_t                numberOfMarkersInMessage;
    std::vector<Marker>     markers;
};

struct Address
{
    uint8_t first;
    uint8_t second;
    uint8_t third;
    uint8_t fourth;
};

struct IPAddress
{
    //ip address
    Address ipAddress;
    //netmask addy
    Address netmask;
    //gateway
    Address gateway;
};

struct IPAddressExtended
{
    //static ip addy
    Address staticIPAddress;
    //static netmask addy
    Address staticNetmask;
    //dynamic ip addy
    Address dynamicIPAddress;
    //dynamic netmask addy
    Address dynamicNetmask;
    //default gateway
    Address defaultGateway;
};

struct UDPInfo
{
    Address ipAddress;
    uint8_t messageType; // 0x01 = inactive, 0x02 = streaming, 0x03 = intermittent, 0x04 = streaming & intermittent
    uint8_t broadOrUniCast; // 0x01 = broadcast, 0x02 = unicast
};

struct SampleRate
{
    uint16_t sampleRateFrequency;
};

struct SerialNumber
{
    uint32_t serialNumber;
};

struct SoftwareVersion
{
    uint8_t major;
    uint8_t minor;
    uint8_t patch;
};

struct TCPIPInformation
{
    Address ipAddress;
    Address hostIPAddress;
    uint8_t messageType; //0x01 inactive, 0x02 streaming, 0x03 intermittent, 0x04 streaming & intermittent
};

struct AddQRResult
{
    uint16_t    qrID;
    bool        result;
};

struct DateTime
{
    uint8_t     day;
    uint8_t     month;
    uint16_t    year;
    uint8_t     hours;
    uint8_t     minutes;
    uint8_t     seconds;
};

struct Acknowledgement //generic acknowledgement struct.
{
    bool value; // can be used for IsOn, Start/Stop, IsSucces etc.
};

struct FilePacket
{
    uint32_t    packetSize;
    uint8_t     type;//0 = start, 1 = packet, 2 = stop
    uint8_t     data[];
};

struct MapImportResult
{
    uint8_t     resultCode;
    uint32_t    packageNumber;
};

struct SoftwareDetails
{
    std::string softwareHash;
    std::string date;
};

struct InputPose
{
    uint64_t timeStamp;
    Pose pose;
    StandardDeviation standardDeviation;
};

struct ArucoMarker
{
    uint64_t timeStamp;
    Pose pose;
    uint16_t markerID;
};

struct MapLoadingInfo
{
    bool success;
    uint8_t percentage;
    std::string message;
};

struct BufferProgress
{
    uint8_t messageType; // 0x01 = progress, 0x02 = result, 0x03 = cancelled
    uint8_t result; // 0x01 = sucess, 0x02 = failure, valid if messagetype = 0x02
    uint8_t progress; // valid if messagetype = 0x01
};

struct DeleteRecordingsResult
{
    bool success;
    std::vector<int> failedIndexes;
};
#endif