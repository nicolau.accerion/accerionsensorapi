/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensor.h"

AccerionSensor::AccerionSensor(Address ip, const std::string& serial, Address localIP, ConnectionType conType)
{
#ifdef DEBUG
    debugMode_ = true;
#endif
    /*Initialize CRC Checking*/
    crc8_.crcInit();
    char buf[3 * 4 + 3 * 1 + 1];
    snprintf(buf, sizeof(buf), "%d.%d.%d.%d", ip.first, ip.second, ip.third, ip.fourth);
    // store remote IP address in remote:
    struct sockaddr_in remote;

#ifdef __linux__ 
    inet_pton(AF_INET, buf, &(remote.sin_addr));
#elif _WIN32
    InetPton(AF_INET, buf, &(remote.sin_addr));
#endif

    // store local IP address in local:
    localIP_ = localIP;

    sensorSerialNumber_ = stoi(serial);
    connectionType = conType;

    udpReceiver = new UDPReceiver(API_RECEIVE_PORT_UDP);
    udpTransmitter = new UDPTransmitter(JUPITER_RECEIVE_PORT_UDP);
    udpTransmitter->setIPAddress(remote.sin_addr);
    udpTransmitter->sensorSerialNumber_ = stoi(serial);
    std::thread udpThread(&AccerionSensor::runUDPCommunication, this);
    udpThread.detach();

    tcpClient = new TCPClient(remote.sin_addr, TCP_PORT);
    tcpClient->sensorSerialNumber_ = stoi(serial);
    std::thread tcpThread(&AccerionSensor::runTCPCommunication, this);
    tcpThread.detach();
}

AccerionSensor::~AccerionSensor()
{
    delete udpTransmitter;
    delete udpReceiver;
    delete tcpClient;
}

void AccerionSensor::runUDPCommunication()
{
    ProfileTimer profileTimer("AccerionSensor UDP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;

    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    uint8_t udpMsgType = 0, udpStrat = 1;
    switch (connectionType)
    {
    case ConnectionType::CONNECTION_TCP:
        udpMsgType = 1;
        break;
    case ConnectionType::CONNECTION_UDP_BROADCAST:
        udpMsgType = 4;
        udpStrat = 1;
        break;
    case ConnectionType::CONNECTION_UDP_UNICAST:
        udpMsgType = 4;
        udpStrat = 2;
        break;
    }
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_UDP_SETTINGS, UDPSettingsCommand(CMD_SET_UDP_SETTINGS, localIP_.first, localIP_.second, localIP_.third, localIP_.fourth, udpMsgType, udpStrat).serialize());
    outgoingCommandsMutex.unlock();

    while (runUDP)
    {
        profileTimer.startLoopTime();
        //receive
        while (udpReceiver->ReceiveMessage())
        {
            receivedMSG_.clear();
            receivedMSG_.insert(receivedMSG_.end(), udpReceiver->getReceivedMessage(), udpReceiver->getReceivedMessage() + udpReceiver->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }

        readMessages(incomingCommandsTotal_, outgoingCommandsTotal_);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            udpTransmitter->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
        if (connectionType == ConnectionType::CONNECTION_TCP)
        {
            runUDP = false;
        }

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
    delete udpTransmitter;
    delete udpReceiver;
}

void AccerionSensor::runTCPCommunication()
{
    ProfileTimer profileTimer("AccerionSensor TCP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;

    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    uint8_t tcpMsgType = 0;
    switch (connectionType)
    {
    case ConnectionType::CONNECTION_TCP:
        tcpMsgType = 4;
        break;
    case ConnectionType::CONNECTION_UDP_BROADCAST:
        tcpMsgType = 1;
        break;
    case ConnectionType::CONNECTION_UDP_UNICAST:
        tcpMsgType = 1;
        break;
    }
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TCPIP_RECEIVER, TCPIPReceiverCommand(CMD_SET_TCPIP_RECEIVER, localIP_.first, localIP_.second, localIP_.third, localIP_.fourth, tcpMsgType).serialize());
    outgoingCommandsMutex.unlock();

    tcpClient->connectToServer();
    while (runTCP)
    {
        profileTimer.startLoopTime();
        //receive
        while (tcpClient->receiveMessage())
        {
            if (!lastMessageWasBroken_)
            {
                receivedMSG_.clear();
            }
            receivedMSG_.insert(receivedMSG_.end(), tcpClient->getReceivedMessage(), tcpClient->getReceivedMessage() + tcpClient->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }

        readMessages(incomingCommandsTotal_, outgoingCommandsTotal_);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            tcpClient->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
        if (connectionType != ConnectionType::CONNECTION_TCP) // TCP OR FORCE TCP
        {
            runTCP = false;
        }

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
    delete tcpClient;
}

void AccerionSensor::readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands)
{
    auto it = incomingCommands.begin();
    while (it != incomingCommands.end())
    {
        receivedCommandID_ = (*it).commandID_;
        receivedCommand_ = (*it).command_;

        // if (debugMode_)
        // {
        //     std::cout << "[TCPLogUpdateManager] readMessages, Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
        //     std::cout << "[TCPLogUpdateManager] readMessages, Received command data is := ";
        //     for (unsigned int i = 0; i < receivedCommand_.size(); ++i)
        //         std::cout << receivedCommand_[i];
        //     std::cout << std::endl;
        // }

        /*Check command, set modes according to command*/
        switch (receivedCommandID_)
        {
        case CommandIDs::PRD_HEARTBEAT_INFO:
            outputHeartBeat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SENSOR_MOUNT_POSE:
            acknowledgeMountPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_NEW_POSITION_IS_SET:
            acknowledgeSensorPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_CORRECTED_POSE_DATA_LIGHT:
            outputCorrectedPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_UNCORRECTED_POSE_DATA:
            outputUncorrectedPose(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_DIAGNOSTICS:
            outputDiagnostics(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::INT_DRIFT_CORRECTION_DONE:
            outputDriftCorrection(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_QUALITY_ESTIMATE:
            outputQualityEstimate(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_LINE_FOLLOWER:
            outputLineFollowerData(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_SIGNATURE_MARKER:
            outputMarkerPosPacket(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LEARNING_MODE:
            acknowledgeMappingToggle(receivedCommand_);
            acknowledgeAccQRMapping(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_DRIFT_CORRECTION_MODE:
            acknowledgeAbsoluteMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECORDING_MODE:
            acknowledgeRecordingMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_IDLE_MODE:
            acknowledgeIdleMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_REBOOT_MODE:
            acknowledgeRebootMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CALIBRATION_MODE:
            acknowledgeCalibrationMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECORDINGS:
            acknowledgeRecordingMsg(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_IP_ADDRESS:
            acknowledgeIPAddress(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SAMPLE_RATE:
            acknowledgeSampleRate(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_COMPLETE_QR_LIBRARY_REMOVED:
            acknowledgeClearQRLibrary(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SERIAL_NUMBER:
            acknowledgeSerialNumber(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED:
            acknowledgeClearClusterLibrary(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SOFTWARE_VERSION:
            acknowledgeSoftwareVersion(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_TCPIP_INFO:
            acknowledgeTCPIPInformation(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_EXPERTMODE:
            acknowledgeExpertMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_QR_REMOVED_FROM_LIBRARY:
            acknowledgeRemoveQR(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_RECOVERY_MODE:
            acknowledgeRecoveryMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CLUSTER_REMOVED:
            acknowledgeRemoveCluster(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SECOND_LINE:
            acknowledgeSecondaryLineFollowerOutput(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_QR_ADDED_TO_LIBRARY:
            acknowledgeAddQR(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_TIME_AND_DATE:
            acknowledgeDateTime(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LINE_FOLLOWER_MODE:
            acknowledgeLineFollowingToggle(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_UDP_INFO:
            acknowledgeUDPSettings(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CLUSTER_G2O:
            acknowledgeClusterInG2OFormat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_REPLACE_CLUSTER_G2O:
            acknowledgeReplaceClusterWithG2OFormat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CONSOLE_OUTPUT_INFO:
            outputConsoleOutputInfo(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_CLUSTER_MAP:
            retrievedMapPiece(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_PLACE_MAP:
            retrievedMapAck(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SOFTWAREHASH:
            acknowledgeSoftwareDetails(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_SIGNATURE_MARKER_MAP_START_STOP:
            acknowledgeMarkerPosPacketStartStop(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_DRIFT_CORRECTION_MISSED:
            outputDriftCorrectionsMissed(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_ARUCO_MARKER_DETECTION_MODE:
            acknowledgeToggleArucoMarkerMode(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::INT_ARUCO_MARKER:
            outputArucoMarker(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_MAP_LOADED:
            outputMapLoaded(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_FRAME:
                acknowledgeFrameCaptureMsg(receivedCommand_);
                it = incomingCommands.erase(it);
                break;
        default:
            it = incomingCommands.erase(it);
            break;
        }
    }
}

void AccerionSensor::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    if (debugMode_)
    {
        std::cout << "[AccerionSensor::parseMessages] numberOfBytesLeft is " << numberOfBytesLeft << std::endl;
        std::cout << "[AccerionSensor::parseMessages] receivedMessage_. Length is " << receivedMessage_.size() << std::endl;
    }

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();

        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if (debugMode_)
                    {
                        std::cout << "[AccerionSensor::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                    }
                }
            }
            // Extract number of bytes in message
            if (receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO && receivedCommandID_ != ACK_CLUSTER_MAP && receivedCommandID_ != ACK_RECORDINGS && receivedCommandID_ != ACK_FRAME)
            {
                if(receivedCommandID_ == STR_SIGNATURE_MARKER)
                {
                    uint8_t numberOfMarkers[2];
                    numberOfMarkers[0] = receivedMessage_[COMMAND_ID_BEGIN_BYTE + COMMAND_ID_LENGTH + numberOfReadBytes];
                    numberOfMarkers[1] = receivedMessage_[COMMAND_ID_BEGIN_BYTE + COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                    uint16_t markers = ntohl(*((uint16_t *)&numberOfMarkers));
                    receivedTotalBytesInMessage = (23 * markers) + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + 2;
                    if(receivedTotalBytesInMessage == 0)
                    {
                        return;
                    }

                }
                else
                {
                    auto iter = commandValues.find(receivedCommandID_);
                    if (iter != commandValues.end())
                    {
                        receivedTotalBytesInMessage = std::get<1>(iter->second); //std::get<1>(commandValues.at(receivedCommandID_));
                    }
                    else
                    {
                        if (debugMode_)
                        {
                            std::cout << "[AccerionSensor] Unknown Command received: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                        }
                        receivedTotalBytesInMessage = 0;
                    }
                }
            }
            else
            {
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t *)&receivedNumOfBytesData));
                if (debugMode_)
                {
                    std::cout << "[AccerionSensor::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
                }
            }
        }
        else
        {
            if (debugMode_)
            {
                std::cout << "[AccerionSensor::parseMessages] Received message was broken 1" << std::endl;
            }

            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());

            if (debugMode_)
            {
                std::cout << "[AccerionSensor::parseMessages] Received message " << std::hex << +receivedCommandID_ << std::dec << "was broken 2 with no of bytesleft: " << numberOfBytesLeft << std::endl;
                std::cout << "[AccerionSensor::parseMessages]receivedMessage_. Length after broken 2 is " << receivedMessage_.size() << std::endl;
            }
            
            return;//break;
        }

        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));

        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t *)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if (debugMode_)
            {
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if (debugMode_)
            {
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }

        if (debugMode_)
        {
            std::cout << "Command length of the to be inserted command: " << receivedCommand_.size() << std::endl;
        }

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        if (debugMode_)
        {
            std::cout << "Received serialNumber is := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            std::cout << "Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
        }

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}

void AccerionSensor::toggleAccQRMapping(bool on, _acknowledgementCallBack qrMappingCallback)
{
    accQRMappingCallBack = qrMappingCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_LEARNING_MODE, BooleanCommand(CMD_SET_LEARNING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleAccQRMappingBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_LEARNING_MODE, BooleanCommand(CMD_SET_LEARNING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(accQRMappingAckMutex);
    if (accQRMappingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedAccQRMappingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeAccQRMapping(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (accQRMappingCallBack)
    {
        accQRMappingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(accQRMappingAckMutex);
    receivedAccQRMappingAck = ack;
    accQRMappingAckCV.notify_all();
}

void AccerionSensor::toggleAbsoluteMode(bool on, _acknowledgementCallBack amCallback)
{
    absoluteModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_DRIFT_CORRECTION_MODE, BooleanCommand(CMD_SET_DRIFT_CORRECTION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleAbsoluteModeBlocking(bool on)
{
    //newAbsoluteModeAckReceived = false;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_DRIFT_CORRECTION_MODE, BooleanCommand(CMD_SET_DRIFT_CORRECTION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(absoluteModeAckMutex);
    if (absoluteModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedAbsoluteModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeAbsoluteMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (absoluteModeCallBack)
    {
        absoluteModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(absoluteModeAckMutex);
    receivedAbsoluteModeAck = ack;
    absoluteModeAckCV.notify_all();
}

void AccerionSensor::toggleRecordingMode(bool on, _acknowledgementCallBack amCallback)
{
    recordingModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECORDING_MODE, BooleanCommand(CMD_SET_RECORDING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleRecordingModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECORDING_MODE, BooleanCommand(CMD_SET_RECORDING_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(recordingModeAckMutex);
    if (recordingModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRecordingModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRecordingMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (recordingModeCallBack)
    {
        recordingModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(recordingModeAckMutex);
    receivedRecordingModeAck = ack;
    recordingModeAckCV.notify_all();
}

void AccerionSensor::toggleIdleMode(bool on, _acknowledgementCallBack amCallback)
{
    idleModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IDLE_MODE, BooleanCommand(CMD_SET_IDLE_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleIdleModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IDLE_MODE, BooleanCommand(CMD_SET_IDLE_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(idleModeAckMutex);
    if (idleModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedIdleModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeIdleMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (idleModeCallBack)
    {
        idleModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(idleModeAckMutex);
    receivedIdleModeAck = ack;
    idleModeAckCV.notify_all();
}

void AccerionSensor::toggleRebootMode(bool on, _acknowledgementCallBack amCallback)
{
    rebootModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_REBOOT_MODE, BooleanCommand(CMD_SET_REBOOT_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleRebootModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_REBOOT_MODE, BooleanCommand(CMD_SET_REBOOT_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(rebootModeAckMutex);
    if (rebootModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRebootModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRebootMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (rebootModeCallBack)
    {
        rebootModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(rebootModeAckMutex);
    receivedRebootModeAck = ack;
    rebootModeAckCV.notify_all();
}

void AccerionSensor::toggleCalibrationMode(bool on, _acknowledgementCallBack amCallback)
{
    calibrationModeCallBack = amCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_CALIBRATION_MODE, BooleanCommand(CMD_SET_CALIBRATION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleCalibrationModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_CALIBRATION_MODE, BooleanCommand(CMD_SET_CALIBRATION_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(calibrationModeAckMutex);
    if (calibrationModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedCalibrationModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeCalibrationMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (calibrationModeCallBack)
    {
        calibrationModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(calibrationModeAckMutex);
    receivedCalibrationModeAck = ack;
    calibrationModeAckCV.notify_all();
}

void AccerionSensor::getIPAddress(_ipAddressCallBack ipCallback)
{
    ipAddressCallBack = ipCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_IP_ADDRESS, EmptyCommand(CMD_GET_IP_ADDRESS).serialize());
    outgoingCommandsMutex.unlock();
}

IPAddressExtended AccerionSensor::getIPAddressBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_IP_ADDRESS, EmptyCommand(CMD_GET_IP_ADDRESS).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    if (ipAddressAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        IPAddressExtended empty;
        empty.staticIPAddress.first = 0;
        empty.staticIPAddress.second = 0;
        empty.staticIPAddress.third = 0;
        empty.staticIPAddress.fourth = 0;
        empty.staticNetmask.first = 0;
        empty.staticNetmask.second = 0;
        empty.staticNetmask.third = 0;
        empty.staticNetmask.fourth = 0;
        empty.dynamicIPAddress.first = 0;
        empty.dynamicIPAddress.second = 0;
        empty.dynamicIPAddress.third = 0;
        empty.dynamicIPAddress.fourth = 0;
        empty.dynamicNetmask.first = 0;
        empty.dynamicNetmask.second = 0;
        empty.dynamicNetmask.third = 0;
        empty.dynamicNetmask.fourth = 0;
        empty.defaultGateway.first = 0;
        empty.defaultGateway.second = 0;
        empty.defaultGateway.third = 0;
        empty.defaultGateway.fourth = 0;
        return empty;
    }

    return receivedIPAddress;
}

void AccerionSensor::acknowledgeIPAddress(std::vector<uint8_t> data)
{
    IPAddressExtended ip;
    ip.staticIPAddress.first = data[0];
    ip.staticIPAddress.second = data[1];
    ip.staticIPAddress.third = data[2];
    ip.staticIPAddress.fourth = data[3];
    ip.staticNetmask.first = data[4];
    ip.staticNetmask.second = data[5];
    ip.staticNetmask.third = data[6];
    ip.staticNetmask.fourth = data[7];
    ip.dynamicIPAddress.first = data[8];
    ip.dynamicIPAddress.second = data[9];
    ip.dynamicIPAddress.third = data[10];
    ip.dynamicIPAddress.fourth = data[11];
    ip.dynamicNetmask.first = data[12];
    ip.dynamicNetmask.second = data[13];
    ip.dynamicNetmask.third = data[14];
    ip.dynamicNetmask.fourth = data[15];
    ip.defaultGateway.first = data[16];
    ip.defaultGateway.second = data[17];
    ip.defaultGateway.third = data[18];
    ip.defaultGateway.fourth = data[19];

    if (ipAddressCallBack)
    {
        ipAddressCallBack(ip);
    }
    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    receivedIPAddress = ip;
    ipAddressAckCV.notify_all();
}

void AccerionSensor::getSampleRate(_sampleRateCallBack srCallback)
{
    sampleRateCallBack = srCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SAMPLE_RATE, EmptyCommand(CMD_GET_SAMPLE_RATE).serialize());
    outgoingCommandsMutex.unlock();
}

SampleRate AccerionSensor::getSampleRateBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SAMPLE_RATE, EmptyCommand(CMD_GET_SAMPLE_RATE).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    if (sampleRateAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SampleRate empty;
        empty.sampleRateFrequency = 0;
        return empty;
    }

    return receivedSampleRate;
}

void AccerionSensor::acknowledgeSampleRate(std::vector<uint8_t> data)
{
    SampleRate sr;
    sr.sampleRateFrequency = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (sampleRateCallBack)
    {
        sampleRateCallBack(sr);
    }
    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    receivedSampleRate = sr;
    sampleRateAckCV.notify_all();
}

void AccerionSensor::clearQRLibrary(_acknowledgementCallBack clearQRCallback)
{
    clearQRLibraryCallBack = clearQRCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_COMPLETE_QR_LIBRARY, EmptyCommand(CMD_REMOVE_COMPLETE_QR_LIBRARY).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::clearQRLibraryBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_COMPLETE_QR_LIBRARY, EmptyCommand(CMD_REMOVE_COMPLETE_QR_LIBRARY).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(clearQRLibraryAckMutex);
    if (clearQRLibraryAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedClearQRLibraryAck.value)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeClearQRLibrary(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (clearQRLibraryCallBack)
    {
        clearQRLibraryCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(clearQRLibraryAckMutex);
    receivedClearQRLibraryAck = ack;
    clearQRLibraryAckCV.notify_all();
}

void AccerionSensor::getSerialNumber(_serialNumberCallBack snQRCallback)
{
    serialNumberCallBack = snQRCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_SERIAL_NUMBERS, EmptyCommand(CMD_GET_ALL_SERIAL_NUMBERS).serialize());
    outgoingCommandsMutex.unlock();
}

SerialNumber AccerionSensor::getSerialNumberBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_SERIAL_NUMBERS, EmptyCommand(CMD_GET_ALL_SERIAL_NUMBERS).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(serialNumberAckMutex);
    if (serialNumberAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SerialNumber sn;
        sn.serialNumber = 0;
        return sn;
    }

    return receivedSerialNumber;
}

void AccerionSensor::acknowledgeSerialNumber(std::vector<uint8_t> data)
{
    SerialNumber sn;
    sn.serialNumber = ((uint32_t)ntohl(*((uint32_t *)&data[0])));

    if (serialNumberCallBack)
    {
        serialNumberCallBack(sn);
    }
    std::unique_lock<std::mutex> lck(serialNumberAckMutex);
    receivedSerialNumber = sn;
    serialNumberAckCV.notify_all();
}

void AccerionSensor::clearClusterLibrary(_acknowledgementCallBack clearClusterCallback)
{
    std::cout << "TRYING TO REMOVE CLUSTER LIB" << std::endl;
    clearClusterLibraryCallBack = clearClusterCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY, EmptyCommand(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::clearClusterLibraryBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY, EmptyCommand(CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(clearClusterLibraryAckMutex);
    if (clearClusterLibraryAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedClearClusterLibraryAck.value)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeClearClusterLibrary(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (clearClusterLibraryCallBack)
    {
        clearClusterLibraryCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(clearClusterLibraryAckMutex);
    receivedClearClusterLibraryAck = ack;
    clearClusterLibraryAckCV.notify_all();
}

void AccerionSensor::getAllAcknowledgements()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_ALL_ACKNOWLEDGEMENTS, EmptyCommand(CMD_GET_ALL_ACKNOWLEDGEMENTS).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::getSoftwareVersion(_softwareVersionCallBack svCallback)
{
    softwareVersionCallBack = svCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWARE_VERSION, EmptyCommand(CMD_GET_SOFTWARE_VERSION).serialize());
    outgoingCommandsMutex.unlock();
}

SoftwareVersion AccerionSensor::getSoftwareVersionBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWARE_VERSION, EmptyCommand(CMD_GET_SOFTWARE_VERSION).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(softwareVersionAckMutex);
    if (softwareVersionAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SoftwareVersion sv;
        sv.major = 0;
        sv.minor = 0;
        sv.patch = 0;
        return sv;
    }

    return receivedSoftwareVersion;
}

void AccerionSensor::acknowledgeSoftwareVersion(std::vector<uint8_t> data)
{
    SoftwareVersion sv;
    sv.major = data[0];
    sv.minor = data[1];
    sv.patch = data[2];

    if (softwareVersionCallBack)
    {
        softwareVersionCallBack(sv);
    }
    std::unique_lock<std::mutex> lck(softwareVersionAckMutex);
    receivedSoftwareVersion = sv;
    softwareVersionAckCV.notify_all();
}

void AccerionSensor::getTCPIPInformation(_tcpIPInformationCallBack ipCallback)
{
    tcpIPInformationCallBack = ipCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_TCPIP_RECEIVER, EmptyCommand(CMD_GET_TCPIP_RECEIVER).serialize());
    outgoingCommandsMutex.unlock();
}

TCPIPInformation AccerionSensor::getTCPIPInformationBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_TCPIP_RECEIVER, EmptyCommand(CMD_GET_TCPIP_RECEIVER).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    if (tcpIPInformationAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        TCPIPInformation ti;
        ti.ipAddress.first = 0;
        ti.ipAddress.second = 0;
        ti.ipAddress.third = 0;
        ti.ipAddress.fourth = 0;
        ti.hostIPAddress.first = 0;
        ti.hostIPAddress.second = 0;
        ti.hostIPAddress.third = 0;
        ti.hostIPAddress.fourth = 0;
        ti.messageType = 0;
        return ti;
    }

    return receivedTCPIPInformation;
}

void AccerionSensor::acknowledgeTCPIPInformation(std::vector<uint8_t> data)
{
    TCPIPInformation ti;
    ti.ipAddress.first = data[0];
    ti.ipAddress.second = data[1];
    ti.ipAddress.third = data[2];
    ti.ipAddress.fourth = data[3];
    ti.hostIPAddress.first = data[4];
    ti.hostIPAddress.second = data[5];
    ti.hostIPAddress.third = data[6];
    ti.hostIPAddress.fourth = data[7];
    ti.messageType = data[8];

    if (tcpIPInformationCallBack)
    {
        tcpIPInformationCallBack(ti);
    }
    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    receivedTCPIPInformation = ti;
    tcpIPInformationAckCV.notify_all();
}

void AccerionSensor::getSensorMountPose(_poseCallBack mpCallback)
{
    sensorMountPoseCallBack = mpCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SENSOR_MOUNT_POSE, EmptyCommand(CMD_GET_SENSOR_MOUNT_POSE).serialize());
    outgoingCommandsMutex.unlock();
}

Pose AccerionSensor::getSensorMountPoseBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SENSOR_MOUNT_POSE, EmptyCommand(CMD_GET_SENSOR_MOUNT_POSE).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(mountPoseMutex);
    if (mountPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        Pose ap;
        ap.heading = 0;
        ap.x = 0;
        ap.y = 0;
        return ap;
    }

    return receivedMountPose;
}

void AccerionSensor::toggleExpertMode(bool on, _acknowledgementCallBack emCallback)
{
    expertModeCallBack = emCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_EXPERTMODE, BooleanCommand(CMD_SET_EXPERTMODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleExpertModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_EXPERTMODE, BooleanCommand(CMD_SET_EXPERTMODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(expertModeAckMutex);
    if (expertModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedExpertModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeExpertMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (expertModeCallBack)
    {
        expertModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(expertModeAckMutex);
    receivedExpertModeAck = ack;
    expertModeAckCV.notify_all();
}

void AccerionSensor::setSampleRate(SampleRate rate, _sampleRateCallBack srCallback)
{
    sampleRateCallBack = srCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SAMPLE_RATE, UINT16Command(CMD_SET_NEW_POSE, rate.sampleRateFrequency).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setSampleRateBlocking(SampleRate rate)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, UINT16Command(CMD_SET_NEW_POSE, rate.sampleRateFrequency).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sampleRateAckMutex);
    if (sampleRateAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedSampleRate.sampleRateFrequency == rate.sampleRateFrequency)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::removeQRFromLibrary(uint16_t qrID, _removeQRCallBack rqrCallback)
{
    removeQRCallBack = rqrCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_QR_FROM_LIBRARY, UINT16Command(CMD_REMOVE_QR_FROM_LIBRARY, qrID).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::removeQRFromLibraryBlocking(uint16_t qrID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REMOVE_QR_FROM_LIBRARY, UINT16Command(CMD_REMOVE_QR_FROM_LIBRARY, qrID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(removeQRAckMutex);
    if (removeQRAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRemoveQRAck == qrID)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRemoveQR(std::vector<uint8_t> data)
{
    auto qrID = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (removeQRCallBack)
    {
        removeQRCallBack(qrID);
    }
    std::unique_lock<std::mutex> lck(removeQRAckMutex);
    receivedRemoveQRAck = qrID;
    removeQRAckCV.notify_all();
}

void AccerionSensor::toggleRecoveryMode(bool on, uint8_t radius, _acknowledgementCallBack emCallback)
{
    recoveryModeCallBack = emCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECOVERY_MODE, RecoveryCommand(CMD_SET_RECOVERY_MODE, on, radius).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleRecoveryModeBlocking(bool on, uint8_t radius)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_RECOVERY_MODE, RecoveryCommand(CMD_SET_RECOVERY_MODE, on, radius).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(recoveryModeAckMutex);
    if (recoveryModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRecoveryModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRecoveryMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (recoveryModeCallBack)
    {
        recoveryModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(recoveryModeAckMutex);
    receivedRecoveryModeAck = ack;
    recoveryModeAckCV.notify_all();
}

void AccerionSensor::removeClusterFromLibrary(uint16_t clusterID, _removeClusterCallBack rclusterCallback)
{
    removeClusterCallBack = rclusterCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_DELETE_CLUSTER, UINT16Command(CMD_DELETE_CLUSTER, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::removeClusterFromLibraryBlocking(uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_DELETE_CLUSTER, UINT16Command(CMD_DELETE_CLUSTER, clusterID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(removeClusterAckMutex);
    if (removeClusterAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedRemoveClusterAck == clusterID)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeRemoveCluster(std::vector<uint8_t> data)
{
    auto clusterID = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (removeClusterCallBack)
    {
        removeClusterCallBack(clusterID);
    }
    std::unique_lock<std::mutex> lck(removeClusterAckMutex);
    receivedRemoveClusterAck = clusterID;
    removeClusterAckCV.notify_all();
}

void AccerionSensor::getSecondaryLineFollowerOutput(uint16_t clusterID, _secondaryLineFollowerCallBack slfCallback)
{
    secondaryLineFollowerCallBack = slfCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SECOND_LINE, UINT16Command(CMD_SECOND_LINE, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

LineFollowerData AccerionSensor::getSecondaryLineFollowerOutputBlocking(uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SECOND_LINE, UINT16Command(CMD_SECOND_LINE, clusterID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(secondaryLineFollowerOutputMutex);
    if (secondaryLineFollowerOutputCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        LineFollowerData lfd;
        lfd.timeStamp = 0;
        lfd.pose.x = 0;
        lfd.pose.y = 0;
        lfd.pose.heading = 0;
        lfd.closestPointX = 0;
        lfd.closestPointY = 0;
        lfd.reserved = 0;
        lfd.clusterID = 0;
        return lfd;
    }

    return receivedSecondaryLineFollowerOutput;
}

void AccerionSensor::acknowledgeSecondaryLineFollowerOutput(std::vector<uint8_t> data)
{
    LineFollowerData lfd;
    lfd.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    lfd.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    lfd.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    lfd.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    lfd.closestPointX = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    lfd.closestPointY = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    lfd.reserved = (int32_t)ntohl(*((int32_t *)&data[28]));
    lfd.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[32]));
    if (secondaryLineFollowerCallBack)
    {
        secondaryLineFollowerCallBack(lfd);
    }
    std::unique_lock<std::mutex> lck(secondaryLineFollowerOutputMutex);
    receivedSecondaryLineFollowerOutput = lfd;
    secondaryLineFollowerOutputCV.notify_all();
}

void AccerionSensor::setIPAddress(IPAddress ip, _ipAddressCallBack ipCallback)
{
    ipAddressCallBack = ipCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IP_ADDRESS, SetIPCommand(CMD_SET_IP_ADDRESS, ip.ipAddress.first, ip.ipAddress.second, ip.ipAddress.third, ip.ipAddress.fourth, ip.netmask.first, ip.netmask.second, ip.netmask.third, ip.netmask.fourth, ip.gateway.first, ip.gateway.second, ip.gateway.third, ip.gateway.fourth).serialize());
    outgoingCommandsMutex.unlock();
}

IPAddressExtended AccerionSensor::setIPAddressBlocking(IPAddress ip)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_IP_ADDRESS, SetIPCommand(CMD_SET_IP_ADDRESS, ip.ipAddress.first, ip.ipAddress.second, ip.ipAddress.third, ip.ipAddress.fourth, ip.netmask.first, ip.netmask.second, ip.netmask.third, ip.netmask.fourth, ip.gateway.first, ip.gateway.second, ip.gateway.third, ip.gateway.fourth).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(ipAddressAckMutex);
    if (ipAddressAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        IPAddressExtended empty;
        empty.staticIPAddress.first = 0;
        empty.staticIPAddress.second = 0;
        empty.staticIPAddress.third = 0;
        empty.staticIPAddress.fourth = 0;
        empty.staticNetmask.first = 0;
        empty.staticNetmask.second = 0;
        empty.staticNetmask.third = 0;
        empty.staticNetmask.fourth = 0;
        empty.dynamicIPAddress.first = 0;
        empty.dynamicIPAddress.second = 0;
        empty.dynamicIPAddress.third = 0;
        empty.dynamicIPAddress.fourth = 0;
        empty.dynamicNetmask.first = 0;
        empty.dynamicNetmask.second = 0;
        empty.dynamicNetmask.third = 0;
        empty.dynamicNetmask.fourth = 0;
        empty.defaultGateway.first = 0;
        empty.defaultGateway.second = 0;
        empty.defaultGateway.third = 0;
        empty.defaultGateway.fourth = 0;
        return empty;
    }

    return receivedIPAddress;
}

void AccerionSensor::addQRToLibrary(uint16_t qrID, Pose qrPose, _addQRCallBack qrCallback)
{
    addQRCallBack = qrCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_ADD_QR_TO_LIBRARY, AddQRCommand(CMD_ADD_QR_TO_LIBRARY, qrID, qrPose.x, qrPose.y, qrPose.heading).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::addQRToLibraryBlocking(uint16_t qrID, Pose qrPose)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_ADD_QR_TO_LIBRARY, AddQRCommand(CMD_ADD_QR_TO_LIBRARY, qrID, qrPose.x, qrPose.y, qrPose.heading).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(addQRMutex);
    if (addQRCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedAddQRAck.result && receivedAddQRAck.qrID == qrID)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeAddQR(std::vector<uint8_t> data)
{
    AddQRResult aqr;

    aqr.qrID = (uint16_t)ntohs(*((uint16_t *)&data[0]));
    if (receivedCommand_[2] == 0x01)
    {
        aqr.result = true;
    }
    else if (receivedCommand_[2] == 0x02)
    {
        aqr.result = false;
    }

    if (addQRCallBack)
    {
        addQRCallBack(aqr);
    }
    std::unique_lock<std::mutex> lck(addQRMutex);
    receivedAddQRAck = aqr;
    addQRCV.notify_all();
}

void AccerionSensor::setDateTime(DateTime dt, _dateTimeCallBack dtCallback)
{
    dateTimeCallBack = dtCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TIME_DATE, DateTimeCommand(CMD_SET_TIME_DATE, dt.day, dt.month, dt.year, dt.hours, dt.minutes, dt.seconds).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setDateTimeBlocking(DateTime dt)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TIME_DATE, DateTimeCommand(CMD_SET_TIME_DATE, dt.day, dt.month, dt.year, dt.hours, dt.minutes, dt.seconds).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(dateTimeMutex);
    if (dateTimeCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedDateTimeAck.day == dt.day && receivedDateTimeAck.month == dt.month && receivedDateTimeAck.year == dt.year && receivedDateTimeAck.hours == dt.hours && receivedDateTimeAck.minutes == dt.minutes && receivedDateTimeAck.seconds == dt.seconds)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeDateTime(std::vector<uint8_t> data)
{
    DateTime dt;
    dt.year = (uint16_t)ntohs(*((uint16_t *)&data[0]));
    dt.month = data[2];
    dt.day = data[3];
    dt.hours = data[4];
    dt.minutes = data[5];
    dt.seconds = data[6];

    if (dateTimeCallBack)
    {
        dateTimeCallBack(dt);
    }
    std::unique_lock<std::mutex> lck(dateTimeMutex);
    receivedDateTimeAck = dt;
    dateTimeCV.notify_all();
}

void AccerionSensor::setSensorPose(Pose mountPoseStruct, _poseCallBack mpCallback)
{
    sensorPoseCallBack = mpCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, PoseCommand(CMD_SET_NEW_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setSensorPoseBlocking(Pose mountPoseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_NEW_POSE, PoseCommand(CMD_SET_NEW_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(sensorPoseMutex);
    if (sensorPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (round(receivedSensorPose.x / 1000000.0) == round(mountPoseStruct.x / 1000000.0) && round(receivedSensorPose.y / 1000000.0) == round(mountPoseStruct.y / 1000000.0) && round(receivedSensorPose.heading / 100.0) == round(mountPoseStruct.heading / 100.0))
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeSensorPose(std::vector<uint8_t> data)
{
    Pose sensorPoseStruct;
    sensorPoseStruct.x = ((int32_t)ntohl(*((int32_t *)&data[0]))) / 1000000.0;
    sensorPoseStruct.y = ((int32_t)ntohl(*((int32_t *)&data[4]))) / 1000000.0;
    sensorPoseStruct.heading = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 100.0;
    if (sensorPoseCallBack)
    {
        sensorPoseCallBack(sensorPoseStruct);
    }
    std::unique_lock<std::mutex> lck(sensorPoseMutex);
    receivedSensorPose = sensorPoseStruct;
    sensorPoseCV.notify_all();
}

void AccerionSensor::setSensorMountPose(Pose mountPoseStruct, _poseCallBack mpCallback)
{
    sensorMountPoseCallBack = mpCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SENSOR_MOUNT_POSE, PoseCommand(CMD_SET_SENSOR_MOUNT_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setSensorMountPoseBlocking(Pose mountPoseStruct)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_SENSOR_MOUNT_POSE, PoseCommand(CMD_SET_SENSOR_MOUNT_POSE, mountPoseStruct.x, mountPoseStruct.y, mountPoseStruct.heading).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(mountPoseMutex);
    if (mountPoseCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (round(receivedMountPose.x / 1000000.0) == round(mountPoseStruct.x / 1000000.0) && round(receivedMountPose.y / 1000000.0) == round(mountPoseStruct.y / 1000000.0) && round(receivedMountPose.heading / 100.0) == round(mountPoseStruct.heading / 100.0))
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::acknowledgeMountPose(std::vector<uint8_t> data)
{
    Pose mountPoseStruct;
    mountPoseStruct.x = ((int32_t)ntohl(*((int32_t *)&data[0]))) / 1000000.0;
    mountPoseStruct.y = ((int32_t)ntohl(*((int32_t *)&data[4]))) / 1000000.0;
    mountPoseStruct.heading = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 100.0;
    if (sensorMountPoseCallBack)
    {
        sensorMountPoseCallBack(mountPoseStruct);
    }
    std::unique_lock<std::mutex> lck(mountPoseMutex);
    receivedMountPose = mountPoseStruct;
    mountPoseCV.notify_all();
}

void AccerionSensor::setPoseAndCovariance(InputPose inputPose)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_POSE_AND_COVARIANCE, PoseAndCovarianceCommand(CMD_SET_POSE_AND_COVARIANCE, inputPose.timeStamp, inputPose.pose.x, inputPose.pose.y, inputPose.pose.heading, inputPose.standardDeviation.x, inputPose.standardDeviation.y, inputPose.standardDeviation.theta).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::setTCPIPReceiver(Address ipAddr, uint8_t messageType, _tcpIPInformationCallBack tcpIPCallback)
{
    tcpIPInformationCallBack = tcpIPCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TCPIP_RECEIVER, TCPIPReceiverCommand(CMD_SET_TCPIP_RECEIVER, ipAddr.first, ipAddr.second, ipAddr.third, ipAddr.fourth, messageType).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setTCPIPReceiverBlocking(Address ipAddr, uint8_t messageType) /** < Returns -1 on error, 0 if it is off, 1 if it is on */
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_TCPIP_RECEIVER, TCPIPReceiverCommand(CMD_SET_TCPIP_RECEIVER, ipAddr.first, ipAddr.second, ipAddr.third, ipAddr.fourth, messageType).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(tcpIPInformationAckMutex);
    if (tcpIPInformationAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedTCPIPInformation.hostIPAddress.first == ipAddr.first && receivedTCPIPInformation.hostIPAddress.second == ipAddr.second && receivedTCPIPInformation.hostIPAddress.third == ipAddr.third && receivedTCPIPInformation.hostIPAddress.fourth == ipAddr.fourth && receivedTCPIPInformation.messageType == messageType)
    {
        return 1;
    }
    return 0;
}

void AccerionSensor::toggleMapping(bool on, uint16_t clusterID, _acknowledgementCallBack mappingCallback)
{
    toggleMappingCallBack = mappingCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_MARKERLESS_LEARNING, ToggleMappingCommand(CMD_START_MARKERLESS_LEARNING, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleMappingBlocking(bool on, uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_MARKERLESS_LEARNING, ToggleMappingCommand(CMD_START_MARKERLESS_LEARNING, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(mappingAckMutex);
    if (mappingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedMappingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeMappingToggle(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleMappingCallBack)
    {
        toggleMappingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(mappingAckMutex);
    receivedMappingAck = ack;
    mappingAckCV.notify_all();
}

void AccerionSensor::toggleLineFollowing(bool on, uint16_t clusterID, _acknowledgementCallBack tlfCallback)
{
    toggleLineFollowingCallBack = tlfCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_LINE_FOLLOWER, ToggleMappingCommand(CMD_START_LINE_FOLLOWER, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleLineFollowingBlocking(bool on, uint16_t clusterID)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_LINE_FOLLOWER, ToggleMappingCommand(CMD_START_LINE_FOLLOWER, on, clusterID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(lineFollowingAckMutex);
    if (lineFollowingAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedLineFollowingAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeLineFollowingToggle(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleLineFollowingCallBack)
    {
        toggleLineFollowingCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(lineFollowingAckMutex);
    receivedLineFollowingAck = ack;
    lineFollowingAckCV.notify_all();
}

void AccerionSensor::setUDPSettings(UDPInfo udpInfo, _setUDPSettingsCallBack udpCallBack)
{
    setUDPSettingsCallBack = udpCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_UDP_SETTINGS, UDPSettingsCommand(CMD_SET_UDP_SETTINGS, udpInfo.ipAddress.first, udpInfo.ipAddress.second, udpInfo.ipAddress.third, udpInfo.ipAddress.fourth, udpInfo.messageType, udpInfo.broadOrUniCast).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setUDPSettingsBlocking(UDPInfo udpInfo)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_UDP_SETTINGS, UDPSettingsCommand(CMD_SET_UDP_SETTINGS, udpInfo.ipAddress.first, udpInfo.ipAddress.second, udpInfo.ipAddress.third, udpInfo.ipAddress.fourth, udpInfo.messageType, udpInfo.broadOrUniCast).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(setUDPSettingsAckMutex);
    if (setUDPSettingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedSetUDPSettingsAck.ipAddress.first == udpInfo.ipAddress.first && receivedSetUDPSettingsAck.ipAddress.second == udpInfo.ipAddress.second && receivedSetUDPSettingsAck.ipAddress.third == udpInfo.ipAddress.third && receivedSetUDPSettingsAck.ipAddress.fourth == udpInfo.ipAddress.fourth && receivedSetUDPSettingsAck.messageType == udpInfo.messageType && receivedSetUDPSettingsAck.broadOrUniCast == udpInfo.broadOrUniCast)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeUDPSettings(std::vector<uint8_t> data)
{
    UDPInfo udpInfo;
    udpInfo.messageType = data[0];
    udpInfo.broadOrUniCast = data[1];
    udpInfo.ipAddress.first = data[2];
    udpInfo.ipAddress.second = data[3];
    udpInfo.ipAddress.third = data[4];
    udpInfo.ipAddress.fourth = data[5];

    if (setUDPSettingsCallBack)
    {
        setUDPSettingsCallBack(udpInfo);
    }
    std::unique_lock<std::mutex> lck(setUDPSettingsAckMutex);
    receivedSetUDPSettingsAck = udpInfo;
    setUDPSettingsAckCV.notify_all();
}

void AccerionSensor::getClusterInG2OFormat(uint16_t clusterID, _clusterInG2OFormatCallBack g2oCallBack)
{
    clusterInG2OFormatCallBack = g2oCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_CLUSTER_G2O, UINT16Command(CMD_GET_CLUSTER_G2O, clusterID).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::getClusterInG2OFormat(uint16_t clusterID, std::string filePath)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_CLUSTER_G2O, UINT16Command(CMD_GET_CLUSTER_G2O, clusterID).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(clusterInG2OAckMutex);
    if (setUDPSettingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }
    // write to filePath
    try
    {
        std::string str(receivedClusterInG2OAck.begin(), receivedClusterInG2OAck.end());
        // std::string id = std::to_string(clusterID);
        if (!doesStringEndWith(filePath, ".g2o"))
        {
            filePath.append(".g2o");
        }
        std::ofstream out(filePath.c_str());
        out << str;
        out.close();
        return 1;
    }
    catch (std::exception &e)
    {
        return 0;
    }
    return 0;
}

void AccerionSensor::acknowledgeClusterInG2OFormat(std::vector<uint8_t> data)
{
    std::vector<uint8_t> receivedG2O = data;

    if (clusterInG2OFormatCallBack)
    {
        clusterInG2OFormatCallBack(receivedG2O);
    }
    std::unique_lock<std::mutex> lck(clusterInG2OAckMutex);
    receivedClusterInG2OAck = receivedG2O;
    clusterInG2OAckCV.notify_all();
}

void AccerionSensor::replaceClusterWithG2OFormat(uint16_t clusterID, std::string filePath, _acknowledgementCallBack g2oCallBack)
{
    replaceClusterG2OCallBack = g2oCallBack;

    std::vector<uint8_t> dataToSend;
    std::ifstream in(filePath.c_str());
    // Check if object is valid
    if (!in)
    {
        //std::cout << "File could not be loaded" << std::endl;
        return;
    }
    //std::cout << "File is loaded!" << std::endl;

    std::ifstream fstream(filePath.c_str(), std::ios::in | std::ios::binary);
    std::vector<uint8_t> contents((std::istreambuf_iterator<char>(fstream)), std::istreambuf_iterator<char>());

    uint32_t messageLength = contents.size() + sizeof(clusterID) + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH;

    for (auto i : contents)
    {
        dataToSend.emplace_back(i);
    }

    in.close();

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REPLACE_CLUSTER_G2O, G2OCommand(CMD_REPLACE_CLUSTER_G2O, messageLength, clusterID, dataToSend).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::replaceClusterWithG2OFormatBlocking(uint16_t clusterID, std::string filePath)
{

    std::vector<uint8_t> dataToSend;
    std::ifstream in(filePath.c_str());
    // Check if object is valid
    if (!in)
    {
        //std::cout << "File could not be loaded" << std::endl;
        return -1;
    }
    //std::cout << "File is loaded!" << std::endl;

    std::ifstream fstream(filePath.c_str(), std::ios::in | std::ios::binary);
    std::vector<uint8_t> contents((std::istreambuf_iterator<char>(fstream)), std::istreambuf_iterator<char>());

    uint32_t messageLength = contents.size() + sizeof(clusterID) + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH;

    for (auto i : contents)
    {
        dataToSend.emplace_back(i);
    }

    in.close();

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_REPLACE_CLUSTER_G2O, G2OCommand(CMD_REPLACE_CLUSTER_G2O, messageLength, clusterID, dataToSend).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(clusterInG2OAckMutex);
    if (setUDPSettingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedLineFollowingAck.value)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeReplaceClusterWithG2OFormat(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (replaceClusterG2OCallBack)
    {
        replaceClusterG2OCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(replaceClusterWithG2OAckMutex);
    receivedReplaceClusterWithG2OAck = ack;
    replaceClusterWithG2OAckCV.notify_all();
}

void AccerionSensor::subscribeToHeartBeat(_heartBeatCallBack hbCallback)
{
    heartBeatCallBack = hbCallback;
}

void AccerionSensor::outputHeartBeat(std::vector<uint8_t> data)
{
    HeartBeat hb;
    hb.ipAddrFirst = data[0];
    hb.ipAddrSecond = data[1];
    hb.ipAddrThird = data[2];
    hb.ipAddrFourth = data[3];
    hb.uniFirst = data[4];
    hb.uniSecond = data[5];
    hb.uniThird = data[6];
    hb.uniFourth = data[7];

    if (heartBeatCallBack)
    {
        heartBeatCallBack(hb);
    }
}

void AccerionSensor::subscribeToCorrectedPose(_correctedPoseCallBack cpCallback)
{
    correctedPoseCallBack = cpCallback;
}

void AccerionSensor::outputCorrectedPose(std::vector<uint8_t> data)
{
    CorrectedPose cp;

    cp.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    cp.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    cp.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    cp.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    cp.xVel = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    cp.yVel = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    cp.thVel = ((int16_t)ntohl(*((int16_t *)&data[28]))) / 100.0;

    cp.standardDeviation.x = ((uint32_t)ntohl(*((uint32_t *)&data[30]))) / 1000000.0;
    cp.standardDeviation.y = ((uint32_t)ntohl(*((uint32_t *)&data[34]))) / 1000000.0;
    cp.standardDeviation.theta = ((uint32_t)ntohl(*((uint32_t *)&data[38]))) / 100.0;

    cp.standardDeviationVelocity.x = ((uint32_t)ntohl(*((uint32_t *)&data[42]))) / 1000000.0;
    cp.standardDeviationVelocity.y = ((uint32_t)ntohl(*((uint32_t *)&data[46]))) / 1000000.0;
    cp.standardDeviationVelocity.theta = ((uint32_t)ntohl(*((uint32_t *)&data[50]))) / 100.0;

    cp.qualityEstimate = data[54];

    if (correctedPoseCallBack)
    {
        correctedPoseCallBack(cp);
    }
}

void AccerionSensor::subscribeToUncorrectedPose(_uncorrectedPoseCallBack upCallback)
{
    uncorrectedPoseCallBack = upCallback;
}

void AccerionSensor::outputUncorrectedPose(std::vector<uint8_t> data)
{
    UncorrectedPose up;

    up.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;

    up.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    up.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    up.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;

    up.xVel = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    up.yVel = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    up.thVel = ((int16_t)ntohl(*((int16_t *)&data[28]))) / 100.0;

    up.standardDeviationVelocity.x = ((uint32_t)ntohl(*((uint32_t *)&data[30]))) / 1000000.0;
    up.standardDeviationVelocity.y = ((uint32_t)ntohl(*((uint32_t *)&data[34]))) / 1000000.0;
    up.standardDeviationVelocity.theta = ((uint32_t)ntohl(*((uint32_t *)&data[38]))) / 100.0;
    up.qualityEstimate = data[42];

    if (uncorrectedPoseCallBack)
    {
        uncorrectedPoseCallBack(up);
    }
}

void AccerionSensor::subscribeToDiagnostics(_diagnosticsCallBack diagCallback)
{
    diagnosticsCallBack = diagCallback;
}

void AccerionSensor::outputDiagnostics(std::vector<uint8_t> data)
{
    Diagnostics diag;

    diag.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    diag.modes = (uint16_t)ntohs(*((uint16_t *)&data[8]));
    diag.warningCodes = (uint16_t)ntohs(*((uint16_t *)&data[10]));
    diag.errorCodes = (uint32_t)ntohl(*((uint32_t *)&data[12]));
    diag.statusCodes = data[16];

    if (diagnosticsCallBack)
    {
        diagnosticsCallBack(diag);
    }
}

void AccerionSensor::subscribeToDriftCorrections(_driftCorrectionCallBack dcCallback)
{
    driftCorrectionCallBack = dcCallback;
}

void AccerionSensor::outputDriftCorrection(std::vector<uint8_t> data)
{
    DriftCorrection dc;
    dc.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    dc.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    dc.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    dc.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    dc.xDelta = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    dc.yDelta = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    dc.thDelta = ((int32_t)ntohl(*((int32_t *)&data[28]))) / 100.0;

    dc.cumulativeTravelledDistance = ((uint32_t)ntohl(*((uint32_t *)&data[32]))) / 1000000.0;
    dc.cumulativeTravelledHeading = ((uint32_t)ntohl(*((uint32_t *)&data[36]))) / 100.0;
    dc.errorPercentage = (uint32_t)ntohl(*((uint32_t *)&data[40]));

    dc.QRID = (uint16_t)ntohs(*((uint16_t *)&data[44]));
    dc.typeOfCorrection = data[46]; //1 = infraless, 2 = qr, 3 = manual
    dc.qualityEstimate = data[47];

    if (driftCorrectionCallBack)
    {
        driftCorrectionCallBack(dc);
    }
}

void AccerionSensor::subscribeToQualityEstimates(_qualityEstimateCallBack qeCallback)
{
    qualityEstimateCallBack = qeCallback;
}

void AccerionSensor::outputQualityEstimate(std::vector<uint8_t> data)
{
    QualityEstimate qe;
    qe.qualityEstimator1 = data[0];
    qe.qualityEstimator2 = data[1];
    qe.qualityEstimator3 = data[2];
    qe.qualityEstimator4 = data[3];
    qe.qualityEstimator5 = data[4];
    qe.qualityEstimator6 = data[5];
    qe.qualityEstimator7 = data[6];
    qe.qualityEstimator8 = data[7];
    qe.qualityEstimator9 = data[8];
    qe.qualityEstimator10 = data[9];

    if (qualityEstimateCallBack)
    {
        qualityEstimateCallBack(qe);
    }
}

void AccerionSensor::subscribeToLineFollowerData(_lineFollowerCallBack lfCallback)
{
    lineFollowerCallBack = lfCallback;
}

void AccerionSensor::outputLineFollowerData(std::vector<uint8_t> data)
{
    LineFollowerData lfd;
    lfd.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    lfd.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    lfd.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    lfd.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    lfd.closestPointX = ((int32_t)ntohl(*((int32_t *)&data[20]))) / 1000000.0;
    lfd.closestPointY = ((int32_t)ntohl(*((int32_t *)&data[24]))) / 1000000.0;
    lfd.reserved = (int32_t)ntohl(*((int32_t *)&data[28]));
    lfd.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[32]));
    if (lineFollowerCallBack)
    {
        lineFollowerCallBack(lfd);
    }
}

void AccerionSensor::subscribeToMarkerPosPacketStartStop(_acknowledgementCallBack mpCallBack)
{
    markerPosStartStopCallBack = mpCallBack;
}

void AccerionSensor::acknowledgeMarkerPosPacketStartStop(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (markerPosStartStopCallBack)
    {
        markerPosStartStopCallBack(ack);
    }
}

void AccerionSensor::subscribeToMarkerPosPacket(_markerPosPacketCallBack mppCallback)
{
    markerPosPacketCallBack = mppCallback;
}

void AccerionSensor::outputMarkerPosPacket(std::vector<uint8_t> data)
{
    MarkerPosPacket mpp;
    mpp.numberOfMarkersInMessage = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    for (int i = 0; i < mpp.numberOfMarkersInMessage; i++)
    {
        Marker m;
        int baseIndex = i * 23;
        m.xPos = ((int32_t)ntohl(*((int32_t *)&data[baseIndex + 2]))) / 1000000.0;
        m.yPos = ((int32_t)ntohl(*((int32_t *)&data[baseIndex + 6]))) / 1000000.0;
        m.clusterID = (uint16_t)ntohs(*((uint16_t *)&data[baseIndex + 10]));
        m.signatureID = (uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 12]));
        m.sigStatus = (uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 16]));
        m.stdDevX = ((uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 17]))) / 1000000.0;
        m.stdDevY = ((uint32_t)ntohl(*((uint32_t *)&data[baseIndex + 21]))) / 1000000.0;
        mpp.markers.push_back(m);
    }

    if (markerPosPacketCallBack)
    {
        markerPosPacketCallBack(mpp);
    }
}

void AccerionSensor::requestMarkerMap(_markerPosPacketCallBack mppCallback)
{
    subscribeToMarkerPosPacket(mppCallback);
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SIGNATURE_MARKER_MAP, EmptyCommand(CMD_GET_SIGNATURE_MARKER_MAP).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::subscribeToConsoleOutputInfo(_consoleOutputCallback conCallback)
{
    consoleOutputCallBack = conCallback;
}

void AccerionSensor::outputConsoleOutputInfo(std::vector<uint8_t> data)
{
    std::string msg(data.begin() + MESSAGE_SIZE_LENGTH, data.end());

    if (consoleOutputCallBack)
    {
        consoleOutputCallBack(msg);
    }
}

bool AccerionSensor::getMap(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!isInProgress)
    {
        isInProgress = true;
        totalMessagesToBeTransferred_ = 0;
        msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        mapSharingPath_ = destinationPath;
        filesSuccessfullyTransferred = true;
        totalFileSize_ = 0;
        totalsent = 0;
        return retrieveFirstMapPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::retrieveFirstMapPiece()
{

    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }

    if (FileExists(mapSharingPath_))
    {
        if (remove(mapSharingPath_.c_str()) != 0)
        {
            statusCallBack(FAILED_TO_REMOVE_EXISTING);
            filesSuccessfullyTransferred = false;
            isInProgress = false;
            return false;
        }
    }
    statusCallBack(PACKING_MAP);
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_MAP, UINT32Command(CMD_GET_MAP, 0).serialize());
    outgoingCommandsMutex.unlock();
    return true;
}

void AccerionSensor::retrievedMapPiece(std::vector<uint8_t> receivedCommand_)
{
    //quint8 cmdType = *(reinterpret_cast<quint8*>(&receivedCommand_[4]));
    //uint32_t msgLength  = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[0]));
    if (receivedCommand_[4] == 0x00) // done, check next
    {
        std::cout << "MAP DONE FOR: " << +sensorSerialNumber_ << std::endl;
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        doneCallBack(filesSuccessfullyTransferred);
    }
    else if (receivedCommand_[4] == 0x01) // fail, check next
    {
        std::cout << "RECEIVED A MAP FAIL FROM SENSOR: " << +sensorSerialNumber_ << std::endl;
        isInProgress = false;
        if (totalMessagesToBeTransferred_ != 0)
        {
            if(mapSharingFile != NULL)
            {
                fclose(mapSharingFile);
                mapSharingFile = NULL;
            }
        }
        filesSuccessfullyTransferred = false;
        doneCallBack(filesSuccessfullyTransferred);
    }
    else if (receivedCommand_[4] == 0x02) // info received
    {
        std::cout << "RECEIVED MAP INFO FROM SENSOR: " << +sensorSerialNumber_ << std::endl;
        totalMessagesToBeTransferred_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        mapSharingFile = fopen(mapSharingPath_.c_str(), "ab");
        if (mapSharingFile)
        {
            msgcounter++;
        }
        retrieveNextMapPiece();
    }
    else if (receivedCommand_[4] == 0x03) // pckg received
    {
        std::cout << "RECEIVED A MAP PACKAGE FROM SENSOR: " << +sensorSerialNumber_ << std::endl;
        double prgrss = ((msgcounter)*100.0 / totalMessagesToBeTransferred_);
        statusCallBack(RETRIEVING_MAP);
        progressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = receivedCommand_.size() - 5;

        auto *buffer = new unsigned char[bytesRead];

        for (int i = 5; i < receivedCommand_.size(); i++)
        {
            buffer[i - 5] = *((unsigned char *)&receivedCommand_[i]);
        }

        fwrite(buffer, sizeof(unsigned char), bytesRead, mapSharingFile);

        fflush(mapSharingFile);

        delete[] buffer;
        msgcounter++;
        retrieveNextMapPiece();
    }
}

void AccerionSensor::retrieveNextMapPiece()
{
    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return;
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_MAP, UINT32Command(CMD_GET_MAP, msgcounter).serialize());
    outgoingCommandsMutex.unlock();
}

bool AccerionSensor::sendMap(std::string sourcePath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB, int strategy)
{
    if (!isInProgress)
    {
        mapStrategy = strategy;
        totalMessagesToBeTransferred_ = 0;
        msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        mapSharingPath_ = sourcePath;
        return sendFirstMapPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::sendFirstMapPiece()
{
    msgcounter = 0;
    totalsent = 0;
    totalFileSize_ = 0;
    totalMessagesToBeTransferred_ = 0;

    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }

    std::ifstream in(mapSharingPath_, std::ios::ate);
    in.seekg(0, std::ios::end);
    totalFileSize_ = static_cast<int>(in.tellg());

    // Check if object is valid
    if (!in)
    {
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        statusCallBack(FAILED_TO_OPEN_FILE);
        return false;
    }

    in.close();
    totalMessagesToBeTransferred_ = static_cast<int>(ceil(static_cast<double>(totalFileSize_) / bufferSize));

    //Sending start message
    uint32_t messageLength = 0 + CommandFields::PACKAGE_NO_LENGTH + CommandFields::PACKAGE_RESULT_LENGTH + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH;
    //uint8_t option = 0;
    std::vector<uint8_t> empty;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_PLACE_MAP, PlaceMapCommand(CMD_PLACE_MAP, messageLength, 0, 0, empty).serialize()); // first 0 is packettype = start, second is messageIndex, which is still 0 here.
    outgoingCommandsMutex.unlock();
    //socketsVector[transferCounter]->tcpSocket_.flush();

    //sending first packet message
    mapSharingFile = fopen(mapSharingPath_.c_str(), "rb");
    if (tcpClient->getConnected())
    {
        if (mapSharingFile)
        {
            size_t bytesRead = 0;
            unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
            // read up to sizeof(buffer) bytes
            bytesRead = fread(buffer, 1, sizeof(buffer), mapSharingFile);

            // process bytesRead worth of data in buffer

            auto messageLength = static_cast<uint32_t>(bytesRead + CommandFields::PACKAGE_NO_LENGTH + CommandFields::PACKAGE_RESULT_LENGTH + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH);
            std::vector<uint8_t> dataToSend;
            for (int i = 0; i < static_cast<int>(bytesRead); i++) // send package per buffer size
            {
                dataToSend.emplace_back(buffer[i]);
            }
            outgoingCommandsMutex.lock();
            outgoingCommands.emplace_back(CMD_PLACE_MAP, PlaceMapCommand(CMD_PLACE_MAP, messageLength, 1, 0, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
            outgoingCommandsMutex.unlock();

            totalsent += bytesRead;
            msgcounter++;
            statusCallBack(SENDING_MAP);
            //socketsVector[transferCounter]->tcpSocket_.flush();
            return true;
        }
        else
        {
            filesSuccessfullyTransferred = false;
            isInProgress = false;
            statusCallBack(FAILED_TO_OPEN_FILE);
            return false;
        }
    }
    else
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        return false;
    }
}

void AccerionSensor::retrievedMapAck(std::vector<uint8_t> receivedCommand_) // 1 = map done, 2 = failed, 3 = pckg success, 4 = package failed, 5 = encrypting, 6=  unzipping
{
    if (receivedCommand_[0] == 0x01) // update succeeded, check next
    {
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        doneCallBack(filesSuccessfullyTransferred);
    }
    else if (receivedCommand_[0] == 0x02) // update failed, check next
    {
        filesSuccessfullyTransferred = false;
        isInProgress = false;
        if(mapSharingFile != NULL)
        {
            fclose(mapSharingFile);
            mapSharingFile = NULL;
        }
        doneCallBack(filesSuccessfullyTransferred);
    }
    else if (receivedCommand_[0] == 0x03 || receivedCommand_[0] == 0x04) // either send next message, current message or stop message
    {
        double prgrss = ((msgcounter)*100.0 / totalMessagesToBeTransferred_);
        statusCallBack(SENDING_MAP);
        progressCallBack(static_cast<int>(prgrss));
        //int msgReceived = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[1]));

        if (prgrss == 100.0)
        {
            statusCallBack(UNPACKING_AND_REPLACING_MAP);
        }
        if (static_cast<int>(msgcounter) == totalMessagesToBeTransferred_ && totalsent == totalFileSize_) // everything has been sent, so sending stop message
        {
            uint32_t messageLength = static_cast<uint32_t>(0 + CommandFields::PACKAGE_NO_LENGTH + CommandFields::PACKAGE_RESULT_LENGTH + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH);
            uint8_t option = 2; // 2 = normal stop, which will replace
            std::vector<uint8_t> empty;
            if (mapStrategy == 1)
            {
                option = 3;
            }
            if (tcpClient->getConnected())
            {
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_PLACE_MAP, PlaceMapCommand(CMD_PLACE_MAP, messageLength, option, 0, empty).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
            }
            else
            {
                isInProgress = false;
                filesSuccessfullyTransferred = false;
                statusCallBack(CONNECTION_FAILED);
                return;
            }
        }
        else
        {
            //send next message..
            if (mapSharingFile)
            {
                unsigned char buffer[bufferSize]; // array of bytes, not pointers-to-bytes
                fseek(mapSharingFile, msgcounter * sizeof(buffer), SEEK_SET);
                int bytesRead = fread(buffer, 1, sizeof(buffer), mapSharingFile);
                auto messageLength = static_cast<uint32_t>(bytesRead + CommandFields::PACKAGE_NO_LENGTH + CommandFields::PACKAGE_RESULT_LENGTH + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH);
                std::vector<uint8_t> dataToSend;
                for (int i = 0; i < bytesRead; i++) // send package per buffer size
                {
                    dataToSend.emplace_back(buffer[i]);
                }
                outgoingCommandsMutex.lock();
                outgoingCommands.emplace_back(CMD_PLACE_MAP, PlaceMapCommand(CMD_PLACE_MAP, messageLength, 1, msgcounter, dataToSend).serialize()); //1 for first message, e.g. packettype = packet, 0 is for 0(e.g. first) messageIndex
                outgoingCommandsMutex.unlock();
                totalsent += bytesRead;
                msgcounter++;
                //std::cout << "Messagecounter: " << msgcounter << std::endl;
                //socketsVector[transferCounter]->tcpSocket_.flush();
            }
        }
    }
    else if (receivedCommand_[0] == 0x05 || receivedCommand_[0] == 0x06)
    {
        statusCallBack(UNPACKING_AND_REPLACING_MAP);
    }
}

void AccerionSensor::getSoftwareDetails(_softwareDetailsCallBack sdCallback)
{
    softwareDetailsCallBack = sdCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWAREHASH, EmptyCommand(CMD_GET_SOFTWAREHASH).serialize());
    outgoingCommandsMutex.unlock();
}

SoftwareDetails AccerionSensor::getSoftwareDetailsBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_SOFTWAREHASH, EmptyCommand(CMD_GET_SOFTWAREHASH).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(softwareDetailsAckMutex);
    if (softwareDetailsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        SoftwareDetails sd;
        sd.date = "";
        sd.softwareHash = "";
        return sd;
    }

    return receivedSoftwareDetails;
}

void AccerionSensor::acknowledgeSoftwareDetails(std::vector<uint8_t> data)
{
    SoftwareDetails sd;
    char hash[40];
    for (int i = 0; i < 40; i++)
    {
        hash[i] = data[i];
    }
    char date[12];
    for (int i = 0; i < 12; i++)
    {
        date[i] = data[40 + i];
    }
    sd.softwareHash = std::string(hash);
    sd.date = std::string(date);

    if (softwareDetailsCallBack)
    {
        softwareDetailsCallBack(sd);
    }
    std::unique_lock<std::mutex> lck(softwareDetailsAckMutex);
    receivedSoftwareDetails = sd;
    softwareDetailsAckCV.notify_all();
}

void AccerionSensor::subscribeToDriftCorrectionsMissed(_driftCorrectionsMissedCallBack dcmCallback)
{
    driftCorrectionsMissedCallBack = dcmCallback;
}

void AccerionSensor::outputDriftCorrectionsMissed(std::vector<uint8_t> data)
{
    int driftCorrectionsMissed = (uint16_t)ntohs(*((uint16_t *)&data[0]));

    if (driftCorrectionsMissedCallBack)
    {
        driftCorrectionsMissedCallBack(driftCorrectionsMissed);
    }
}

void AccerionSensor::subscribeToArucoMarkers(_arucoMarkerCallBack amCallback)
{
    arucoMarkerCallBack = amCallback;
}

void AccerionSensor::outputArucoMarker(std::vector<uint8_t> data)
{
    ArucoMarker am;
    am.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&receivedCommand_[0])) / 1000000.0;
    am.pose.x = ((int32_t)ntohl(*((int32_t *)&data[8]))) / 1000000.0;
    am.pose.y = ((int32_t)ntohl(*((int32_t *)&data[12]))) / 1000000.0;
    am.pose.heading = ((int32_t)ntohl(*((int32_t *)&data[16]))) / 100.0;
    am.markerID = (uint16_t)ntohl(*((uint16_t *)&data[20]));

    if (arucoMarkerCallBack)
    {
        arucoMarkerCallBack(am);
    }
}

void AccerionSensor::subscribeToMapLoaded(_mapLoadedCallBack mlCallback)
{
    mapLoadedCallBack = mlCallback;
}

void AccerionSensor::outputMapLoaded(std::vector<uint8_t> data)
{
    MapLoadingInfo mli;
    mli.success = data[0] == 1;
    mli.percentage = data[1];
    mli.message = "";
    
    if (mapLoadedCallBack)
    {
        mapLoadedCallBack(mli);
    }
}

void AccerionSensor::toggleArucoMarkerDetectionMode(bool on, _acknowledgementCallBack tammCallback)
{
    toggleArucoMarkerModeCallBack = tammCallback;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_ARUCO_MARKER_MODE, BooleanCommand(CMD_SET_ARUCO_MARKER_MODE, on).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::toggleArucoMarkerDetectionModeBlocking(bool on)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_ARUCO_MARKER_MODE, BooleanCommand(CMD_SET_ARUCO_MARKER_MODE, on).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(arucoMarkerModeAckMutex);
    if (arucoMarkerModeAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedArucoMarkerModeAck.value == on)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::acknowledgeToggleArucoMarkerMode(std::vector<uint8_t> data)
{
    Acknowledgement ack;
    if (data[0] == 0x01)
    {
        ack.value = true;
    }
    else if (data[0] == 0x02)
    {
        ack.value = false;
    }

    if (toggleArucoMarkerModeCallBack)
    {
        toggleArucoMarkerModeCallBack(ack);
    }
    std::unique_lock<std::mutex> lck(arucoMarkerModeAckMutex);
    receivedArucoMarkerModeAck = ack;
    arucoMarkerModeAckCV.notify_all();
}

void AccerionSensor::setBufferLength(uint32_t bufferLength, _bufferLengthCallBack blCallBack)
{
    bufferLengthCallBack = blCallBack;
    bufferLength = std::ceil(bufferLength*1000000); // convert to micrometer for serialization
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_BUFFER_LENGTH, UINT32Command(CMD_SET_BUFFER_LENGTH, bufferLength).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::setBufferLengthBlocking(uint32_t bufferLength)
{
    bufferLength = std::ceil(bufferLength*1000000);// convert to micrometer for serialization
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_SET_BUFFER_LENGTH, UINT32Command(CMD_SET_BUFFER_LENGTH, bufferLength).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    if (bufferLengthAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    if (receivedBufferLength == bufferLength)
    {
        return 1;
    }

    return 0;
}

void AccerionSensor::getBufferLength(_bufferLengthCallBack blCallBack)
{
    bufferLengthCallBack = blCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_BUFFER_LENGTH, EmptyCommand(CMD_GET_BUFFER_LENGTH).serialize());
    outgoingCommandsMutex.unlock();
}

int AccerionSensor::getBufferLengthBlocking()
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_GET_BUFFER_LENGTH, EmptyCommand(CMD_GET_BUFFER_LENGTH).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    if (bufferLengthAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return -1;
    }

    return receivedBufferLength;
}

void AccerionSensor::acknowledgeBufferLength(std::vector<uint8_t> data)
{
    int bufferLength = ((uint32_t)ntohl(*((uint32_t *)&data[0]))) / 1000000.0;

    if(bufferLengthCallBack)
    {
        bufferLengthCallBack(bufferLength);
    }

    std::unique_lock<std::mutex> lck(bufferLengthAckMutex);
    receivedBufferLength = bufferLength;
    bufferLengthAckCV.notify_all();
}

void AccerionSensor::startBufferedRecovery(int32_t xPos, int32_t yPos, uint8_t radius, _bufferProgressCallBack bpCallBack)
{
    bufferProgressCallBack = bpCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_START_PROCESSING_BUFFER, BufferedRecoveryCommand(CMD_START_PROCESSING_BUFFER, xPos, yPos, radius).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::cancelBufferedRecovery(_bufferProgressCallBack bpCallBack)
{
    bufferProgressCallBack = bpCallBack;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_STOP_PROCESSING_BUFFER, BooleanCommand(CMD_STOP_PROCESSING_BUFFER, false).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeBufferProgress(std::vector<uint8_t> data)
{
    BufferProgress bp;
    bp.messageType  = data[0];
    bp.result       = data[1];
    bp.progress     = data[2];

    if(bufferProgressCallBack)
    {
        bufferProgressCallBack(bp);
    }
}

void AccerionSensor::getRecordingsList(_recordingListCallBack recCallBack)
{
    recordingListCallBack = recCallBack;
    std::vector<uint8_t> vec;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 5, vec).serialize());
    outgoingCommandsMutex.unlock();
}

bool AccerionSensor::getRecordingsListBlocking(std::vector<std::string> &vector)
{
    std::vector<uint8_t> vec;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 5, vec).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(recordingListAckMutex);
    if (recordingListAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return false;
    }

    for(const auto & i : receivedRecordingList)
    {
        vector.push_back(i);
    }   

    return true;
}

void AccerionSensor::deleteRecordings(std::vector<uint8_t> indexes, _deleteRecordingsCallBack cb)
{
    deleteRecordingsCallBack = cb;
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 6, indexes).serialize());
    outgoingCommandsMutex.unlock();
}

DeleteRecordingsResult AccerionSensor::deleteRecordingsBlocking(std::vector<uint8_t> indexes)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 6, indexes).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(deleteRecordingsAckMutex);
    if (deleteRecordingsAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        DeleteRecordingsResult deleteRecordingsResult;
        deleteRecordingsResult.success = false;
        return deleteRecordingsResult;
    }

    return deleteRecordingsResult;
}

bool AccerionSensor::getRecordings(std::vector<uint8_t> indexes, std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!recordingsIsInProgress)
    {
        recordingsIsInProgress = true;
        totalRecordingsMessagesToBeTransferred_ = 0;
        recordingsMsgcounter = 0;
        recordingsProgressCallBack = progressCB;
        recordingsDoneCallBack = doneCB;
        recordingsStatusCallBack = statusCB;
        recordingsPath_ = destinationPath;
        recordingIndexes_ = indexes;
        return retrieveFirstRecordingsPiece();
    }
    else
    {
        recordingsStatusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionSensor::retrieveFirstRecordingsPiece()
{
    recordingsStatusCallBack(PACKING_RECORDINGS);

    if (!tcpClient->getConnected())
    {
        recordingsStatusCallBack(CONNECTION_FAILED);
        recordingsSuccessfullyTransferred = false;
        recordingsIsInProgress = false;
        return false;
    }

    if (FileExists(recordingsPath_))
    {
        if (remove(recordingsPath_.c_str()) != 0)
        {
            recordingsStatusCallBack(FAILED_TO_REMOVE_EXISTING);
            recordingsSuccessfullyTransferred = false;
            recordingsIsInProgress = false;
            return false;
        }
    }
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 2, recordingIndexes_).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::retrieveNextRecordingsPiece()
{
    if (!tcpClient->getConnected())
    {
        recordingsStatusCallBack(CONNECTION_FAILED);
        recordingsSuccessfullyTransferred = false;
        recordingsIsInProgress = false;
        return;
    }
    uint8_t array[4];
    Serialization::serializeUInt32(recordingsMsgcounter,array,false);
    std::vector<uint8_t> vec;
    for(unsigned char i : array)
    {
        vec.push_back(i);
    }
    
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_RECORDINGS, RecordingsCommand(CMD_RECORDINGS, 3, vec).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionSensor::acknowledgeRecordingMsg(std::vector<uint8_t> data)
{
    int size = data.size() - 5;
    int msgType = data[4];

    if (msgType == 0x00) // done, check next
    {
        recordingsIsInProgress = false;
        if(recordingsFile != NULL)
        {
            fclose(recordingsFile);
            recordingsFile = NULL;
        }
        recordingsDoneCallBack(recordingsSuccessfullyTransferred);
    }
    else if (msgType == 0x01) // fail, check next
    {
        recordingsIsInProgress = false;
        if (totalRecordingsMessagesToBeTransferred_ != 0)
        {
            if(recordingsFile != NULL)
            {
                fclose(recordingsFile);
                recordingsFile = NULL;
            }
        }
        recordingsSuccessfullyTransferred = false;
        recordingsDoneCallBack(recordingsSuccessfullyTransferred);
    }
    else if (msgType == 0x02) // info received
    {
        totalRecordingsMessagesToBeTransferred_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        recordingsFile = fopen(recordingsPath_.c_str(), "ab");
        if (recordingsFile)
        {
            recordingsMsgcounter++;
        }
        retrieveNextRecordingsPiece();
    }
    else if (msgType == 0x03) // pckg received
    {
        double prgrss = ((recordingsMsgcounter)*100.0 / totalRecordingsMessagesToBeTransferred_);
        recordingsStatusCallBack(RETRIEVING_RECORDINGS);
        recordingsProgressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = data.size() - 5;

        auto *buffer = new unsigned char[bytesRead];

        for (int i = 5; i < data.size(); i++)
        {
            buffer[i - 5] = *((unsigned char *)&data[i]);
        }

        fwrite(buffer, sizeof(unsigned char), bytesRead, recordingsFile);

        fflush(recordingsFile);

        delete[] buffer;
        recordingsMsgcounter++;
        retrieveNextRecordingsPiece();
    }
    else if (msgType == 0x04) // error
    {

    }
    else if (msgType == 0x05) // list
    {
        receivedRecordingList.clear();
        std::string str(receivedCommand_.begin()+5, receivedCommand_.end());

        std::istringstream f(str);
        std::string s;    
        while (getline(f, s, '|')) {
            receivedRecordingList.push_back(s);
        }

        if(recordingListCallBack)
        {
            recordingListCallBack(receivedRecordingList);
        }

        std::unique_lock<std::mutex> lck(recordingListAckMutex);
        recordingListAckCV.notify_all();
    }
    else if (msgType == 0x06) // delete
    {
        deleteRecordingsResult.failedIndexes.clear();
        if(size > 0)
        {
            for(int i = 5; i < size; i++)
            {
                deleteRecordingsResult.failedIndexes.push_back(data[i]);
            }
            deleteRecordingsResult.success = false;
        }
        else
        {
            deleteRecordingsResult.success = true;
        }

        if(deleteRecordingsCallBack)
        {
            deleteRecordingsCallBack(deleteRecordingsResult);
        }

        std::unique_lock<std::mutex> lck(deleteRecordingsAckMutex);
        deleteRecordingsAckCV.notify_all();
    }
}

std::vector<uint8_t> AccerionSensor::captureFrame(uint8_t camIdx, std::string key)
{
    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(CMD_CAPTURE_FRAME, CaptureFrameCommand(CMD_CAPTURE_FRAME, camIdx, key).serialize());
    outgoingCommandsMutex.unlock();

    std::unique_lock<std::mutex> lck(captureFrameAckMutex);
    if (captureFrameAckCV.wait_for(lck, std::chrono::seconds(timeOutInSecs)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        captureFrameResult.clear();
        return captureFrameResult;
    }

    return captureFrameResult;
}

void AccerionSensor::acknowledgeFrameCaptureMsg(std::vector<uint8_t> data)
{
    captureFrameResult.clear();
    int size = data.size() - 5;
    int msgType = data[4];

    size_t bytesRead = data.size() - 5;

        for (int i = 5; i < data.size(); i++)
        {
            captureFrameResult.push_back(*((unsigned char *)&data[i]));
        }

        std::unique_lock<std::mutex> lck(captureFrameAckMutex);
        captureFrameAckCV.notify_all();
}