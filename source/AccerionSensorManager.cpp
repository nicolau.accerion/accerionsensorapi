/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorManager.h"

void AccerionSensorManager::runUDPCommunication()
{
    ProfileTimer profileTimer("AccerionSensorManager UDP thread", true);
    float maxDuration = 1e6/NetworkConstants::APIThroughput;

    auto * udpReceiver = new UDPReceiver(API_RECEIVE_PORT_UDP);
    std::vector<Command> incomingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    std::cout << "[AccerionSensorManager] - Started Listening For Heartbeat Messages" << std::endl;
    while(true)
    {        
        profileTimer.startLoopTime();
        //receive
        while(udpReceiver->ReceiveMessage())
        {
            receivedMSG_.clear();
            receivedMSG_.insert(receivedMSG_.end(), udpReceiver->getReceivedMessage(), udpReceiver->getReceivedMessage() + udpReceiver->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }
        incomingCommandsTotal_.clear();        

        profileTimer.endLoopTime();
        long totalTime = profileTimer.getTotalLoopTime();
        if(totalTime < maxDuration)
        {
            int delay = maxDuration - totalTime;
            if(delay > 0)
            {
                std::this_thread::sleep_for(std::chrono::microseconds(delay));
            }
        }
    }
    std::cout << "[AccerionSensorManager] - Stopped Listening For Heartbeat Messages" << std::endl;
}

AccerionSensorManager* AccerionSensorManager::getInstance()
{
    static AccerionSensorManager instance;

    return &instance;
}

AccerionSensorManager::AccerionSensorManager()
{
     /*Initialize CRC Checking*/
    crc8_.crcInit();
    //launch communication on different thread..
    std::thread t(&AccerionSensorManager::runUDPCommunication, this);
    t.detach();
}

std::list<std::pair<Address, std::string>> AccerionSensorManager::getAllSensors()
{
    return sensors;
}

AccerionSensor* AccerionSensorManager::getAccerionSensorByIP(Address ip, Address localIP, ConnectionType conType)
{
    std::string serial;
    for(const auto& e: sensors)
    {
        if(e.first.first == ip.first && e.first.second == ip.second && e.first.third == ip.third && e.first.fourth == ip.fourth)
        {
            serial = e.second;
        }
    }
    if(!serial.empty())
    {
        std::cout << "Sensor with serial number found: " << serial << std::endl;
        return new AccerionSensor(ip, serial, localIP, conType);
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

AccerionSensor* AccerionSensorManager::getAccerionSensorByIPBlocking(Address sensorIP, Address localIP, ConnectionType conType, int timeoutValueInSeconds)
{
    newSensorReceived = false;
    sensorIP_ = sensorIP;
    std::unique_lock<std::mutex> lck(sensorRequestAckMutex);
    if (sensorRequestAckCV.wait_for(lck, std::chrono::seconds(timeoutValueInSeconds)) == std::cv_status::timeout)
    {
        std::cout << "timeout.." << std::endl;
        return nullptr;
    }
    std::cout << "Sensor found.." << std::endl;
    AccerionSensor* sensor = getAccerionSensorByIP(sensorIP, localIP, conType);
    if(sensor == nullptr)
    {
        std::cout << "Sensor is nullptr, trying again blocking" << std::endl;
        sensor = getAccerionSensorByIPBlocking(sensorIP, localIP, conType, timeoutValueInSeconds);
    }
    return sensor;
}


void AccerionSensorManager::getAccerionSensorByIP(Address sensorIP, Address localIP, ConnectionType conType, _sensorCallBack scallback)
{
    newSensorReceived = false;
    sensorCallBack = scallback;

    sensorIP_ = sensorIP;
    localIP_ = localIP;
    conType_ = conType;
}

AccerionSensor* AccerionSensorManager::getAccerionSensorBySerial(std::string serial, Address localIP, ConnectionType conType)
{
    Address ip;
    bool found = false;
    for(const auto& e: sensors)
    {
        if(e.second == serial)
        {
            ip = e.first;
            found = true;
        }
    }
    if(found)
    {
        std::cout << "Sensor with ip found: " << +ip.first << "." << +ip.second << "." << +ip.third << "." << +ip.fourth << std::endl;
        return new AccerionSensor(ip, serial, localIP, conType);
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

void AccerionSensorManager::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();
        
        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if(debugMode_)
                    {
                        std::cout << "[AccerionSensorManager::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                    }
                }
            }
            // Extract number of bytes in message
            	if(receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO){
		        auto iter = commandValues.find(receivedCommandID_);
		        if(iter != commandValues.end())
		        {
		            receivedTotalBytesInMessage  = std::get<1>(iter->second);//std::get<1>(commandValues.at(receivedCommandID_));
		        }
		        else
		        {
                    if(debugMode_)
                    {
		                std::cout << "[AccerionSensorManager] Unknown Command received: " << std::hex << +receivedCommandID_ << std::dec << " From sensor: " <<  ntohl(*((uint32_t *)&receivedSerialNumberData)) << std::endl;
                    }
		            receivedTotalBytesInMessage = 0;
                    messageReady_ = false;
                    return;
		        }
		}else{
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t*)&receivedNumOfBytesData));
                if(debugMode_)
                {
                        std::cout << "[AccerionSensorManager::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }
        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));
        if(receivedCommandID_ == PRD_HEARTBEAT_INFO)
        {
            Address ip;
            ip.first   = receivedCommand_[0];
            ip.second  = receivedCommand_[1];
            ip.third   = receivedCommand_[2];
            ip.fourth  = receivedCommand_[3];
            std::string serial = std::to_string(receivedSerialNumber_);
            bool exists = false;
            for(const auto& e: sensors)
            {
                if(e.first.first == ip.first && e.first.second == ip.second && e.first.third == ip.third && e.first.fourth == ip.fourth && e.second == serial)
                {
                    exists = true;
                }
            }
            if(!exists)
            {
                std::pair<Address, std::string> sensor;
                sensor.first = ip;
                sensor.second = serial;
                sensors.push_back(sensor);
            }
            if(!newSensorReceived && ip.fourth == sensorIP_.fourth && ip.third == sensorIP_.third && ip.second == sensorIP_.second && ip.first == sensorIP_.first)
            {
                std::unique_lock<std::mutex> lck(sensorRequestAckMutex);
                newSensorReceived = true;
                if(sensorCallBack)
                {
                    AccerionSensor* sensor = getAccerionSensorByIP(sensorIP_, localIP_, conType_);
                    sensorCallBack(sensor);
                    sensorCallBack = nullptr;
                }
                newSensorReceived = true;
                sensorRequestAckCV.notify_all();
            }
        }
        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t*)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if(debugMode_)
            {
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if(debugMode_)
            {
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            }
            messageReady_ = false;
            return;
        }

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}