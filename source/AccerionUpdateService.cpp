/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUpdateService.h"

AccerionUpdateService::AccerionUpdateService(Address ip, std::string serial, Address localIP)
{
    /*Initialize CRC Checking*/
    crc8_.crcInit();
    char buf[3 * 4 + 3 * 1 + 1];
    snprintf(buf, sizeof(buf), "%d.%d.%d.%d", ip.first, ip.second, ip.third, ip.fourth);
    // store remote IP address in remote:
    struct sockaddr_in remote;
    inet_pton(AF_INET, buf, &(remote.sin_addr));

    // store local IP address in local:
    localIP_ = localIP;

    sensorSerialNumber_ = stoi(serial);

    tcpClient = new TCPClient(remote.sin_addr, UPDATE_SERVICE_PORT_TCP);
    tcpClient->sensorSerialNumber_ = stoi(serial);
    std::thread tcpThread(&AccerionUpdateService::runTCPCommunication, this);
    tcpThread.detach();
}

AccerionUpdateService::~AccerionUpdateService()
{
    delete udpReceiver;
}

void AccerionUpdateService::runTCPCommunication()
{
    std::vector<Command> incomingCommandsTotal_;
    std::vector<Command> outgoingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;

    tcpClient->connectToServer();
    while (runTCP)
    {
        //receive
        while (tcpClient->receiveMessage())
        {
            if (!lastMessageWasBroken_)
            {
                receivedMSG_.clear();
            }
            receivedMSG_.insert(receivedMSG_.end(), tcpClient->getReceivedMessage(), tcpClient->getReceivedMessage() + tcpClient->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }
        
        readMessages(incomingCommandsTotal_, outgoingCommandsTotal_);

        incomingCommandsTotal_.clear();

        //send..
        if (outgoingCommandsMutex.try_lock())
        {
            tcpClient->sendMessages(outgoingCommands);
            clearOutgoingCommands();
            outgoingCommandsMutex.unlock();
        }
        outgoingCommandsTotal_.clear();
    }
}

void AccerionUpdateService::readMessages(std::vector<Command> &incomingCommands, std::vector<Command> &outgoingCommands)
{
    auto it = incomingCommands.begin();
    while (it != incomingCommands.end())
    {
        receivedCommandID_ = (*it).commandID_;
        receivedCommand_ = (*it).command_;

        /*Check command, set modes according to command*/
        switch (receivedCommandID_)
        {
        case CommandIDs::PRD_HEARTBEAT_INFO:
            outputHeartBeat(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::STR_DIAGNOSTICS:
            outputDiagnostics(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        case CommandIDs::ACK_LOGS:
            retrievedLogPiece(receivedCommand_);
            it = incomingCommands.erase(it);
            break;
        default:
            it = incomingCommands.erase(it);
            break;
        }
    }
}

void AccerionUpdateService::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    if (debugMode_)
    {
        std::cout << "[AccerionUpdateService::parseMessages] numberOfBytesLeft is " << numberOfBytesLeft << std::endl;
        std::cout << "[AccerionUpdateService::parseMessages] receivedMessage_. Length is " << receivedMessage_.size() << std::endl;
    }

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();

        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if (debugMode_)
                        std::cout << "[AccerionUpdateService::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                }
            }
            // Extract number of bytes in message
            if (receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO && receivedCommandID_ != ACK_LOGS)
            {
                auto iter = commandValues.find(receivedCommandID_);
                if (iter != commandValues.end())
                {
                    receivedTotalBytesInMessage = std::get<1>(iter->second); //std::get<1>(commandValues.at(receivedCommandID_));
                }
                else
                {
                    std::cout << "Unknown Command received" << std::endl;
                    receivedTotalBytesInMessage = 0;
                }
            }
            else
            {
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t *)&receivedNumOfBytesData));
                if (debugMode_)
                    std::cout << "[AccerionUpdateService::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages] Received message was broken 1" << std::endl;
            }
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages] Received message was broken 2 with no of bytesleft: " << numberOfBytesLeft << std::endl;
            }
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            if (debugMode_)
            {
                std::cout << "[AccerionUpdateService::parseMessages]receivedMessage_. Length after broken 2 is " << receivedMessage_.size() << std::endl;
            }
            break;
        }
        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));

        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t *)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if (debugMode_)
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if (debugMode_)
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }

        if (debugMode_)
            std::cout << "Command length of the to be inserted command: " << receivedCommand_.size() << std::endl;

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        if (debugMode_)
        {
            std::cout << "Received serialNumber is := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            std::cout << "Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
            std::cout << "Received command size is := " << receivedCommand_.size() << std::endl;
        }

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}

void AccerionUpdateService::subscribeToHeartBeat(_heartBeatCallBack hbCallback)
{
    heartBeatCallBack = hbCallback;
}

void AccerionUpdateService::outputHeartBeat(std::vector<uint8_t> data)
{
    HeartBeat hb;
    hb.ipAddrFirst = data[0];
    hb.ipAddrSecond = data[1];
    hb.ipAddrThird = data[2];
    hb.ipAddrFourth = data[3];
    hb.uniFirst = data[4];
    hb.uniSecond = data[5];
    hb.uniThird = data[6];
    hb.uniFourth = data[7];

    if (heartBeatCallBack)
    {
        heartBeatCallBack(hb);
    }
}

void AccerionUpdateService::subscribeToDiagnostics(_diagnosticsCallBack diagCallback)
{
    diagnosticsCallBack = diagCallback;
}

void AccerionUpdateService::outputDiagnostics(std::vector<uint8_t> data)
{
    Diagnostics diag;

    diag.timeStamp = Serialization::ntoh64(reinterpret_cast<uint64_t*>(&data[0])) / 1000000.0;
    diag.modes = (uint16_t)ntohs(*((uint16_t *)&data[8]));
    diag.warningCodes = (uint16_t)ntohs(*((uint16_t *)&data[10]));
    diag.errorCodes = (uint32_t)ntohl(*((uint32_t *)&data[12]));
    diag.statusCodes = data[16];

    if (diagnosticsCallBack)
    {
        diagnosticsCallBack(diag);
    }
}

bool AccerionUpdateService::getLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!isInProgress)
    {
        commandIDToBeSent_ = CMD_GET_LOGS;
        isInProgress = true;
        totalMessagesToBeTransfered_ = 0;
        msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        logsPath_ = destinationPath;
        return retrieveFirstLogPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionUpdateService::getBackupLogs(std::string destinationPath, _progressCallBack progressCB, _doneCallBack doneCB, _statusCallBack statusCB)
{
    if (!isInProgress)
    {
        commandIDToBeSent_ = CMD_GET_BACKUP_LOGS;
        isInProgress = true;
        totalMessagesToBeTransfered_ = 0;
        msgcounter = 0;
        progressCallBack = progressCB;
        doneCallBack = doneCB;
        statusCallBack = statusCB;
        logsPath_ = destinationPath;
        return retrieveFirstLogPiece();
    }
    else
    {
        statusCallBack(ALREADY_IN_PROGRESS);
        return false;
    }
}

bool AccerionUpdateService::retrieveFirstLogPiece()
{
    statusCallBack(PACKING_LOGS);

    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccesfullyTransferred = false;
        isInProgress = false;
        return false;
    }

    if (FileExists(logsPath_))
    {
        if (remove(logsPath_.c_str()) != 0)
        {
            statusCallBack(FAILED_TO_REMOVE_EXISTING);
            filesSuccesfullyTransferred = false;
            isInProgress = false;
            return false;
        }
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(commandIDToBeSent_, UINT32Command(commandIDToBeSent_, 0).serialize());
    outgoingCommandsMutex.unlock();
}

void AccerionUpdateService::retrievedLogPiece(std::vector<uint8_t> receivedCommand_)
{
    //quint8 cmdType = *(reinterpret_cast<quint8*>(&receivedCommand_[4]));
    //uint32_t msgLength  = (uint32_t) ntohl(*((uint32_t*)&receivedCommand_[0]));

    if (receivedCommand_[4] == 0x00) // done, check next
    {
        isInProgress = false;
        fclose(logsFile);
        doneCallBack(filesSuccesfullyTransferred);
    }
    else if (receivedCommand_[4] == 0x01) // fail, check next
    {
        isInProgress = false;
        if (totalMessagesToBeTransfered_ != 0)
        {
            fclose(logsFile);
        }
        filesSuccesfullyTransferred = false;
        doneCallBack(filesSuccesfullyTransferred);
    }
    else if (receivedCommand_[4] == 0x02) // info received
    {
        totalMessagesToBeTransfered_ = (uint32_t)ntohl(*((uint32_t *)&receivedCommand_[5]));
        logsFile = fopen(logsPath_.c_str(), "ab");
        if (logsFile)
        {
            msgcounter++;
        }
        retrieveNextLogPiece();
    }
    else if (receivedCommand_[4] == 0x03) // pckg received
    {
        double prgrss = ((msgcounter)*100.0 / totalMessagesToBeTransfered_);
        statusCallBack(RETRIEVING_LOGS);
        progressCallBack(static_cast<int>(prgrss));

        size_t bytesRead = receivedCommand_.size() - 5;

        auto *buffer = new unsigned char[bytesRead];

        for (int i = 5; i < receivedCommand_.size(); i++)
        {
            buffer[i - 5] = *((unsigned char *)&receivedCommand_[i]);
        }

        fwrite(buffer, sizeof(unsigned char), bytesRead, logsFile);

        fflush(logsFile);

        delete[] buffer;
        msgcounter++;
        retrieveNextLogPiece();
    }
}

void AccerionUpdateService::retrieveNextLogPiece()
{
    if (!tcpClient->getConnected())
    {
        statusCallBack(CONNECTION_FAILED);
        filesSuccesfullyTransferred = false;
        isInProgress = false;
        return;
    }

    outgoingCommandsMutex.lock();
    outgoingCommands.emplace_back(commandIDToBeSent_, UINT32Command(commandIDToBeSent_, msgcounter).serialize());
    outgoingCommandsMutex.unlock();
}
