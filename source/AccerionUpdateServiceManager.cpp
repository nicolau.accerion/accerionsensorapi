/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionUpdateServiceManager.h"

void AccerionUpdateServiceManager::runUDPCommunication()
{
    auto * udpReceiver = new UDPReceiver(UPDATE_SERVICE_TRANSMIT_PORT_UDP);
    std::vector<Command> incomingCommandsTotal_;
    std::vector<uint8_t> receivedMSG_;
    std::cout << "[AccerionUpdateServiceManager] - Started Listening For Heartbeat Messages" << std::endl;
    while(true)
    {        
        //receive
        while(udpReceiver->ReceiveMessage())
        {
            receivedMSG_.clear();
            receivedMSG_.insert(receivedMSG_.end(), udpReceiver->getReceivedMessage(), udpReceiver->getReceivedMessage() + udpReceiver->getReceivedNumOfBytes());
            receivedCommand_.clear();
            parseMessage(incomingCommandsTotal_, receivedMSG_);
        }
        incomingCommandsTotal_.clear();        
    }
    std::cout << "[AccerionUpdateServiceManager] - Stopped Listening For Heartbeat Messages" << std::endl;
}

AccerionUpdateServiceManager* AccerionUpdateServiceManager::getInstance()
{
    static AccerionUpdateServiceManager instance;

    return &instance;
}

AccerionUpdateServiceManager::AccerionUpdateServiceManager()
{
     /*Initialize CRC Checking*/
    crc8_.crcInit();
    //launch communication on different thread..
    std::thread t(&AccerionUpdateServiceManager::runUDPCommunication, this);
    t.detach();
}

std::list<std::pair<Address, std::string>> AccerionUpdateServiceManager::getAllUpdateServices()
{
    return updateServices;
}

AccerionUpdateService* AccerionUpdateServiceManager::getAccerionUpdateServiceByIP(Address ip, Address localIP)
{
    std::string serial;
    for(const auto& e: updateServices)
    {
        if(e.first.first == ip.first && e.first.second == ip.second && e.first.third == ip.third && e.first.fourth == ip.fourth)
        {
            serial = e.second;
        }
    }
    if(!serial.empty())
    {
        std::cout << "Sensor with serial number found: " << serial << std::endl;
        return new AccerionUpdateService(ip, serial, localIP);
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

AccerionUpdateService* AccerionUpdateServiceManager::getAccerionUpdateServiceBySerial(std::string serial, Address localIP)
{
    Address ip;
    bool found = false;
    for(const auto& e: updateServices)
    {
        if(e.second == serial)
        {
            ip = e.first;
            found = true;
        }
    }
    if(found)
    {
        std::cout << "Sensor with ip found: " << ip.first << "." << ip.second << "." << ip.third << "." << ip.fourth << std::endl;
        return new AccerionUpdateService(ip, serial, localIP);
    }
    std::cout << "Sensor not found" << std::endl;
    return nullptr;
}

void AccerionUpdateServiceManager::parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_)
{
    uint8_t receivedSerialNumberData[4];

    //uint8_t receivedTotalBytesInMessageData[4];
    uint32_t receivedTotalBytesInMessage;
    uint32_t numberOfBytesLeft = receivedMessage_.size();
    uint32_t numberOfReadBytes = 0;

    while (numberOfBytesLeft > 0)
    {
        receivedCommand_.clear();
        
        if (numberOfBytesLeft >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
        {
            for (unsigned int i = 0; i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i++)
            {
                /*Get Serial Number*/
                if (i >= CommandFields::SERIAL_NUMBER_BEGIN_BYTE && i < CommandFields::SERIAL_NUMBER_BEGIN_BYTE + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH)
                {
                    receivedSerialNumberData[i] = receivedMessage_[i + numberOfReadBytes];
                }
                /*Get Command Number*/
                else if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE && i < CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH)
                {
                    receivedCommandID_ = receivedMessage_[i + numberOfReadBytes];
                    if(debugMode_)
                        std::cout << "[AccerionUpdateServiceManager::parseMessages] Received command: " << std::hex << +receivedCommandID_ << std::dec << std::endl;
                }
            }
            // Extract number of bytes in message
            	if(receivedCommandID_ != CMD_REPLACE_CLUSTER_G2O && receivedCommandID_ != CMD_PLACE_MAP && receivedCommandID_ != ACK_CONSOLE_OUTPUT_INFO){
		        auto iter = commandValues.find(receivedCommandID_);
		        if(iter != commandValues.end())
		        {
		            receivedTotalBytesInMessage  = std::get<1>(iter->second);//std::get<1>(commandValues.at(receivedCommandID_));
		        }
		        else
		        {
		            std::cout << "Unknown Command received" << std::endl;
		            receivedTotalBytesInMessage = 0;
		        }
		}else{
                uint8_t receivedNumOfBytesData[4];
                receivedNumOfBytesData[0] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes];
                receivedNumOfBytesData[1] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 1];
                receivedNumOfBytesData[2] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 2];
                receivedNumOfBytesData[3] = receivedMessage_[CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH + numberOfReadBytes + 3];

                receivedTotalBytesInMessage = ntohl(*((uint32_t*)&receivedNumOfBytesData));
                if(debugMode_)
                    std::cout << "[AccerionUpdateServiceManager::parseMessages] Received length: " << receivedTotalBytesInMessage << std::endl;
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }

        if (receivedTotalBytesInMessage <= numberOfBytesLeft)
        {
            for (unsigned int i = CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH; i < receivedTotalBytesInMessage; i++)
            {
                /*Get Command Data*/
                if (i >= CommandFields::COMMAND_ID_BEGIN_BYTE + CommandFields::COMMAND_ID_LENGTH && i < receivedTotalBytesInMessage - 1)
                {
                    receivedCommand_.push_back(receivedMessage_[i + numberOfReadBytes]);
                }
                /*Get CRC8 Data*/
                else
                {
                    receivedCRC8_ = receivedMessage_[i + numberOfReadBytes];
                }
            }
        }
        else
        {
            lastMessageWasBroken_ = true;
            receivedMessage_ = std::vector<uint8_t>(receivedMessage_.end() - numberOfBytesLeft, receivedMessage_.end());
            break;
        }
        lastMessageWasBroken_ = false;

        /*Check serial number*/
        receivedSerialNumber_ = ntohl(*((uint32_t *)&receivedSerialNumberData));
        if(receivedCommandID_ == PRD_HEARTBEAT_INFO)
        {
            Address ip;
            ip.first   = receivedCommand_[0];
            ip.second  = receivedCommand_[1];
            ip.third   = receivedCommand_[2];
            ip.fourth  = receivedCommand_[3];
            std::string serial = std::to_string(receivedSerialNumber_);
            bool exists = false;
            for(const auto& e: updateServices)
            {
                if(e.first.first == ip.first && e.first.second == ip.second && e.first.third == ip.third && e.first.fourth == ip.fourth)
                {
                    exists = true;
                }
            }
            if(!exists)
            {
                std::pair<Address, std::string> sensor;
                sensor.first = ip;
                sensor.second = serial;
                updateServices.push_back(sensor);
            }
        }
        uint8_t calculatedCRC8 = crc8_.crcFast((uint8_t*)receivedMessage_.data() + numberOfReadBytes, receivedTotalBytesInMessage - 1);
        if (receivedSerialNumber_ != sensorSerialNumber_ && receivedSerialNumber_ != DEFAULT_SERIAL_NUMBER && sensorSerialNumber_ != DEFAULT_SERIAL_NUMBER)
        {
            if(debugMode_)
                std::cout << "Received Message is not for this Jupiter, which has serial number := " << std::hex << sensorSerialNumber_ << " it is intended for serial number := " << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }
        if (receivedCRC8_ != calculatedCRC8)
        {
            if(debugMode_)
                std::cout << "Received Message does not have the correct crc8 code, it has the wrong code := " << std::hex << +receivedCRC8_ << std::dec << std::endl;
            messageReady_ = false;
            return;
        }

        commands.emplace_back(receivedCommandID_, receivedCommand_);

        numberOfReadBytes += receivedTotalBytesInMessage;
        numberOfBytesLeft -= receivedTotalBytesInMessage;
        receivedCommand_.clear();
    }
}