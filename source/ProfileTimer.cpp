/* Copyright (C) 2019 Accerion / Unconstrained Robotics B.V. - All Rights Reserved
 * You( excluding Accerion employees) may NOT use, distribute and modify this code 
 * under any terms or the terms of any license, unless with full prior permission by 
 * Accerion supported by a contract signed by both parties.
 *
 * Accerion employees may use, distribute (only in compiled and encrypted form) and 
 * modify this code as long as it is intended for Accerion usage and in no way creates
 * a situation that in any way could affect Accerion negatively. Contact your supervisor
 * in case of doubt.
 *
 * For any questions contact Accerion
 * Author(s):
 *	-Accerion
 */
#include "ProfileTimer.h"

ProfileTimer::ProfileTimer(const std::string& mainName, bool keepHistogram)
{
    mainName_          = mainName;
    nPartsToProfile_   = 0;
    idxCurrPart_       = 0;
    loopCount_         = 0;
    currThroughput_    = 0;
    avgThroughput_     = 0;
    avgLoopThroughput_ = 0;
    totalAbsTime_      = 0;
    keepHistogram_     = keepHistogram;
    histGridSz_        = 10;
    histSize_          = 300/histGridSz_;
    
    histogramStepTimes_.resize(histSize_);
    std::fill(histogramStepTimes_.begin(), histogramStepTimes_.end(), 0);

    startAbsTime();
}

ProfileTimer::~ProfileTimer()
{

}

void ProfileTimer::startAbsTime()
{
    absStartTime_  = std::chrono::high_resolution_clock::now();
}

void ProfileTimer::endAbsTime()
{
    absEndTime_     = std::chrono::high_resolution_clock::now();
    totalAbsTime_   = std::chrono::duration_cast<std::chrono::microseconds>( absEndTime_ - absStartTime_ ).count();  
}

void ProfileTimer::startLoopTime()
{
    nPartsToProfile_ = idxCurrPart_;
    idxCurrPart_     = 0;

    loopStartTime_  = std::chrono::high_resolution_clock::now();
    currTime_   = loopStartTime_;
    prevTime_   = currTime_;
}

void ProfileTimer::endLoopTime()
{
    loopEndTime_ = std::chrono::high_resolution_clock::now();
    loopCount_++;

    float currThroughput   = computeCurrentThroughput();

    avgLoopThroughput_ = (avgLoopThroughput_*(loopCount_-1) + currThroughput_)/(float)loopCount_;

    if(keepHistogram_)
    {
        // float currThroughput   = computeCurrentThroughput();
        unsigned int histEntry = (int)(currThroughput/histGridSz_);
        if( histEntry < histSize_ )
        {
            histogramStepTimes_[histEntry]++;
        }
        else
        {
            histogramStepTimes_[histSize_-1]++;
        }
    }
    totalLoopTime_  = std::chrono::duration_cast<std::chrono::microseconds>( loopEndTime_ - loopStartTime_ ).count();
}

void ProfileTimer::storeRelTime(const std::string& sectionName)
{   // currTime_ - prevTime_

    prevTime_   = currTime_;
    currTime_   = std::chrono::high_resolution_clock::now(); 

    if(sectionNames_[idxCurrPart_].empty())
    {
        sectionNames_[idxCurrPart_] = sectionName;
    }

    currentDurations_[idxCurrPart_] = std::chrono::duration_cast<std::chrono::microseconds>( currTime_ - prevTime_ ).count();
    meanDurations_[idxCurrPart_]    = (double)((meanDurations_[idxCurrPart_]*loopCount_) + currentDurations_[idxCurrPart_])/(double)(loopCount_+1.0);

    idxCurrPart_++;
}

// void ProfileTimer::storeAbsTime(const std::string& sectionName)
// {   //currTime_ - loopStartTime_

//     prevTime_   = currTime_;
//     currTime_   = std::chrono::high_resolution_clock::now(); 

//     if(sectionNames_[idxCurrPart_].empty())
//     {
//         sectionNames_[idxCurrPart_] = sectionName;
//     }

//     currentDurations_[idxCurrPart_] = std::chrono::duration_cast<std::chrono::microseconds>( currTime_ - loopStartTime_ ).count();
//     meanDurations_[idxCurrPart_]    = (double)((meanDurations_[idxCurrPart_]*loopCount_) + currentDurations_[idxCurrPart_])/(loopCount_+1.0);

//     idxCurrPart_++;
// }

void ProfileTimer::outputProfileDetails()
{
    for(unsigned int ii = 0; ii < nPartsToProfile_; ii++)
    {
        std::cout << std::setw(15) << sectionNames_[ii] << ": " << std::setw(6) << currentDurations_[ii] << " us : mean " << std::setw(6) << std::setprecision(1) << meanDurations_[ii] << " us"<< std::endl;
    }

    std::cout << "Loop time: " << totalLoopTime_ << std::endl;
    std::cout << "Throughput: " << 1000000.0/((float)totalLoopTime_) << std::endl;
}   

void ProfileTimer::outputThroughputDetails()
{
    std::stringstream ss;
    ss << "-start----------- Throughput details for " << mainName_ << std::endl;

    if(nPartsToProfile_ > 0)
    {
        ss << std::setw(25) << " " <<  "   mean duration [us]  |  mean throughput [Hz]" << std::fixed << std::endl;

        for(unsigned int ii = 0; ii < nPartsToProfile_; ii++)
        {
            ss << std::setw(25) << sectionNames_[ii] << ": " << std::setw(12) << std::setprecision(1) << meanDurations_[ii] << " us      | " << std::setw(12) << std::setprecision(1) << 1000000.0/((float)meanDurations_[ii]) << " Hz"<< std::endl;
        }
    }

    if(keepHistogram_)
    {
        // print the histogram for throughput
        ss << "-- histogram (in Hz) for n:= "<< loopCount_ << " loops" << std::endl;
        for(int ii=0; ii<histSize_; ii++)
        {
            if( histogramStepTimes_[ii] != 0) 
            { 
                ss << std::setw(3) << std::setprecision(0) << ii*histGridSz_ << "-" << std::setw(3) << std::setprecision(0) << (ii+1)*histGridSz_ << " : ";
                ss << std::setw(6) << histogramStepTimes_[ii] << " : \%" << std::fixed << std::setw(4) << std::setprecision(1) <<  100*(float)histogramStepTimes_[ii]/loopCount_ << std::endl;
            }
        }
    }

    computeAverageThroughput();

    ss << "Total time:                   "  << std::setw(6) << (float)totalAbsTime_/1000000.0  << " [s]" << std::endl;
    ss << "Total Abs throughput:         "  << std::setw(6) << avgThroughput_           << " [Hz]" << std::endl;
    ss << "Avg loop throughput:          "  << std::setw(6) << avgLoopThroughput_       << " [Hz]" << std::endl;
    ss << "-end------------- Throughput details for "  << mainName_            << std::endl;

    std::cout << ss.str();
}   

float ProfileTimer::computeCurrentThroughput()
{   
    currThroughput_ = 1000000.0/((float)totalLoopTime_);
    return currThroughput_;
}

float ProfileTimer::computeAverageThroughput()
{   
    if(totalAbsTime_ == 0)
    {
        std::cout << "ProfileTimer: please use endAbsTime() immediately after a loop to be more accurate" << std::endl;
        endAbsTime();
    }
    avgThroughput_  = loopCount_*1000000.0/((float)totalAbsTime_);

    return avgThroughput_;
}