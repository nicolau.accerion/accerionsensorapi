/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "UDPReceiver.h"

using namespace std;

UDPReceiver::UDPReceiver(unsigned int receivePort)
{
#ifdef DEBUG
    debugMode_ = true;
#endif
    

    receivePort_ = receivePort;

    socketLength_ = sizeof(remoteAddress_);


#ifdef __linux__ 
    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
#elif _WIN32
    WSADATA wsaData;
    int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
    }
    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    unsigned long argp = 1;// 1 to set non-blocking, 0 to set re-usable
    int result = setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, (char*)&argp, sizeof(argp));
    if (result != 0)
    {
        printf("setsockopt() error %d\n", result);
    }

    argp = 1;// 1 to set non-blocking, 0 to set blocking
    if (ioctlsocket(socketEndpoint_, FIONBIO, &argp) == SOCKET_ERROR)
    {
        printf("ioctlsocket() error %d\n", WSAGetLastError());
    }
#endif

    if(socketEndpoint_ < 0)
        cout << "Error while opening receiving socket" << endl;

#ifdef __linux__
    int enable = 1;
    if (setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        std::cout <<"setsockopt(SO_REUSEADDR) failed" << std::endl;
#endif

    thisAddress_.sin_family = AF_INET;
    thisAddress_.sin_port = htons(receivePort_);
    thisAddress_.sin_addr.s_addr = htonl (INADDR_ANY);

    if(bind(socketEndpoint_, (struct sockaddr *) &thisAddress_, sizeof(thisAddress_)) < 0)
        perror("Error while binding port, error is := " );
}

UDPReceiver::~UDPReceiver()
{
#ifdef __linux__ 
    close(socketEndpoint_);
#elif _WIN32
    closesocket(socketEndpoint_);
#endif
}

// bool UDPReceiver::setMulticastIPAddress(struct in_addr multicastAddress)
// {
//     struct in_addr netStart;
//     struct in_addr netEnd;

//     netStart.s_addr = inet_addr(NetworkConstants::multicastRangeFirstIPAddress.c_str());
//     netEnd.s_addr   = inet_addr(NetworkConstants::multicastRangeLastIPAddress.c_str());

//     if ((ntohl(multicastAddress.s_addr) >= ntohl(netStart.s_addr)) && (ntohl(multicastAddress.s_addr) <= ntohl(netEnd.s_addr)))
//     {
//         /*Drop membership from previous multicast group*/
//         struct ip_mreq ipMreq;
//         ipMreq.imr_multiaddr.s_addr = inet_addr(inet_ntoa(remoteAddress_.sin_addr));
//         ipMreq.imr_interface.s_addr = htonl(INADDR_ANY);

//         if (setsockopt(socketEndpoint_, IPPROTO_IP, IP_DROP_MEMBERSHIP, &ipMreq, sizeof(ipMreq)) < 0)
//         {
//             perror("Error when calling setsockopt for IP_DROP_MEMBERSHIP, error is");
//         }

//         /*Set the IP addresses for multicast UDP message*/
//         remoteAddress_.sin_family = AF_INET;
//         remoteAddress_.sin_port = htons(receivePort_);
//         remoteAddress_.sin_addr.s_addr = multicastAddress.s_addr;
//         memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

//         // Join to the new multicast group
//         ipMreq.imr_multiaddr.s_addr = inet_addr(inet_ntoa(remoteAddress_.sin_addr));
//         if (setsockopt(socketEndpoint_, IPPROTO_IP, IP_ADD_MEMBERSHIP, &ipMreq, sizeof(ipMreq)) < 0)
//         {
//             perror("Error when calling setsockopt for IP_ADD_MEMBERSHIP, error is");
//         }

//         if (debugMode_)
//         {
//             cout << "From UDP Receiver, setting multicast address to := " << inet_ntoa(remoteAddress_.sin_addr) << endl;
//         }
//         return true;
//     }
//     else
//     {
//         if (debugMode_)
//         {
//             std::cout << "False UDP Multicast address!" << std::endl;
//         }
//         return false;
//     }
// }

#ifdef __linux__ 
bool UDPReceiver::ReceiveMessage()
{
    receivedNumOfBytes_ = recvfrom(socketEndpoint_, receivedMessage_, sizeof(receivedMessage_), 0, (struct sockaddr*) &remoteAddress_, &socketLength_);

    if(receivedNumOfBytes_ != -1)
    {
        if (receivedNumOfBytes_ > bufferSize_)
        {
            if(debugMode_)
                cout << "Received UDP Message is too big, received num of bytes is := " << receivedNumOfBytes_ << endl;
            return false;
        }
        // if(debugMode_)
        //     cout << "received msg: " << receivedMessage_ << "received num of bytes := " << receivedNumOfBytes_ << endl;
        return true;
    }
    else
    {
        int errsv = errno;
        if(debugMode_ && (errsv != 11))
            perror ("Error while receiving messages in UDPReceiver, error is");
        return false;
    }
}
#elif _WIN32

bool UDPReceiver::ReceiveMessage()
{
    receivedNumOfBytes_ = recvfrom(socketEndpoint_, receivedMessage_, sizeof(receivedMessage_), 0, (struct sockaddr*) &remoteAddress_, &socketLength_);
    if (receivedNumOfBytes_ == SOCKET_ERROR) {
        int iError = WSAGetLastError();
        if (iError == WSAEWOULDBLOCK)
        {
            return false;
            //printf("recv failed with error: WSAEWOULDBLOCK\n");
        }
        else
        {
            printf("recv failed with error: %ld\n", iError);
            return false;
        }
     }
     else
     {
         if(receivedNumOfBytes_ > bufferSize_)
         {
            return false;
         }
         return true;
     }
}
#endif