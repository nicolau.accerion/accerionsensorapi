/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "commands.h"

Command::Command(uint8_t commandID, std::vector<uint8_t> command)
{
    commandID_ = commandID;
    command_ = command;
}

EmptyCommand::EmptyCommand(uint8_t commandID) : Command(commandID, std::vector<uint8_t>())
{

}

std::vector<uint8_t> EmptyCommand::serialize()
{
    return command_;
}

BooleanCommand::BooleanCommand(uint8_t commandID, bool boolValue) : Command(commandID, std::vector<uint8_t>())
{
    ackValue_ = boolValue ? CommandFlags::cmdTrue : CommandFlags::cmdFalse;
}

std::vector<uint8_t> BooleanCommand::serialize()
{
    command_.emplace_back(ackValue_);
    return command_;
}

UINT16Command::UINT16Command(uint8_t commandID, uint16_t value) : Command(commandID, std::vector<uint8_t>())
{
    value_ = value;
}

std::vector<uint8_t> UINT16Command::serialize()
{
    uint8_t byteArray[2];
    Serialization::serializeUInt16(value_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 2);
    return command_;
}

UINT32Command::UINT32Command(uint8_t commandID, uint32_t value) : Command(commandID, std::vector<uint8_t>())
{
    value_ = value;
}

std::vector<uint8_t> UINT32Command::serialize()
{
    uint8_t byteArray[4];
    Serialization::serializeUInt32(value_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);
    return command_;
}

RecoveryCommand::RecoveryCommand(uint8_t commandID, bool onOff, uint8_t radius) : Command(commandID, std::vector<uint8_t>())
{
    onOff_ = onOff ? CommandFlags::cmdTrue : CommandFlags::cmdFalse;
    radius_ = radius;
}

std::vector<uint8_t> RecoveryCommand::serialize()
{
    command_.emplace_back(onOff_);
    command_.emplace_back(radius_);
    return command_;
}

AddQRCommand::AddQRCommand(uint8_t commandID, uint16_t qrID, int32_t xPos, int32_t yPos, int32_t theta) : Command(commandID, std::vector<uint8_t>())
{
    qrID_ = qrID;
    xPos_ = std::ceil(xPos*1000000);
    yPos_ = std::ceil(yPos*1000000);
    theta_ = std::ceil(theta*100);
}

std::vector<uint8_t> AddQRCommand::serialize()
{
    uint8_t qrArray[2];
    Serialization::serializeUInt16(qrID_, qrArray, false);
    command_.insert(command_.end(), qrArray, qrArray + 2);

    uint8_t byteArray[4];
    Serialization::serializeUInt32(xPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(yPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(theta_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    return command_;
}

PoseCommand::PoseCommand(uint8_t commandID, double xPos, double yPos, double theta) : Command(commandID, std::vector<uint8_t>())
{
    xPos_ = std::ceil(xPos*1000000);
    yPos_ = std::ceil(yPos*1000000);
    theta_ = std::ceil(theta*100);
}

std::vector<uint8_t> PoseCommand::serialize()
{
    uint8_t byteArray[4];
    Serialization::serializeUInt32(xPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(yPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(theta_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    return command_;
}

SetIPCommand::SetIPCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t netmaskFirst,uint8_t netmaskSecond, uint8_t netmaskThird, uint8_t netmaskFourth, uint8_t gatewayFirst, uint8_t gatewaySecond, uint8_t gatewayThird, uint8_t gatewayFourth) : Command(commandID, std::vector<uint8_t>())
{
    ipAddrFirst_    = ipAddrFirst;
    ipAddrSecond_   = ipAddrSecond;
    ipAddrThird_    = ipAddrThird;
    ipAddrFourth_   = ipAddrFourth;
    netmaskFirst_   = netmaskFirst;
    netmaskSecond_  = netmaskSecond;
    netmaskThird_   = netmaskThird;
    netmaskFourth_  = netmaskFourth;
    gatewayFirst_   = gatewayFirst;
    gatewaySecond_  = gatewaySecond;
    gatewayThird_   = gatewayThird;
    gatewayFourth_  = gatewayFourth;
}

std::vector<uint8_t> SetIPCommand::serialize()
{
    command_.emplace_back(ipAddrFirst_);
    command_.emplace_back(ipAddrSecond_);
    command_.emplace_back(ipAddrThird_);
    command_.emplace_back(ipAddrFourth_);
    command_.emplace_back(netmaskFirst_);
    command_.emplace_back(netmaskSecond_);
    command_.emplace_back(netmaskThird_);
    command_.emplace_back(netmaskFourth_);
    command_.emplace_back(gatewayFirst_);
    command_.emplace_back(gatewaySecond_);
    command_.emplace_back(gatewayThird_);
    command_.emplace_back(gatewayFourth_);

    return command_;
}

DateTimeCommand::DateTimeCommand(uint8_t commandID, uint8_t day, uint8_t month, uint16_t year, uint8_t hours, uint8_t minutes, uint8_t seconds) : Command(commandID, std::vector<uint8_t>())
{
    day_ = day;
    month_ = month;
    year_ = year;
    hours_ = hours;
    minutes_ = minutes;
    seconds_ = seconds;
}

std::vector<uint8_t> DateTimeCommand::serialize()
{
    uint8_t byteArray[2];
    Serialization::serializeUInt16(year_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 2);
    command_.emplace_back(month_);
    command_.emplace_back(day_);
    command_.emplace_back(hours_);
    command_.emplace_back(minutes_);
    command_.emplace_back(seconds_);
    return command_;
}

ToggleMappingCommand::ToggleMappingCommand(uint8_t commandID, bool value, uint16_t clusterID) : Command(commandID, std::vector<uint8_t>())
{
    clusterID_ = clusterID;
    value_ = value;
}

std::vector<uint8_t> ToggleMappingCommand::serialize()
{

    if(value_)
    {
        command_.push_back(0x01);
    }
    else
    {
        command_.push_back(0x02);
    }

    uint8_t byteArray[2];
    Serialization::serializeUInt16(clusterID_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 2);

    return command_;
}

PoseAndCovarianceCommand::PoseAndCovarianceCommand(uint8_t commandID, uint64_t timeStamp, double xPos, double yPos, double heading, double stdDevX, double stdDevY, double stdDevTheta) : Command(commandID, std::vector<uint8_t>())
{
    timeStamp_ = timeStamp;
    xPos_ = std::ceil(xPos*1000000);
    yPos_ = std::ceil(yPos*1000000);
    heading_ = std::ceil(heading*100);
    stdDevX_ = std::ceil(stdDevX*1000000);
    stdDevY_ = std::ceil(stdDevY*1000000);
    stdDevTheta_ = std::ceil(stdDevTheta*100);
}

std::vector<uint8_t> PoseAndCovarianceCommand::serialize()
{
    uint8_t eightByteArray[8];
    Serialization::serializeUInt64(timeStamp_, eightByteArray, false);
    command_.insert(command_.end(), eightByteArray, eightByteArray + 8);

    uint8_t byteArray[4];
    Serialization::serializeUInt32(xPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(yPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(heading_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(stdDevX_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(stdDevY_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(stdDevTheta_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    return command_;
}

TCPIPReceiverCommand::TCPIPReceiverCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t messageType) : Command(commandID, std::vector<uint8_t>())
{
    ipAddrFirst_    = ipAddrFirst;
    ipAddrSecond_   = ipAddrSecond;
    ipAddrThird_    = ipAddrThird;
    ipAddrFourth_   = ipAddrFourth;
    messageType_    = messageType;
}

std::vector<uint8_t> TCPIPReceiverCommand::serialize()
{
    command_.emplace_back(ipAddrFirst_);
    command_.emplace_back(ipAddrSecond_);
    command_.emplace_back(ipAddrThird_);
    command_.emplace_back(ipAddrFourth_);
    command_.emplace_back(messageType_);
    return command_;
}

UDPSettingsCommand::UDPSettingsCommand(uint8_t commandID, uint8_t ipAddrFirst, uint8_t ipAddrSecond, uint8_t ipAddrThird, uint8_t ipAddrFourth, uint8_t messageType, uint8_t broadOrUniCast) : Command(commandID, std::vector<uint8_t>())
{
    ipAddrFirst_    = ipAddrFirst;
    ipAddrSecond_   = ipAddrSecond;
    ipAddrThird_    = ipAddrThird;
    ipAddrFourth_   = ipAddrFourth;
    messageType_    = messageType;
    broadOrUniCast_ = broadOrUniCast;
}

std::vector<uint8_t> UDPSettingsCommand::serialize()
{
    command_.emplace_back(ipAddrFirst_);
    command_.emplace_back(ipAddrSecond_);
    command_.emplace_back(ipAddrThird_);
    command_.emplace_back(ipAddrFourth_);
    command_.emplace_back(messageType_);
    command_.emplace_back(broadOrUniCast_);
    return command_;
}

G2OCommand::G2OCommand(uint8_t commandID, uint32_t messageLength, uint16_t clusterID, std::vector<uint8_t> data) : Command(commandID, std::vector<uint8_t>())
{
    messageLength_ = messageLength;
    clusterID_ = clusterID;
    data_ = data;
}

std::vector<uint8_t> G2OCommand::serialize()
{
    uint8_t byteArray[4];
    Serialization::serializeUInt32(messageLength_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    uint8_t byteArray2[2];
    Serialization::serializeUInt16(clusterID_, byteArray2, false);
    command_.insert(command_.end(), byteArray2, byteArray2 + 2);

    for(auto e: data_)
    {
        command_.emplace_back(e);
    }
    return command_;
}

PlaceMapCommand::PlaceMapCommand(uint8_t commandID, uint32_t messageLength, uint8_t packetType, uint32_t packetNumber, std::vector<uint8_t> data) : Command(commandID, std::vector<uint8_t>())
{
    messageLength_ = messageLength;
    packetType_ = packetType;
    packetNumber_ =  packetNumber;
    data_ = data;
}

std::vector<uint8_t> PlaceMapCommand::serialize()
{
    uint8_t byteArray[4];
    Serialization::serializeUInt32(messageLength_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    command_.emplace_back(packetType_);

    Serialization::serializeUInt32(packetNumber_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    for (auto i : data_)
    {
        command_.emplace_back(i);    
    }
    
    return command_;
}

BufferedRecoveryCommand::BufferedRecoveryCommand(uint8_t commandID, uint32_t xPos, uint32_t yPos, uint8_t radius) : Command(commandID, std::vector<uint8_t>())
{
    xPos_ = std::ceil(xPos*1000000);
    yPos_ = std::ceil(yPos*1000000);
    radius_ = radius;
}

std::vector<uint8_t> BufferedRecoveryCommand::serialize()
{
    uint8_t byteArray[4];
    Serialization::serializeUInt32(xPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    Serialization::serializeUInt32(yPos_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    command_.emplace_back(radius_);
    
    return command_;
}

RecordingsCommand::RecordingsCommand(uint8_t commandID, uint8_t packetType, std::vector<uint8_t> data) : Command(commandID, std::vector<uint8_t>())
{
    packetType_ = packetType;
    data_ = data;
}

std::vector<uint8_t> RecordingsCommand::serialize()
{
    messageLength_ = data_.size() + 1 + CommandFields::SENSOR_SERIAL_NUMBER_LENGTH + CommandFields::COMMAND_ID_LENGTH + CommandFields::CRC_LENGTH + CommandFields::MESSAGE_SIZE_LENGTH;
    uint8_t byteArray[4];
    Serialization::serializeUInt32(messageLength_, byteArray, false);
    command_.insert(command_.end(), byteArray, byteArray + 4);

    command_.emplace_back(packetType_);

    for (auto i : data_)
    {
        command_.emplace_back(i);    
    }
    
    return command_;
}

CaptureFrameCommand::CaptureFrameCommand(uint8_t commandID, uint8_t camIdx, std::string key) : Command(commandID, std::vector<uint8_t>())
{
    camIdx_ = camIdx;
    key_ = key;
}

std::vector<uint8_t> CaptureFrameCommand::serialize()
{
     command_.emplace_back(camIdx_);
     command_.insert(command_.end(), key_.c_str(), key_.c_str() + 16);
    return command_;
}